\section{Perfect sampling: noiseless, lossless}
\label{sec:perfect}

\subsection{Generalities and notation}

In this section, we will learn a procedure in perfect conditions : the traces are complete, sampling is lossless and noiseless.

\begin{illustration}{}
  In our experiments the traces contain all the destination register values for each instruction.
\end{illustration}

\paragraph{Generated data}

We want to build an oracle: a function $\mathcal{F}$ that compute the same output as the targeted procedure for any given input.
To do that we will use traces: we repeatedly register trace data for several executions of the same procedure for different input data.
% Theses traces should contain the information to build our oracle.

In the data generating phase, we execute the procedure $n$ times, with inputs of size $is$ bytes, outputs of size $os$ bytes, and traces length of $ts$ bytes.
All measured data are sampled, giving us only values as unsigned bytes: $\mathbb{B} = \llbracket 0, 255 \rrbracket$.

The experiments gives us three matrices:
\begin{itemize}
\item $I$ is the input matrix, where $(I_{e, i}) \in \mathbb{B}^{n \times is}$.
\item $O$ is the output matrix, where $(O_{e, o}) \in \mathbb{B}^{n \times os}$.
\item $T$ is the trace matrix, where $(T_{e, t}) \in \mathbb{B}^{n \times ts}$.
\end{itemize}

One row of a matrix correponds to one execution. For the trace matrix, one column corresponds to all values sampled at the same time $t$, for all executions.

\paragraph{How to choose inputs}

In the data generating experiments, inputs have to be provided to the procedure we are trying to learn.
Our oracle $\mathcal{F}$ will be only as good as the inputs feeded: we must aim at triggering all the datapath inside the procedure, or at least all the ones necessary to use our oracle afterward.
In this sense, generating inputs for our analysis is similar to generating inputs for a fuzzing analysis: if we have any knowledge about the procedure to learn, we can craft better inputs and thus lower the required number of execution to successfully build our oracle.

\begin{illustration}
  In our example, we consider that we reverse engineer an unknown symmetric encryption procedure, thus random inputs are used.
  \end{illustration}

\paragraph{Vectorized functions and other notations}

Since the traces are synchronous, if $\forall e \exists t_1, t_2, t_3, f$ such that $T_{e,t_3} = f\left(T_{e,t_1}, T_{e, t_2}\right)$, we can say that there is a function that links times $t_1$ and $t_2$ to $t_3$.
We note $\vec{f}$ the vectorized function that applies $f$ element-wise to its inputs.

Therefore, the previous property can be writen $\exists t_1, t_2, t_3, f$ such that $T_{*, t_3} = \vec{f}\left(T_{*,t_1}, T_{*,t_2}\right)$.
The $*$ denoting that we work with a colum ($*$ select all rows) in the $T$ matrix.

To simplify our explanation, we abuse this notation by omitting the reference to the trace matrix: $\tilde{t}$ is a shorthand to designate the vector $T_{*,t}$ formed by all values in the trace matrix at time $t$.
If the matrix we are refering to is not obvious, we may indicate it as an exponent: $\tilde{t} = \tilde{t}^T = T_{*,t}$.
As a mnemonic, the tilde is graphically similar to a small trace.
The new notation is therefore: $\exists t_1, t_2, t_3, f$ such that $\tilde{t_3} = \vec{f}(\tilde{t_1}, \tilde{t_2})$.

\paragraph{Fractional added information}

At numerous points, we need a measure to detect 1-to-1 and 2-to-1 functions by measuring how much information is in the output, knowing the inputs.
The measure that we devised is what we call fractional added information ($fai$), which is the normalized conditional entropy.
\begin{equation}
  \label{eq:fai}
  fai(\tilde{b} | \tilde{a}) = \frac{H(\tilde{b} | \tilde{a})}{H(\tilde{b})}
\end{equation}

where $H$ is the Shannon entropy, $\tilde{b}$ the output and $\tilde{a}$ the input of the 1-to-1 or 2-to-1 function.
The normalization is useful to obtain an easier way to detect when the output is totally determined by the inputs, or evaluate in what proportion is this dependance, independently from the entropy of the output $\tilde{b}$.
A $fai$ measure of $1$ means that $\tilde{b}$ is independent from $\tilde{a}$. If $fai$ is $0$, then $\tilde{b}$ is totally determined by the knowledge of $\tilde{a}$.

In our case the input $\tilde{a}$ can be a column of bytes (1-to-1 function), or a column of pair of bytes (2-to-1 function).

\begin{illustration}
  Computing $H$ reliably can be tricky, in particular in our case where we want to minimize the number of traces needed.
  The estimation of Shannon entropy is an active area of research, but as far as we know, no solution has been proposed when we have a number of samples far lower than the size of the input sets.
  In our case, computing $fai$ requires to compute the entropy of an array of triplets (3-tuple) of bytes, but we have far less than $2^{24}$ executions.
  The entropy of, for example, $2^{16}$ triplets of bytes is necessary lower than $16$, even if they are drawn from a distribution with entropy greater than $16$.

  To overcome this issue, we build an entropy estimator. We precompute a table of entropy values given a fixed number of triplets, with both the computed Shannon entropy and the entropy of the distribution (corresponding to an infinite amount of data).
  We then use this table to find the estimated entropy from the computed entropy by interpolating between the closest values in the estimator table.
\end{illustration}

\paragraph{Strategy}

Whatever the sampling quality of our exeriments (lossy, noisy or not), our technique follows a similar strategy.

First, we recover the structure of the information flow inside the traces, and in relations with inputs and outputs.
Then we transform our $I, O$ and $T$ matrices into \textbf{dual traces} $D$, a new matrix representing the ``ideal'' traces that can be extracted from the procedure (noiseless, lossless and with only 2-to-1 functions).
From these dual traces, we determine the actual functions linking values between them in the same execution.

% Therefore, executing our oracle function is equivalent to: fill the input bytes and then recursively computes all the values in the dual trace, and finally extract the output bytes in it.

\subsection{Simplifying traces}


The first pass to apply to the raw traces is to remove all useless data.
First if the trace values at a given time are constant for all executions, there is no information content in it.
If $\exists c \in \mathbb{B}$ such that $\tilde{t} = c\cdot \vec{\mathbf{1}}$ then we remove $\tilde{t}$.

If two columns are identical, we keep only the first (to comply with the temporal causality principle).
If $\exists t_1, t_2$ with $t_1 < t_2$ such that $\tilde{t_1} = \tilde{t_2}$ then we remove $\tilde{t_2}$.

\begin{illustration}
In our data, this simplification allows us to keep only \TODO{?} columns out of \TODO{?} initially. %gives details for the two simplifications  
\end{illustration}


\subsection{Dual values and dual traces}
\label{sec:dual}

According to our prerequirements, only 1-to-1 or 2-to-1 operations are present in the trace at this point.
Therefore, the values at each time are entirely determined by the values at, at most, two previous times.

\paragraph{Dual traces} We can represent our computation as only composed of 2-to-1 operations.% if we apply a substitution on the operation input.

The traces that contains only (noiseless, lossless) 2-to-1 operations are called dual traces. They can easily be computed from the raw traces in the noiseless, lossless sampling case.

Let $f_i: \mathbb{B} \to \mathbb{B}, i \in \llbracket 0, n-1\rrbracket$ a valid chain of 1-to-1 operations. Meaning that in our procedure, the input of $f_i$ for a given $i \neq 0$ is the output of $f_{i-1}$.
At the end of the chain, the output of $f_{n-1}$ is used as the (\textit{e.g.} first) input of a 2-to-1 operation $g: \mathbb{B} \times \mathbb{B} \to \mathbb{B}$.
For each such 1-to-1 chain, the raw to dual traces transformation requires to remove all columns in the traces corresponding to the outputs of operations $f_i, \in \llbracket 0, n-1\rrbracket$. Now the 2-to-1 operation $g$ can be observed as a new operation
$g'(a,b) = g(f_{n-1} \circ \cdots \circ f_0(a), b)$, for $a, b \in \mathbb{B}$.

In other words, we can replace the initial 2-to-1 operation consuming the chain output with a new 2-to-1 operation consuming the chain input, while removing the rest of the 1-to-1 chain.

\paragraph{Dual values} In the case of a chain of 1-to-1 operations $f_i: \mathbb{B} \to \mathbb{B}, i \in \llbracket 0, n-1\rrbracket$, we call the vectors $[a, f_0(a), f_1 \circ f_0(a), \cdots , f_{n-1} \circ \cdots \circ f_0(a)], \forall a \in \mathbb{B}$ the dual values.
In the perfect case, dual values can be projected to their first dimension without loss of information, but the concept will prove handy in more complex cases (cf sections~\ref{sec:lossy} and \ref{sec:noisy}).

%2-to-1 functions only by removing all 1-to-1 functions

\paragraph{Identifying 1-to-1 functions}

To identify a 1-to-1 function, we compute the $fai(\tilde{t_2} | \tilde{t_1})$ measure for all pairs of time $(t_1, t_2)$ where $0 \leq t_1 < t_2 < ts$.
If $fai(\tilde{t_2} | \tilde{t_1}) = 0$, then $\exists f | \tilde{t_2} = \vec{f}(\tilde{t_1})$.

\begin{illustration}
Experimentally, instead of a strict equality to $0$, we select $fai$ values lower than a threshold taking into account imprecisions due to floating point arithmetics and estimation of entropy.
This is done by computing all $fai(\tilde{t_2} | \tilde{t_1})$ as illustrated on \autoref{fig:fai_mat}.
\end{illustration}

\begin{figure}[h]
  \input{figures/fai_matrix.tex}
  \caption{The $fai(\tilde{t_2} | \tilde{t_1})$ values for the first $512$ times (columns) of the traces, ranging a bit more than two AES rounds.}
  \label{fig:fai_mat}
\end{figure}


\subsection{Identifying and extracting 2-to-1 functions}
\label{sec:id_ex_2to1}

\paragraph{Identifying}

From now on, we are working with dual traces, containing only lossless, noiseless, 2-to-1 functions. A particularly costly step is to find these latter functions.
To identify them, we examine $fai$ measures for each $(t_1, t_2, t_3)$ triplets with $0 \leq t_1 < t_2 < t_3 < ts$.

If $fai(\tilde{t_3} | \tilde{t_1}, \tilde{t_2}) = 0$, then $\exists f | \tilde{t_3} = \vec{f}(\tilde{t_1}, \tilde{t_2})$.

\begin{illustration}
  Again, experimentally the actual condition is: lower than a given threshold.
\end{illustration}

% First, we compute the triplet shannon entropy $E_{t_1, t_2, t_3}$ for all executions.
% To be a 2-to-1 function, the output at $t_3$ must be entirely determined by inputs at $t_1$ and $t_2$.
% Our criteria to decide if the triplet $t_1, t_2, t_3$ corresponds to a 2-to-1 functions is:
% \begin{equation}
%   \label{eq:2-to-1_criteria}
%   E_{t_1, t_2, t_3} \leq 16 \text{ and } E_{t_1, t_2, t_3} \leq E_{t_1, t_2} + E_{t_3}
% \end{equation}

\paragraph{Extracting}

With the 2-to-1 functions identifed, we can now extract the function themselves.
For that purpose, we build a dictionnary of all the values encountered.
We build a function $g$ with inputs at time $t_1$ and $t_2$ and output at time $t_3$ by filling the $256 \times 256$ table (the two dimensions correponding to the two input values) with the corresponding output.
In case of incoherence, two different outputs for the same inputs, we deduce that we misidentified a 2-to-1 function.
If our table is not completly filled at the end of the process, we will probably not be able to compute the output from all the inputs (``probably'' because some patterns may correctly never be seen): we need more traces to reverse engineer the procedure.

\paragraph{How many executions do we need}
To fill our table, we must have enough data. 
Using the parameters for our problem instance: $i=2^{16}$ the number of possible inputs for a 2-to-1 functions, applied to the Coupon’s collector problem.
The expectaction for the number $n$ of executions to observe all procedure inputs is:
\begin{equation}
  E(n) = i\cdot \sum_{k=1}^i \frac{1}{k} \approx 764\,646 \text{ executions}
\end{equation}

\subsection{Linking inputs and outputs to dual traces}
\label{sec:io_linking}

To finish our oracle, we need to link procedure inputs and outputs to their corresponding appearance time in the dual traces.
This is simply done, for each input/output byte, by finding then extracting the corresponding 1-to-1 function.

\TODO{développer}


% intuitive principles

% detecting 1 -> 1 and 2 -> 1
% structure vs values


%%% Local Variables:
%%% mode: latex
%%% TeX-master: "../main"
%%% End:
