\section{Noisy lossy sampling}
\label{sec:noisy}

In this section, we consider that our sampling method is noisy: sampling twice the same data may yield different results. As a consequence, we must now work with probabilities.

Our analysis will be conducted with Gaussian noise: in this section the noisy sampling function $n(a, \sigma, r)$ is $s(a) + \sigma * \sqrt{2} * erf^{-1}(2\cdot r-1)$ with $a$ the data to sample, $\sigma$ the noise standard distribution and $r \in [0, 1]$ is a random value (one different value per sampling).

\subsection{Building poor dual traces}

\subsubsection{Identifying 1-to-1 functions}

1-to-1 functions can be identified, as in the previous cases, by using the $fai$ measure.
A new difficulty arises because of the noise: how to separate 1-to-1 functions with noise from 2-to-1 function.

For example, given the binary decompositions $\alpha = \alpha_7 \cdots \alpha_0$ and $\beta = \beta_7 \cdots \beta_0$, let $f$ be the function $f(\alpha, \beta) = \alpha_7 \cdots \alpha_1 (\alpha_0 \oplus \beta_0)$.
There is a risk that $f$ be misidentified as a 1-to-1 function where it is a 2-to-1 function.

There is no silver bullet to avoid this misidentification, but it can be overcome with different techniques.
First, if we are able to characterize the noise (by measuring $\sigma$), then we can use this knowledge to determine the best threshold for the $fai$ measure.
Second, in case of misidentification, the error can be observed since the graph of 2-to-1 functions will be incomplete.
The holes in the graph even point to which functions have been misidentified.
Finally, most classic operations (add, sub, xor, \ldots) that are usually implemented as instructions, do not suffer from this problem.

\subsubsection{Building dual values}

In case of the presence of noise, the dual values vectors may have more values than elements in $\mathbb{B}$.
Since the noise is iid, we can use a clustering method on these vectors to improve the signal to noise ratio.
The number of clusters must be equal or lower than $256$, allowing to project each dual value to its cluster index.

\begin{illustration}
  How we did the clustering ?
\end{illustration}


\subsection{Enriching poor dual traces}

In the noisy case, since our sampling is lossy, we also need to enrich our poor dual traces.
The technique is similar: we identify a value $\gamma = f_1(\alpha, \beta)$ in column $\tilde{c}$ by observing its impact on function $f_2$.

But in the noisy case, $f_2$ was characterized by the vector with $256$ coordinates ($1$ coordinate per $\delta$ value) $$\vec{v}_{\alpha, \beta} = [f_2(\gamma)(0), f_2(\gamma)(1), \cdots, f_2(\gamma)(255)].$$
In the noisy case, for a given $\gamma$ and $\delta$, we may observe different values $f_2(\gamma)(\delta)$ because of the noise.
As a consequence, we will characterize $f_2$ by a matrix in this case.
$$m_{\alpha, \beta} = \begin{bmatrix}
  p_0(f_2(\gamma)(0) = 0) & p_1(f_2(\gamma)(1) = 0) & \cdots & p_{255}(f_2(\gamma)(255) = 0) \\
  p_0(f_2(\gamma)(0) = 1) & p_1(f_2(\gamma)(1) = 1) & \cdots & p_{255}(f_2(\gamma)(255) = 1) \\
  \vdots & \vdots & \ddots & \vdots \\
  p_0(f_2(\gamma)(0) = 255) & p_1(f_2(\gamma)(1) = 255) & \cdots & p_{255}(f_2(\gamma)(255) = 255) \\
\end{bmatrix}.$$
In $m_{\alpha, \beta}$, each column $\delta$ is the probability distribution of observing $f_2(\gamma)(\delta)$ in the traces.
We hope that even if we have noise on the $\delta$ and $\epsilon$ values ($\epsilon = f_2(\gamma, \delta)$), $m_{\alpha, \beta}$ is still characteristic of the $\gamma$ value.

Now, instead of a sieve, we use a distance function to compare two matrices. In our case we sum the euclidian distances of the columns two by two.
$$dist(m_{\alpha, \beta}, m'_{\alpha, \beta}) = \sum_{0 \leq c < 256}\left( \sqrt{\sum_{0 \leq r < 256}(m_{\alpha, \beta})_{r,c}^2 - (m'_{\alpha, \beta})_{r,c}^2} \right).$$

We use the distance $dist$ to cluster the pairs $(\alpha, \beta)$: if they are close, we assume that they induce the same $\gamma$ value.

\begin{figure}
  \input{figures/second_order_with_input_control.tex}
  \caption{Visual representation of one particular matrix $m_{\alpha, \beta}$. The nine horizontal bands represent the nine Hamming weigth values with noise.
  For a fixed value $\delta$, the values $f_2(\gamma)(\delta)$ represents a Gaussian distribution centered on one Hamming weight value.}
  \label{fig:second_order_ex}
\end{figure}


\begin{illustration}
  euclidian distance with convolution to make it possible when undersampling
\end{illustration}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "../main"
%%% End:
