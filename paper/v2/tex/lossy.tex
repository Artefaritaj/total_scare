\section{Lossy sampling}
\label{sec:lossy}

In this section we consider the case of lossy sampling. This occurs when the tracing method is not perfect, for example we cannot extract the complete value of a register but only some bits.
In this section, traces are noiseless: for the same input, we obtain exactly the same trace. 
\begin{illustration}
  In our practical experiments, our sampling function $s$ is the hamming weight: the lossy traces contains the hamming weights of each byte of the perfect traces (that we have seen in \autoref{sec:perfect}).  
\end{illustration}


\subsection{Poor dual traces}

If we build dual traces as in the perfect case (lossless sampling), we quickly face a problem: intermediate values do not contain enough information to convert to dual traces by projecting dual values to their first dimension.


For a given 1-to-1 chain ${f_0, f_1, \cdots, f_{n-1}}$, the corresponding dual values are now $[s(a), s\circ f_0(a), s\circ f_1 \circ f_0(a), \cdots , s \circ f_{n-1} \circ \cdots \circ f_0(a)], \forall a \in \mathbb{B}$ where $s$ is the sampling (lossy) function.

Since there is no noise, there are at most $256$ (the size of $\mathbb{B}$) different vectors, $1$ per $a \in \mathbb{B}$.
As such the conversion to dual traces is not a projection to the first dimension, as in the perfect case, but a projection from dual values (vectors) to $\mathbb{B}$.
%extract the intermediate 2-to-1 functions. Indeed a hamming weight can take one out of 9 possible values instead of one out of 256 for an arbitrary byte function. Different 2-to-1 byte functions can be believed identical if we only observe the hamming weights.

A problem arises when there is no 1-to-1 chain between two 2-to-1 functions, or if the chain is too small with respect to the sampling function: there is not enough information in the dual value to properly recontruct the initial (before sampling) value.

% Example

\begin{illustration}
Let us imagine a 1-to-1 function $f_0$ that feeds a 2-to-1 function. If we look at our dual values $[HW(a), HW\left( f_0(a) \right)]$, we have at most $81$ different vectors, even if all $256$ byte values are taken by $a$.  
\end{illustration}



As a consequence, we call these dual traces, the poor dual traces (since they are poor in information content).
But we can recreate complete (rich) dual traces with dedicated techniques in some cases.

% As a consequence, building the dual traces has to be done in two phases.
% \begin{itemize}
% \item First, we build the poor dual traces: we concatenate the plaintexts, the lossy traces and the ciphertexts in one matrix and perform the same dual traces construction as in the perfect case (cf \autoref{sec:perfect}).
% \item We enrich the poor dual traces to recover intermediate value full ranges: this is the more complex step that we will describe in details in this section.
% \end{itemize}

\subsection{Enriching poor dual traces}

\paragraph{Appending inputs and outputs}
There is a simple way to add information into the poor dual traces: compute them from the raw traces appended with input before and output after.
These data are lossless and can be used as part of 1-to-1 chains or as intermediate values.

The goal now is to enrich our poor dual traces, that contain only 2-to-1 functions, to recover the information lost to the lossy sampling process.
The intuition behind our solution is simple: even if the actual intermediate value is lost to the lossy sampling, its influence on the rest of the trace can still be observed.

\subsubsection{Enrichment by second order side effects}

Enrichment is done one time (one column in the trace matrix) at a time.
By induction, to enrich the poor dual traces at time $t$, we suppose that all values at times $< t$ are rich.
Then we enrich $\tilde{t}$ before focusing on the next time $t+1$.

We consider the pattern of two elementary operations illustrated on \autoref{fig:wealth1}.

\begin{figure}[h!]
  \centering
  \input{figures/lossy1.tex}
  \caption{Wealth pattern: rich values are doubled.}
  \label{fig:wealth1}
\end{figure}


% vectorized operator
In this pattern, we consider $5$ times (or columns) named $a, b, c, d, e$, where $\exists f_1, f_2 | \tilde{c} = \vec{f_1}(\tilde{a}, \tilde{b})$ and $\tilde{e} = \vec{f_2}(\tilde{c}, \tilde{d})$.
We can observe $\tilde{a}, \tilde{b}, \vec{s}(\tilde{c}), \vec{s}(\tilde{d})$ or  $\tilde{d}$, $\vec{s}(\tilde{e})$ or $\tilde{e}$ and we want to recontruct $\tilde{c}$.

For that we will combine two properties. First, a given pair $(\alpha, \beta) \in \mathbb{B}^2$ determines the value $\gamma = f_1(\alpha, \beta)$.
Therefore, we can focus on values $\tilde{a}_{\tilde{a}\vec{=}\alpha,\tilde{b}\vec{=}\beta}, \tilde{b}_{\tilde{a}\vec{=}\alpha,\tilde{b}\vec{=}\beta}$: notation to say that we select rows $e$ in $T_{e,t}$ (the trace matrix) such that $T_{e,a} = \alpha$ and $T_{e,b} = \beta$.

We can observe that $\vec{s}(\tilde{c}_{\tilde{a}\vec{=}\alpha,\tilde{b}\vec{=}\beta}) = s(\gamma)\cdot \vec{\mathbf{1}}$.
In plain words, if we select rows such that the pair value $(\alpha, \beta)$ at time $a$ and $b$ is constant, we observe a constant vector at time $c$ for these same rows.
But $\tilde{d}_{\tilde{a}\vec{=}\alpha,\tilde{b}\vec{=}\beta}$ is not a constant vector. We can try to recover $\gamma$ from its influence on $f_2$.
We invoke currying to note $f_2(\gamma) : \mathbb{B} \mapsto \mathbb{B}$ the function created from $f_2$ by fixing one of its input.%: $f_2(\gamma) = f_2(\gamma, \cdot)$.
The goal is now to cluster pairs of value for times $a$ and $b$ depending on the resulting $f_2(\gamma)$ function.

Given two pairs $(\alpha_1, \beta_1), (\alpha_2, \beta_2)$, we can say that both pairs map to the same value $\gamma_1 = \gamma_2$ through $f_1$ if the two functions $f_2(\gamma_1)$ and $f_2(\gamma_2)$ are indistinguishable.
Please note that it is possible that $\gamma_1$ and $\gamma_2$ are different from $\gamma$ the initial value sampled in the trace, if this difference has no observable side effects, but it does not have any consequences to reconstruct the procedure data flow.

In the lossy case, identifying if $f_2(\gamma_1)$ and $f_2(\gamma_2)$ are identical can be achieved with a sieve.
For all pairs $(\alpha, \beta)$, determine the vector  $\vec{v}_{\alpha, \beta} = [f_2(\gamma)(0), f_2(\gamma)(1), \cdots, f_2(\gamma)(255)]$ where $\gamma = f_1(\alpha, \beta)$.
If a value is not observed in vector $\tilde{d}$ (if value $\delta$ is not seen in vector $\tilde{d}$, it is not possible to observe $f_2(\gamma)(\delta)$), replace the corresponding coordinate in the vector by a special ``don't care'' (noted $*$) value.

The $sieve: \mathbb{B}^{256} \times \mathbb{B}^{256} \mapsto \{\text{true}, \text{false}\}$ function determines if two vectors $\vec{v}_{\alpha_1, \beta_1}$ and $\vec{v}_{\alpha_2, \beta_2}$ corresponds to the same value in vector $\tilde{c}$.
$sieve(\vec{v}_{\alpha_1, \beta_1}, \vec{v}_{\alpha_2, \beta_2}) = \text{true}$ if and only if $f_1(\alpha_1, \beta_1) = \gamma_1 = \gamma_2 = f_1(\alpha_2, \beta_2)$.
The $sieve$ function compare vector coordinates two by two and return $\text{true}$ only if they are all equal, $*$ being equal to all values in $\mathbb{B}$.

Since, there are at most $256$ different values for $\gamma$, there are at most $256$ sets of pairs $(\alpha, \beta)$ that are transitively equal according to the $sieve$ function.
The final step is to number those sets, and use these values to generate the new colum $\tilde{c}$.

\TODO{use the value $s(\gamma)$}

% Then compare all pairs two by two, $(\alpha_1, \beta_1)$ and $(\alpha_2, \beta_2)$ maps to the same value in colum $\tilde{c}$, if the vectors are equal.\TODO{improve notation}
% Since $\gamma \in \mathbb{B}$, there can be only $256$ different $f_2^\cdot$ functions. 

\subsubsection{When is enrichment possible ?}
\label{sec:enrich_possible}

The critical question, whose answer may prevent our technique to work and point to efficient countermeasures, is at what conditions is enrichment possible.
We have identified $3$ conditions that may prevent enrichment:
\begin{enumerate}
  \item The number of observed traces. $n$ the number of traces determines the number of don't care holes in vector $\vec{v}_{\alpha, \beta}$.
At the extreme, if we only observe a few traces, the $sieve$ function would nearly always return $true$ since the vectors would be full of $*$.
We need to have a sizable amount of traces, per pair $(\alpha, \beta)$.
\begin{illustration}
  In our case, with Hamming weight as the sampling function, experimentally we observe that we need around $6M$ traces to succeed.
  This is the minimal number of execution where the sieve works but where there are still a lot of don’t care holes in vector $\vec{v}_{\alpha, \beta}$.
  If we desire to fill the vector with actual values, we would expect to need $2^{24} \cdot \sum_{k=1}^{2^{24}}\frac{1}{k} \approx 289M$ traces.
\end{illustration}
  \item The sampling function. Depending on the sampling function, we may need to recover a lot of information, or just a little.
  If with a Hamming weight function, $\vec{v}_{\alpha, \beta} \in [\![0, 9]\!]^{256}$, but if the sampling function is ``set most significant bit to zero'', then $\vec{v}_{\alpha, \beta} \in [\![0, 127]\!]^{256}$
  wich make our sieving converge quickier.
  \item The structure of the graph of 2-to-1 functions. In a way, this condition is an extension of the previous one: the best sampling function for the attacker is the identity.
It is possible to observe $\tilde{d}$ instead of $\vec{s}(\tilde{d})$ if $d < c$. We can do better: even if $c < d$, it may be possible to reorder the columns in the trace matrix.
Indeed, we need to maintain the dependance order $<_d$, a partial order that says that $\forall a, b$, $a <_d b$ if $b$ depends on $a$ or, equivalently, if there is a path from $a$ to $b$ in the graph of 2-to-1 functions.
We may reorder columns in the trace if we keep the dependance order. In some case, this flexibility may allow to enrich $d$ before $c$.

% Finally, the structure of the computation allows us to observe $\tilde{d}$ instead of $\vec{s}(\tilde{d})$ if $d < c$ or if $c < d$ and $c <_d d$.

\end{enumerate}

% Iteratively, we select the first poor dual value and try to enrich it.
% \begin{figure}[h]
%   \centering
%   \input{figures/lossy1.tex}
%   \caption{Wealth pattern 1: $\otimes$ for a rich value. The node under consideration is doubled.}
%   \label{fig:wealth1}
% \end{figure}

% \begin{figure}[h]
%   \centering
%   \input{figures/lossy2.tex}
%   \caption{Wealth pattern 2: $\otimes$ for a rich value. The node under consideration is doubled.}
%   \label{fig:wealth2}
% \end{figure}

% \begin{figure}[h]
%   \centering
%   \input{figures/lossy3.tex}
%   \caption{Wealth pattern 3: $\otimes$ for a rich value. The node under consideration is doubled.}
%   \label{fig:wealth3}
% \end{figure}

\subsection{Procedure recovery from rich dual traces}

From the rich dual traces, we fall back to the perfect case. Similarly to what was presented in sections~\ref{sec:id_ex_2to1} and~\ref{sec:io_linking}.
 
% case where it works easily (t4 is perfect)

% case where t4 is lossy
% case where t5 is lossy

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "../main"
%%% End:
