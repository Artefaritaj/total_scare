#!/usr/bin/env nix-shell
#!nix-shell --pure -p gnuplot -i bash

SCRIPTPATH="$( cd "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"
RESULT_DIR="$SCRIPTPATH/../data"
FIGURE_DIR="$SCRIPTPATH/.."

# $1 Plot title
# $2 hex prefix (e.g. "ul1d")
function gen_heatmap_fig {
    gnuplot -c "$SCRIPTPATH/display_second_order.gnuplot" "$1" "$RESULT_DIR/$2.csv" "$FIGURE_DIR/$2.tex" $3 $4
}

if [ -z $1 ]; then
    gen_heatmap_fig "$m_{\alpha, \beta}$" "second_order_with_input_control" 1 1
    # gen_heatmap_fig "Protected L1D" "pl1d" 

else
    gen_heatmap_fig "$1" "$2"
fi