# Total SCARE

## Folders

- rv_lib: contains the riscv machine as a lib
- simplass: the simple assembler. Parser and code generator for our own assembly language.
- rvem: the CLI tool to compile a program and run it on the emulator.
- aes: a simplass AES implementation.


## Rust toolchain

Install the rust toolchain if not already done

```
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
```

## Build and install (or update) the riscv emulator

```
cargo install --git https://gitlab.com/Artefaritaj/total_scare.git rvem
```
## Run the emulator to run a program

```
rvem -i main.s -o io run
```

## Run the emulator to trace a program

```
rvem -i main.s -o io -t traces.binu8 trace -c 1000
```

main.s is the input simplass file, trace.binu8 is where we store the traces, io is the filename to store inputs ("io.in") and the outputs ("io.out"), 1000 is the number of program executions to trace.

## Analysis

Launch julia

```
julia
```

Then import the loaders and the analysis functions.

```
include("../analysis/loaders.js")
include("../analysis/analysis.js")
```

Load the trace matrix and simplify it by removing constant columns.

```
traces = load_traces("traces.binu8")
simpl = remove_constants(traces)
```

Let's perform a simple mutual information analysis

```
mi_mat = mi_matrix(simpl)
```

Install the Plots package if not already done:
```
using Pkg
Pkg.install("Plots")
```

We can now display the resulting matrix.

```
using Plots
Plots.heatmap(mi_mat)
```

![AES round MI matrix](./illustrations/hm.png)


