/*
 * File: listing.rs
 * Project: program
 * Created Date: Friday February 14th 2020
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Monday, 17th February 2020 4:50:32 pm
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2020 INRIA
 */

use std::collections::{BTreeMap, HashMap};
use std::fmt;
use std::path::Path;
use std::fs;
use std::io::Write;

use crate::{
    machine::*,
    instructions::*,
    error::*,
};

#[derive(Debug)]
pub struct Listing<I: Instruction> {
    instructions:   BTreeMap<MemoryAddress, I>,
    symbols:        BTreeMap<MemoryAddress, String>,
}


impl<I: Instruction> Listing<I> {
    pub fn new() -> Listing<I> {
        Listing { instructions: BTreeMap::new(), symbols: BTreeMap::new() }
    }

    pub fn register(&mut self, address: MemoryAddress, instruction: I) {
        self.instructions.insert(address, instruction);
    }

    pub fn add_symbol(&mut self, address: MemoryAddress, symbol: String) {
        self.symbols.insert(address, symbol);
    }

    pub fn read_symbol_ledger(&mut self, ledger: &HashMap<String, MemoryAddress>) {
        for (symbol, add) in ledger.iter() {
            self.add_symbol(*add, symbol.to_owned())
        }
    }

    pub fn write_to_file<P: AsRef<Path>>(&self, path: P) -> Result<(), MachineError>{
        let mut file = fs::File::create(path)?;
        
        file.write_all(self.to_string().as_bytes())?;

        Ok(())
    }
}


impl<I: Instruction> fmt::Display for Listing<I> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {

        for (add, inst) in self.instructions.iter() {

            if let Some(symbol) = self.symbols.get(&add) {
                write!(f, "{}:\n", symbol)?;
            }

            write!(f, "{:x}:\t\t{:?}\n", add, inst)?;
        }
        Ok(())
    }
}