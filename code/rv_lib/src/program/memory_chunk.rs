/*
 * File: memory_chunk.rs
 * Project: program
 * Created Date: Friday July 5th 2019
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Tuesday, 17th December 2019 11:07:28 am
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2019 INRIA
 */

// use std::fmt;

use crate::{
    machine::*,
    // instructions::*,
};

#[derive(Debug,Clone)]
pub struct MemoryChunk {
    pub start_address: MemoryAddress,
    pub data: Vec<u8> 
}

impl MemoryChunk {
    pub fn empty() -> MemoryChunk {
        MemoryChunk {
            start_address:  MemoryAddress(0),
            data:         Vec::new()
        }
    }

    pub fn new(start_address: MemoryAddress, data: Vec<u8>) -> MemoryChunk {
        MemoryChunk {
            start_address,
            data
        }
    }

    pub fn size(&self) -> usize {
        self.data.len()
    }

    pub fn end_address(&self) -> MemoryAddress {
        let mut start = self.start_address;
        start.offset(self.size() as i64);
        start
    }

    pub fn is_address_in_chunk(&self, add: MemoryAddress) -> bool {
        add.0 >= self.start_address.0 && add.0 < (self.start_address.0 + self.size() as u64)
    }
}

// impl fmt::Display for MemoryChunk {
//     fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
//         write!(f, "Memory:")?;

//         let bytes_per_line = 16;
//         let header_mask = 0xFFFFFFFFFFFFFFF0u64;

//         let mut last_header = 0x0u64;

//         let mut drawn_on_line = 0;

//         for (add, val) in self.content.iter() {

//             if add.0 == 0 {
//                 write!(f, "\n\t@0x{:<10x}\t", add.0)?;
//                 last_header = add.0;
//                 drawn_on_line = 0;
//             }

//             //draw header if needed
//             let address_header = add.0 & header_mask;
//             if last_header != address_header{

//                 //finish line
//                 while drawn_on_line < bytes_per_line {
//                     write!(f, ".. ")?;
//                     drawn_on_line += 1;
//                 }

//                 //display address jump
//                 if (address_header - last_header) / bytes_per_line != 1 {
//                     write!(f, "\n\t...")?;
//                 }

//                 //display address header
//                 write!(f, "\n\t@0x{:<10x}\t", address_header)?;
//                 last_header = address_header;
//                 drawn_on_line = 0;
//             }


//             let line_index = add.0 % bytes_per_line;

//             //draw place holders
//             while drawn_on_line < line_index {
//                 write!(f, ".. ")?;
//                 drawn_on_line += 1;
//             }

//             write!(f, "{:02x} ", val)?;
//             drawn_on_line += 1;
//         }

//         //finish line
//         while drawn_on_line < bytes_per_line {
//             write!(f, ".. ")?;
//             drawn_on_line += 1;
//         }
        
//         Ok(())
//     }
// }