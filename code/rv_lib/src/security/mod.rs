/*
 * File: mod.rs
 * Project: security
 * Created Date: Friday November 15th 2019
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Friday, 15th November 2019 4:54:24 pm
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2019 INRIA
 */

//  pub mod hregister;
pub mod confidentiality_key;
pub mod confidential;

//  pub use self::hregister::HRegister;
pub use self::confidentiality_key::{ConfidentialityKey, KEY_WORD_SIZE};
pub use self::confidential::Confidential;