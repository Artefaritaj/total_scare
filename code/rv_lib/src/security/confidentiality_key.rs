/*
 * File: confidentiality_key.rs
 * Project: security
 * Created Date: Friday November 15th 2019
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Friday, 15th November 2019 4:54:13 pm
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2019 INRIA
 */

use rand::prelude::*;

use crate::{
    security::*,
};

pub const KEY_WORD_SIZE: usize = 4;

#[derive(Debug, Clone)]
pub struct ConfidentialityKey {
    pub material: [u32; KEY_WORD_SIZE]
}

impl ConfidentialityKey {
    pub fn rand() -> ConfidentialityKey {
    let mut rng = thread_rng();

    let mut material = [0u32; KEY_WORD_SIZE];
    for i in 0..KEY_WORD_SIZE {
        material[i] = rng.gen();
    }

    ConfidentialityKey { material }
    }
}

impl Confidential for ConfidentialityKey {
    fn encrypt(self, key: &ConfidentialityKey) -> Self {
        let mut encrypted = ConfidentialityKey { material: [0u32; KEY_WORD_SIZE] };
        for i in 0..KEY_WORD_SIZE {
            encrypted.material[i] = self.material[i] ^ key.material[i];
        }
        encrypted
    }

    fn decrypt(self, key: &ConfidentialityKey) -> Self {
        let mut encrypted = ConfidentialityKey { material: [0u32; KEY_WORD_SIZE] };
        for i in 0..KEY_WORD_SIZE {
            encrypted.material[i] = self.material[i] ^ key.material[i];
        }
        encrypted
    }
}