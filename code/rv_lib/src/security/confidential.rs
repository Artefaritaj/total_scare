/*
 * File: confidential.rs
 * Project: security
 * Created Date: Friday November 15th 2019
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Friday, 15th November 2019 3:35:08 pm
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2019 INRIA
 */

use std::num::Wrapping;


use crate::{
    security::*,
};

pub trait Confidential {
    // fn is_confidential(&self) -> bool;

    fn encrypt(self, key: &ConfidentialityKey) -> Self;
    fn decrypt(self, key: &ConfidentialityKey) -> Self;
}

// this is only simulated encryption => not secure !
impl Confidential for u32 {
    fn encrypt(self, key: &ConfidentialityKey) -> Self {
        self ^ key.material[0]
    }

    fn decrypt(self, key: &ConfidentialityKey) -> Self {
        self ^ key.material[0]
    }
}

// this is only simulated encryption => not secure !
impl Confidential for Wrapping<u32> {
    fn encrypt(self, key: &ConfidentialityKey) -> Self {
        Wrapping(self.0 ^ key.material[0])
    }

    fn decrypt(self, key: &ConfidentialityKey) -> Self {
        Wrapping(self.0 ^ key.material[0])
    }
}

// this is only simulated encryption => not secure !
impl Confidential for u64 {
    fn encrypt(self, key: &ConfidentialityKey) -> Self {
        self ^ key.material[0] as u64 ^ (key.material[1] as u64) << 32
    }

    fn decrypt(self, key: &ConfidentialityKey) -> Self {
        self ^ key.material[0] as u64 ^ (key.material[1] as u64) << 32
    }
}