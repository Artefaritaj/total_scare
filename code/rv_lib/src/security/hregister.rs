/*
 * File: hregister.rs
 * Project: security
 * Created Date: Friday November 15th 2019
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Friday, 15th November 2019 10:52:34 am
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2019 INRIA
 */

use std::convert::TryFrom;
use crate::error::*;
use std::str::FromStr;

/// A hardened register allowings to select confidentiality level
#[derive(Debug,Clone,Copy,Hash,PartialEq,Eq,PartialOrd,Ord)]
pub enum HRegister {
    Public(PublicRegister),
    Confidential(ConfidentialRegister),
}

#[derive(Debug,Clone,Copy,Hash,PartialEq,Eq,PartialOrd,Ord)]
pub enum PublicRegister {
    X0,
    X1,
    X2,
    X3,
    X4,
    X5,
    X6,
    X7,
    X8,
    X9,
    X10,
    X11,
    X12,
    X13,
    X14,
    X15,
}

#[derive(Debug,Clone,Copy,Hash,PartialEq,Eq,PartialOrd,Ord)]
pub enum ConfidentialRegister {
    X16,
    X17,
    X18,
    X19,
    X20,
    X21,
    X22,
    X23,
    X24,
    X25,
    X26,
    X27,
    X28,
    X29,
    X30,
    X31,

    ///Program counter
    PC,
}

impl HRegister {
    pub fn zero() -> HRegister {
        HRegister::Public(PublicRegister::X0)
    }

    pub fn return_address() -> HRegister {
        HRegister::Public(PublicRegister::X1)
    }

    pub fn stack_pointer() -> HRegister {
        HRegister::Public(PublicRegister::X2)
    }

    pub fn global_pointer() -> HRegister {
        HRegister::Public(PublicRegister::X3)
    }

    pub fn thread_pointer() -> HRegister {
        HRegister::Public(PublicRegister::X4)
    }

    pub fn t0() -> HRegister {
        HRegister::Public(PublicRegister::X5)
    }

    pub fn t1() -> HRegister {
        HRegister::Public(PublicRegister::X6)
    }
    
    pub fn t2() -> HRegister {
        HRegister::Public(PublicRegister::X7)
    }

    pub fn s0() -> HRegister {
        HRegister::Public(PublicRegister::X8)
    }

    pub fn frame_pointer() -> HRegister {
        HRegister::Public(PublicRegister::X8)
    }

    pub fn s1() -> HRegister {
        HRegister::Public(PublicRegister::X9)
    }

    pub fn a0() -> HRegister {
        HRegister::Public(PublicRegister::X10)
    }

    pub fn a1() -> HRegister {
        HRegister::Public(PublicRegister::X11)
    }

    pub fn a2() -> HRegister {
        HRegister::Public(PublicRegister::X12)
    }

    pub fn a3() -> HRegister {
        HRegister::Public(PublicRegister::X13)
    }

    pub fn a4() -> HRegister {
        HRegister::Public(PublicRegister::X14)
    }

    pub fn a5() -> HRegister {
        HRegister::Public(PublicRegister::X15)
    }

    pub fn a6() -> HRegister {
        HRegister::Confidential(ConfidentialRegister::X16)
    }

    pub fn a7() -> HRegister {
        HRegister::Confidential(ConfidentialRegister::X17)
    }
}


impl TryFrom<u32> for HRegister {
    type Error = String;

    fn try_from(value: u32) -> Result<Self, Self::Error> {
        match value {
            0 =>    Ok(HRegister::Public(PublicRegister::X0)),
            1 =>    Ok(HRegister::Public(PublicRegister::X1)),
            2 =>    Ok(HRegister::Public(PublicRegister::X2)),
            3 =>    Ok(HRegister::Public(PublicRegister::X3)),
            4 =>    Ok(HRegister::Public(PublicRegister::X4)),
            5 =>    Ok(HRegister::Public(PublicRegister::X5)),
            6 =>    Ok(HRegister::Public(PublicRegister::X6)),
            7 =>    Ok(HRegister::Public(PublicRegister::X7)),
            8 =>    Ok(HRegister::Public(PublicRegister::X8)),
            9 =>    Ok(HRegister::Public(PublicRegister::X9)),
            10 =>   Ok(HRegister::Public(PublicRegister::X10)),
            11 =>   Ok(HRegister::Public(PublicRegister::X11)),
            12 =>   Ok(HRegister::Public(PublicRegister::X12)),
            13 =>   Ok(HRegister::Public(PublicRegister::X13)),
            14 =>   Ok(HRegister::Public(PublicRegister::X14)),
            15 =>   Ok(HRegister::Public(PublicRegister::X15)),
            16 =>   Ok(HRegister::Confidential(ConfidentialRegister::X16)),
            17 =>   Ok(HRegister::Confidential(ConfidentialRegister::X17)),
            18 =>   Ok(HRegister::Confidential(ConfidentialRegister::X18)),
            19 =>   Ok(HRegister::Confidential(ConfidentialRegister::X19)),
            20 =>   Ok(HRegister::Confidential(ConfidentialRegister::X20)),
            21 =>   Ok(HRegister::Confidential(ConfidentialRegister::X21)),
            22 =>   Ok(HRegister::Confidential(ConfidentialRegister::X22)),
            23 =>   Ok(HRegister::Confidential(ConfidentialRegister::X23)),
            24 =>   Ok(HRegister::Confidential(ConfidentialRegister::X24)),
            25 =>   Ok(HRegister::Confidential(ConfidentialRegister::X25)),
            26 =>   Ok(HRegister::Confidential(ConfidentialRegister::X26)),
            27 =>   Ok(HRegister::Confidential(ConfidentialRegister::X27)),
            28 =>   Ok(HRegister::Confidential(ConfidentialRegister::X28)),
            29 =>   Ok(HRegister::Confidential(ConfidentialRegister::X29)),
            30 =>   Ok(HRegister::Confidential(ConfidentialRegister::X30)),
            31 =>   Ok(HRegister::Confidential(ConfidentialRegister::X31)),

            _ => Err(format!("{} is not a valid register number", value))
        }
    }
}

impl FromStr for HRegister {
    type Err = ISAError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s.to_lowercase().as_ref() {
            "x0"|"zero"                         => Ok(HRegister::Public(PublicRegister::X0)),
            "x1"|"ra"|"return_address"          => Ok(HRegister::Public(PublicRegister::X1)),
            "x2"|"sp"|"stack_pointer"           => Ok(HRegister::Public(PublicRegister::X2)),
            "x3"|"gp"|"global_pointer"          => Ok(HRegister::Public(PublicRegister::X3)),
            "x4"|"tp"|"thread_pointer"          => Ok(HRegister::Public(PublicRegister::X4)),
            "x5"|"t0"                           => Ok(HRegister::Public(PublicRegister::X5)),
            "x6"|"t1"                           => Ok(HRegister::Public(PublicRegister::X6)),
            "x7"|"t2"                           => Ok(HRegister::Public(PublicRegister::X7)),
            "x8"|"s0"|"fp"|"frame_pointer"      => Ok(HRegister::Public(PublicRegister::X8)),
            "x9"|"s1"                           => Ok(HRegister::Public(PublicRegister::X9)),
            "x10"|"a0"                          => Ok(HRegister::Public(PublicRegister::X10)),
            "x11"|"a1"                          => Ok(HRegister::Public(PublicRegister::X11)),
            "x12"|"a2"                          => Ok(HRegister::Public(PublicRegister::X12)),
            "x13"|"a3"                          => Ok(HRegister::Public(PublicRegister::X13)),
            "x14"|"a4"                          => Ok(HRegister::Public(PublicRegister::X14)),
            "x15"|"a5"                          => Ok(HRegister::Public(PublicRegister::X15)),
            "x16"|"a6"                          => Ok(HRegister::Confidential(ConfidentialRegister::X16)),
            "x17"|"a7"                          => Ok(HRegister::Confidential(ConfidentialRegister::X17)),
            "x18"|"s2"                          => Ok(HRegister::Confidential(ConfidentialRegister::X18)),
            "x19"|"s3"                          => Ok(HRegister::Confidential(ConfidentialRegister::X19)),
            "x20"|"s4"                          => Ok(HRegister::Confidential(ConfidentialRegister::X20)),
            "x21"|"s5"                          => Ok(HRegister::Confidential(ConfidentialRegister::X21)),
            "x22"|"s6"                          => Ok(HRegister::Confidential(ConfidentialRegister::X22)),
            "x23"|"s7"                          => Ok(HRegister::Confidential(ConfidentialRegister::X23)),
            "x24"|"s8"                          => Ok(HRegister::Confidential(ConfidentialRegister::X24)),
            "x25"|"s9"                          => Ok(HRegister::Confidential(ConfidentialRegister::X25)),
            "x26"|"s10"                         => Ok(HRegister::Confidential(ConfidentialRegister::X26)),
            "x27"|"s11"                         => Ok(HRegister::Confidential(ConfidentialRegister::X27)),
            "x28"|"t3"                          => Ok(HRegister::Confidential(ConfidentialRegister::X28)),
            "x29"|"t4"                          => Ok(HRegister::Confidential(ConfidentialRegister::X29)),
            "x30"|"t5"                          => Ok(HRegister::Confidential(ConfidentialRegister::X30)),
            "x31"|"t6"                          => Ok(HRegister::Confidential(ConfidentialRegister::X31)),
            "pc"|"program_counter"              => Ok(HRegister::Confidential(ConfidentialRegister::PC)),
            
            _ => Err(ISAError::RegisterParsingError { input: s.to_owned() })
        }
    }
}