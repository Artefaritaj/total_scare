/*
 * File: csr.rs
 * Project: src
 * Created Date: Friday July 19th 2019
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Monday, 22nd July 2019 11:25:29 am
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2019 INRIA
 */

// use crate::instructions::*;
// use crate::error::ISAError;

use std::fmt;

#[derive(Clone,Copy,PartialEq,Eq,PartialOrd,Ord,Hash)]
pub struct Shamt(pub u8);

impl fmt::Debug for Shamt {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "shamt 0x{:x}", self.0)
    }
}
