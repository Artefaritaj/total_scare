/*
 * File: part_selector.rs
 * Project: instructions
 * Created Date: Monday December 16th 2019
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Friday, 11th June 2021 11:53:25 am
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2019 INRIA
 */


///Select a part of an instruction
#[derive(Debug, Clone,Copy, PartialEq,Eq, PartialOrd,Ord,Hash)]
pub enum InstructionPartSelector {
    Rd,
    Rs1,
    Rs2,
    ImmU,
    ImmJ,
    ImmB,
    ImmI,
    ImmS,
    Shamt,
    Pred,//for fence
    Succ,//for fence
    Csr,
    Zimm,
}

impl InstructionPartSelector {

    fn read_u32_le(buffer: &[u8]) -> u32 {
        let mut res: u32 = 0;
        for i in 0..4 {
            res >>= 8;
            res ^= (buffer[i] as u32) << 24;
        }
        res
    }

    fn write_u32_le(mut val: u32, buffer: &mut [u8]) {
        for i in 0..4 {
            buffer[i] = (val & 0xFF) as u8;
            val >>= 8;
        }
    }

    pub fn rewrite(&self, buffer: &mut[u8], val: u64) {

        match self {
            InstructionPartSelector::ImmI => {
                let before: u32 = InstructionPartSelector::read_u32_le(buffer);
                let erasing: u32 = 0xFFF << 20;
                let in_place: u32 = (val as u32 & 0xFFF) << 20;
                let after: u32 = (before & !erasing) | in_place;
                
                InstructionPartSelector::write_u32_le(after, buffer);
            },
            InstructionPartSelector::ImmU => {
                let before: u32 = InstructionPartSelector::read_u32_le(buffer);
                let erasing: u32 = 0xFFFFF000;
                let in_place: u32 = val as u32 & 0xFFFFF000;
                let after: u32 = (before & !erasing) | in_place;
                
                InstructionPartSelector::write_u32_le(after, buffer);
            },
            InstructionPartSelector::ImmJ => {
                let imm1: u32 = val as u32 & 0x7FE;
                let imm11: u32 = val as u32 & 0x800;
                let imm12: u32 = val as u32 & 0xFF000;
                let imm20: u32 = val as u32 & 0x100000;

                let reorder_val: u32 = imm12 | imm11 << 9 | imm1 << 20 | imm20 << 11;
                let before: u32 = InstructionPartSelector::read_u32_le(buffer);
                let erasing: u32 = 0xFFFFF000;
                let in_place: u32 = reorder_val as u32 & 0xFFFFF000;
                let after: u32 = (before & !erasing) | in_place;

                InstructionPartSelector::write_u32_le(after, buffer);
            },
            InstructionPartSelector::ImmB => {
                let imm1: u32 = val as u32 & 0x1E;
                let imm5: u32 = val as u32 & 0x7E0;
                let imm11: u32 = val as u32 & 0x800;
                let imm12: u32 = val as u32 & 0x1000;

                let reorder_val: u32 = imm1 << 7 | imm5 << 20 | imm11 >> 4 | imm12 << 19;
                let before: u32 = InstructionPartSelector::read_u32_le(buffer);
                let erasing: u32 = 0xFE000F80;
                let in_place: u32 = reorder_val as u32 & 0xFE000F80;
                let after: u32 = (before & !erasing) | in_place;

                InstructionPartSelector::write_u32_le(after, buffer);
            },
            InstructionPartSelector::ImmS => {
                let imm0: u32 = val as u32 & 0x1F;
                let imm5: u32 = val as u32 & 0xFE0;

                let reorder_val: u32 = imm0 << 7 | imm5 << 20;
                let before: u32 = InstructionPartSelector::read_u32_le(buffer);
                let erasing: u32 = 0xFE000F80;
                let in_place: u32 = reorder_val as u32 & 0xFE000F80;
                let after: u32 = (before & !erasing) | in_place;

                InstructionPartSelector::write_u32_le(after, buffer);
            },
            _ => {} //do nothing
        }
    }
}

#[test]
fn test_rewriting_immi() {
    use crate::instructions::decoder::*;
    use crate::instructions::*;

    use byteorder::{/*ReadBytesExt, */WriteBytesExt, LittleEndian};

    let addi: RV32I = RV32I::Addi(Register::X2, Register::X3, Immediate(0xAAA));

    let encoded_u32 = addi.encode().unwrap();
    let mut mem: Vec<u8> = vec![1, 2];
    mem.write_u32::<LittleEndian>(encoded_u32).unwrap();

    let part = InstructionPartSelector::ImmI;
    part.rewrite(&mut mem[2..6], 0x555);

    //verify
    let addi_verif: RV32I = RV32I::Addi(Register::X2, Register::X3, Immediate(0x555));
    let encoded_u32_verif = addi_verif.encode().unwrap();
    
    let mut mem_verif: Vec<u8> = vec![1, 2];
    mem_verif.write_u32::<LittleEndian>(encoded_u32_verif).unwrap();

    assert_eq!(mem, mem_verif, "Immi: {:?} instead of {:?}", mem, mem_verif);

}

#[test]
fn test_rewriting_immj() {
    use crate::instructions::decoder::*;
    use crate::instructions::*;

    use byteorder::{/*ReadBytesExt, */WriteBytesExt, LittleEndian};

    let jal: RV32I = RV32I::Jal(Register::X2, Offset(0x123456));

    let encoded_u32 = jal.encode().unwrap();
    let mut mem: Vec<u8> = vec![1, 2];
    mem.write_u32::<LittleEndian>(encoded_u32).unwrap();

    let part = InstructionPartSelector::ImmJ;
    part.rewrite(&mut mem[2..6], 0x165432);

    //verify
    let jal_verif: RV32I = RV32I::Jal(Register::X2, Offset(0x165432));
    let encoded_u32_verif = jal_verif.encode().unwrap();
    
    let mut mem_verif: Vec<u8> = vec![1, 2];
    mem_verif.write_u32::<LittleEndian>(encoded_u32_verif).unwrap();

    assert_eq!(mem, mem_verif, "Immj: {:?} instead of {:?}", mem, mem_verif);

}

#[test]
fn test_rewriting_immb() {
    use crate::instructions::decoder::*;
    use crate::instructions::*;

    use byteorder::{/*ReadBytesExt, */WriteBytesExt, LittleEndian};

    let beq: RV32I = RV32I::Beq(Register::X2, Register::X3, Offset(0x1234));

    let encoded_u32 = beq.encode().unwrap();
    let mut mem: Vec<u8> = vec![1, 2];
    mem.write_u32::<LittleEndian>(encoded_u32).unwrap();

    let part = InstructionPartSelector::ImmB;
    part.rewrite(&mut mem[2..6], 0x1654);

    //verify
    let beq_verif: RV32I = RV32I::Beq(Register::X2, Register::X3, Offset(0x1654));
    let encoded_u32_verif = beq_verif.encode().unwrap();
    
    let mut mem_verif: Vec<u8> = vec![1, 2];
    mem_verif.write_u32::<LittleEndian>(encoded_u32_verif).unwrap();

    assert_eq!(mem, mem_verif, "ImmB: {:?} instead of {:?}", mem, mem_verif);

}

#[test]
fn test_rewriting_imms() {
    use crate::instructions::decoder::*;
    use crate::instructions::*;

    use byteorder::{/*ReadBytesExt, */WriteBytesExt, LittleEndian};

    let sw: RV32I = RV32I::Sw(Register::X2, Register::X3, Offset(0x1234));

    let encoded_u32 = sw.encode().unwrap();
    let mut mem: Vec<u8> = vec![1, 2];
    mem.write_u32::<LittleEndian>(encoded_u32).unwrap();

    let part = InstructionPartSelector::ImmS;
    part.rewrite(&mut mem[2..6], 0x1654);

    //verify
    let sw_verif: RV32I = RV32I::Sw(Register::X2, Register::X3, Offset(0x1654));
    let encoded_u32_verif = sw_verif.encode().unwrap();
    
    let mut mem_verif: Vec<u8> = vec![1, 2];
    mem_verif.write_u32::<LittleEndian>(encoded_u32_verif).unwrap();

    assert_eq!(mem, mem_verif, "ImmS: {:?} instead of {:?}", mem, mem_verif);

}