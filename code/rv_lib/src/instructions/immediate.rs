/*
 * File: immediate.rs
 * Project: instructions
 * Created Date: Thursday July 4th 2019
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Wednesday, 19th February 2020 3:16:42 pm
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2019 INRIA
 */

// use crate::instructions::*;
// use crate::error::ISAError;

use std::ops::*;
use std::fmt;
use std::num::Wrapping;

#[derive(Clone,Copy,PartialEq,Eq, PartialOrd,Ord,Hash)]
pub struct Immediate(pub u32);

impl Immediate {

    pub fn from_u64(val: u64) -> Immediate {
        Immediate( val as u32)
    }

    /// sign_bit is 0 or not 0
    /// sign_pos is 0-indexed
    pub fn sign_extend(val: u32, sign_bit: u32, sign_pos: u32) -> Immediate {
        let mut imm_val = val;
        let mut bitselect = 0x80000000;

        if sign_bit != 0 {
            for _ in sign_pos+1..32 {
                imm_val |= bitselect;
                bitselect >>= 1;
            }
        }

        Immediate(imm_val)
    }

    pub fn new(i: i32) -> Immediate {
        Immediate ( (i & 0xFFF) as u32)
    }
}


// *******************************
//
// 32-bit
//
// *******************************


impl Add<Immediate> for u32 {
    type Output = u32;

    fn add(self, other: Immediate) -> u32 {
        self.wrapping_add(other.0)
    }
}

impl BitAnd<Immediate> for u32 {
    type Output = u32;

    fn bitand(self, other: Immediate) -> u32 {
        self & other.0
    }
}

impl BitOr<Immediate> for u32 {
    type Output = u32;

    fn bitor(self, other: Immediate) -> u32 {
        self | other.0
    }
}

impl BitXor<Immediate> for u32 {
    type Output = u32;

    fn bitxor(self, other: Immediate) -> u32 {
        self ^ other.0
    }
}

impl Shl<Immediate> for u32 {
    type Output = u32;

    fn shl(self, other: Immediate) -> u32 {
        self << (other.0 as u32)
    }
}


impl Shr<Immediate> for u32 {
    type Output = u32;

    fn shr(self, other: Immediate) -> u32 {
        self >> (other.0 as u32)
    }
}

impl Add<Immediate> for Wrapping<u32> {
    type Output = Wrapping<u32>;

    fn add(self, other: Immediate) -> Wrapping<u32> {
        self + Wrapping(other.0)
    }
}

impl BitAnd<Immediate> for Wrapping<u32> {
    type Output = Wrapping<u32>;

    fn bitand(self, other: Immediate) -> Wrapping<u32> {
        Wrapping(self.0 & other.0)
    }
}

impl BitOr<Immediate> for Wrapping<u32> {
    type Output = Wrapping<u32>;

    fn bitor(self, other: Immediate) -> Wrapping<u32> {
        Wrapping(self.0 | other.0)
    }
}

impl BitXor<Immediate> for Wrapping<u32> {
    type Output = Wrapping<u32>;

    fn bitxor(self, other: Immediate) -> Wrapping<u32> {
        Wrapping(self.0 ^ other.0)
    }
}

impl Shl<Immediate> for Wrapping<u32> {
    type Output = Wrapping<u32>;

    fn shl(self, other: Immediate) -> Wrapping<u32> {
        Wrapping(self.0 << (other.0 as u32))
    }
}


impl Shr<Immediate> for Wrapping<u32> {
    type Output = Wrapping<u32>;

    fn shr(self, other: Immediate) -> Wrapping<u32> {
        Wrapping(self.0 >> (other.0 as u32))
    }
}

// *******************************
//
// 64-bit
//
// *******************************


impl Add<Immediate> for u64 {
    type Output = u64;

    fn add(self, other: Immediate) -> u64 {
        self.wrapping_add(other.0 as u64)
    }
}

impl BitAnd<Immediate> for u64 {
    type Output = u64;

    fn bitand(self, other: Immediate) -> u64 {
        self & (other.0 as u64)
    }
}

impl BitOr<Immediate> for u64 {
    type Output = u64;

    fn bitor(self, other: Immediate) -> u64 {
        self | (other.0 as u64)
    }
}

impl BitXor<Immediate> for u64 {
    type Output = u64;

    fn bitxor(self, other: Immediate) -> u64 {
        self ^ (other.0 as u64)
    }
}

impl Shl<Immediate> for u64 {
    type Output = u64;

    fn shl(self, other: Immediate) -> u64 {
        self << (other.0 as u64)
    }
}

impl Shr<Immediate> for u64 {
    type Output = u64;

    fn shr(self, other: Immediate) -> u64 {
        self >> (other.0 as u64)
    }
}

impl fmt::Debug for Immediate {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Imm 0x{:x}", self.0)
    }
}


#[test]
fn test_sign_extend() {
    let input = 0x1A9876;
    let sign = 1;
    let high_pos = 20;

    let signed = Immediate::sign_extend(input, sign, high_pos);
    assert_eq!(signed, Immediate(0xFFFA9876));
}