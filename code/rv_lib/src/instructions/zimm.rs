/*
 * File: csr.rs
 * Project: src
 * Created Date: Friday July 19th 2019
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Monday, 22nd July 2019 11:25:13 am
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2019 INRIA
 */

// use crate::instructions::*;
// use crate::error::ISAError;

use std::fmt;

#[derive(Clone,Copy,PartialEq,Eq,PartialOrd,Ord,Hash)]
pub struct Zimm(pub u8);

impl fmt::Debug for Zimm {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Zimm 0x{:x}", self.0)
    }
}
