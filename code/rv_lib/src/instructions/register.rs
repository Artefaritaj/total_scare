/*
 * File: register.rs
 * Project: instructions
 * Created Date: Monday July 1st 2019
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Wednesday, 19th February 2020 11:24:25 am
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2019 INRIA
 */

use std::fmt;

use crate::{
    error::*,
    instructions::decoder::*,
};
use std::str::FromStr;

#[derive(Debug,Clone,Copy,Hash,PartialEq,Eq,PartialOrd,Ord)]
pub enum Register {
    X0,
    X1,
    X2,
    X3,
    X4,
    X5,
    X6,
    X7,
    X8,
    X9,
    X10,
    X11,
    X12,
    X13,
    X14,
    X15,
    X16,
    X17,
    X18,
    X19,
    X20,
    X21,
    X22,
    X23,
    X24,
    X25,
    X26,
    X27,
    X28,
    X29,
    X30,
    X31,

    ///Program counter
    PC
}

impl Register {

    pub fn is_confidential(&self) -> bool {
        match self {
            /*Register::X0 | */Register::X1 | Register::X2 | Register::X3 | Register::X4 | Register::X5 | Register::X6 | Register::X7 |
            Register::X8 | Register::X9 | Register::X10 | Register::X11 | Register::X12 | Register::X13 | Register::X14 | Register::X15
            => {
                true
            },

            _ => false
        }
    }

    pub fn zero() -> Register {
        Register::X0
    }

    pub fn return_address() -> Register {
        Register::X1
    }

    pub fn stack_pointer() -> Register {
        Register::X2
    }

    pub fn global_pointer() -> Register {
        Register::X3
    }

    pub fn thread_pointer() -> Register {
        Register::X4
    }

    pub fn t0() -> Register {
        Register::X5
    }

    pub fn t1() -> Register {
        Register::X6
    }
    
    pub fn t2() -> Register {
        Register::X7
    }

    pub fn s0() -> Register {
        Register::X8
    }

    pub fn frame_pointer() -> Register {
        Register::X8
    }

    pub fn s1() -> Register {
        Register::X9
    }

    pub fn a0() -> Register {
        Register::X10
    }

    pub fn a1() -> Register {
        Register::X11
    }

    pub fn a2() -> Register {
        Register::X12
    }

    pub fn a3() -> Register {
        Register::X13
    }

    pub fn a4() -> Register {
        Register::X14
    }

    pub fn a5() -> Register {
        Register::X15
    }

    pub fn a6() -> Register {
        Register::X16
    }

    pub fn a7() -> Register {
        Register::X17
    }
}


impl Decode for Register {
    fn decode(icode: u32)-> Result<Register, ISAError> {
        match icode {
            0 => Ok(Register::X0),
            1 => Ok(Register::X1),
            2 => Ok(Register::X2),
            3 => Ok(Register::X3),
            4 => Ok(Register::X4),
            5 => Ok(Register::X5),
            6 => Ok(Register::X6),
            7 => Ok(Register::X7),
            8 => Ok(Register::X8),
            9 => Ok(Register::X9),
            10 => Ok(Register::X10),
            11 => Ok(Register::X11),
            12 => Ok(Register::X12),
            13 => Ok(Register::X13),
            14 => Ok(Register::X14),
            15 => Ok(Register::X15),
            16 => Ok(Register::X16),
            17 => Ok(Register::X17),
            18 => Ok(Register::X18),
            19 => Ok(Register::X19),
            20 => Ok(Register::X20),
            21 => Ok(Register::X21),
            22 => Ok(Register::X22),
            23 => Ok(Register::X23),
            24 => Ok(Register::X24),
            25 => Ok(Register::X25),
            26 => Ok(Register::X26),
            27 => Ok(Register::X27),
            28 => Ok(Register::X28),
            29 => Ok(Register::X29),
            30 => Ok(Register::X30),
            31 => Ok(Register::X31),

            _ => Err(ISAError::RegisterParsingError{ input: icode.to_string() })
        }
    }
}

impl Encode for Register {
    fn encode(&self) -> Result<u32, ISAError> {
        match self {
            Register::X0 => Ok(0),
            Register::X1 => Ok(1),
            Register::X2 => Ok(2),
            Register::X3 => Ok(3),
            Register::X4 => Ok(4),
            Register::X5 => Ok(5),
            Register::X6 => Ok(6),
            Register::X7 => Ok(7),
            Register::X8 => Ok(8),
            Register::X9 => Ok(9),
            Register::X10 => Ok(10),
            Register::X11 => Ok(11),
            Register::X12 => Ok(12),
            Register::X13 => Ok(13),
            Register::X14 => Ok(14),
            Register::X15 => Ok(15),
            Register::X16 => Ok(16),
            Register::X17 => Ok(17),
            Register::X18 => Ok(18),
            Register::X19 => Ok(19),
            Register::X20 => Ok(20),
            Register::X21 => Ok(21),
            Register::X22 => Ok(22),
            Register::X23 => Ok(23),
            Register::X24 => Ok(24),
            Register::X25 => Ok(25),
            Register::X26 => Ok(26),
            Register::X27 => Ok(27),
            Register::X28 => Ok(28),
            Register::X29 => Ok(29),
            Register::X30 => Ok(30),
            Register::X31 => Ok(31),

            _ => Err(ISAError::EncodingError{ input: format!("{:?}", self)})
        }
    }
}

impl FromStr for Register {
    type Err = ISAError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s.to_lowercase().as_ref() {
            "x0"|"zero"                         => Ok(Register::X0),
            "x1"|"ra"|"return_address"          => Ok(Register::X1),
            "x2"|"sp"|"stack_pointer"           => Ok(Register::X2),
            "x3"|"gp"|"global_pointer"          => Ok(Register::X3),
            "x4"|"tp"|"thread_pointer"          => Ok(Register::X4),
            "x5"|"t0"                           => Ok(Register::X5),
            "x6"|"t1"                           => Ok(Register::X6),
            "x7"|"t2"                           => Ok(Register::X7),
            "x8"|"s0"|"fp"|"frame_pointer"      => Ok(Register::X8),
            "x9"|"s1"                           => Ok(Register::X9),
            "x10"|"a0"                          => Ok(Register::X10),
            "x11"|"a1"                          => Ok(Register::X11),
            "x12"|"a2"                          => Ok(Register::X12),
            "x13"|"a3"                          => Ok(Register::X13),
            "x14"|"a4"                          => Ok(Register::X14),
            "x15"|"a5"                          => Ok(Register::X15),
            "x16"|"a6"                          => Ok(Register::X16),
            "x17"|"a7"                          => Ok(Register::X17),
            "x18"|"s2"                          => Ok(Register::X18),
            "x19"|"s3"                          => Ok(Register::X19),
            "x20"|"s4"                          => Ok(Register::X20),
            "x21"|"s5"                          => Ok(Register::X21),
            "x22"|"s6"                          => Ok(Register::X22),
            "x23"|"s7"                          => Ok(Register::X23),
            "x24"|"s8"                          => Ok(Register::X24),
            "x25"|"s9"                          => Ok(Register::X25),
            "x26"|"s10"                         => Ok(Register::X26),
            "x27"|"s11"                         => Ok(Register::X27),
            "x28"|"t3"                          => Ok(Register::X28),
            "x29"|"t4"                          => Ok(Register::X29),
            "x30"|"t5"                          => Ok(Register::X30),
            "x31"|"t6"                          => Ok(Register::X31),
            "pc"|"program_counter"              => Ok(Register::PC),
            
            _ => Err(ISAError::RegisterParsingError { input: s.to_owned() })
        }
    }
}

impl fmt::Display for Register {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Register::X0 => write!(f, "x0/zero"),
            Register::X1 => write!(f, "x1/ra"),
            Register::X2 => write!(f, "x2/sp"),
            Register::X3 => write!(f, "x3/gp"),
            Register::X4 => write!(f, "x4/tp"),
            Register::X5 => write!(f, "x5/t0"),
            Register::X6 => write!(f, "x6/t1"),
            Register::X7 => write!(f, "x7/t2"),
            Register::X8 => write!(f, "x8/s0/fp"),
            Register::X9 => write!(f, "x9/s1"),
            Register::X10 => write!(f, "x10/a0"),
            Register::X11 => write!(f, "x11/a1"),
            Register::X12 => write!(f, "x12/a2"),
            Register::X13 => write!(f, "x13/a3"),
            Register::X14 => write!(f, "x14/a4"),
            Register::X15 => write!(f, "x15/a5"),
            Register::X16 => write!(f, "x16/a6"),
            Register::X17 => write!(f, "x17/a7"),
            Register::X18 => write!(f, "x18/s2"),
            Register::X19 => write!(f, "x19/s3"),
            Register::X20 => write!(f, "x20/s4"),
            Register::X21 => write!(f, "x21/s5"),
            Register::X22 => write!(f, "x22/s6"),
            Register::X23 => write!(f, "x23/s7"),
            Register::X24 => write!(f, "x24/s8"),
            Register::X25 => write!(f, "x25/s9"),
            Register::X26 => write!(f, "x26/s10"),
            Register::X27 => write!(f, "x27/s11"),
            Register::X28 => write!(f, "x28/t3"),
            Register::X29 => write!(f, "x29/t4"),
            Register::X30 => write!(f, "x30/t5"),
            Register::X31 => write!(f, "x31/t6"),
            Register::PC => write!(f, "pc"),
        }
    }
}