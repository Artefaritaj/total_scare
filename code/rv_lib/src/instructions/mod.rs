/*
 * File: mod.rs
 * Project: instructions
 * Created Date: Monday July 1st 2019
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Monday, 16th December 2019 2:12:56 pm
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2019 INRIA
 */

pub mod instruction;
pub mod rv32i;
// pub mod rv64i;
pub mod rvalue;
pub mod register;
pub mod apply;
pub mod offset;
pub mod immediate;
pub mod decoder;
pub mod csr;
pub mod shamt;
pub mod zimm;
pub mod part_selector;

pub use self::register::Register;
pub use self::rvalue::RValue;
pub use self::rv32i::RV32I;
pub use self::instruction::Instruction;
pub use self::apply::*;
pub use self::offset::Offset;
pub use self::immediate::Immediate;
pub use self::csr::Csr;
pub use self::shamt::Shamt;
pub use self::zimm::Zimm;
pub use self::part_selector::InstructionPartSelector;