/*
 * File: add.rs
 * Project: apply
 * Created Date: Tuesday July 2nd 2019
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Wednesday, 19th February 2020 3:43:28 pm
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2019 INRIA
 */

use crate::{
    instructions::*,
    machine::*,
    // error::ISAError,
};

// use std::num::Wrapping;

pub fn add<R: RValue>(rd: &Register, rs1: &Register, rs2: &Register, state: &mut State<R>) -> ApplicationEffect {
    let val1 = state.read_register(rs1);
    let val2 = state.read_register(rs2);

    let added = val1 + val2;
    state.write_register(rd, added);
    trace!("ADD: {} = {:#010x}", rd, added);
    ApplicationEffect::default().set_modified_register(*rd)
}

pub fn addi<R: RValue>(rd: &Register, rs1: &Register, /*off: &Offset,*/ imm: &Immediate, state: &mut State<R>) -> ApplicationEffect {
    let val1 = state.read_register(rs1);

    // let added = val1.offset(off)?;
    let added = val1.to_u32().wrapping_add(imm.0);
    state.write_register(rd, R::from_u32(added));
    trace!("ADDI: {} = {:#010x}", rd, added);
    ApplicationEffect::default().set_modified_register(*rd)
}

pub fn and<R: RValue>(rd: &Register, rs1: &Register, rs2: &Register, state: &mut State<R>) -> ApplicationEffect {
    let val1 = state.read_register(rs1);
    let val2 = state.read_register(rs2);

    let anded = val1 & val2;
    state.write_register(rd, anded);
    trace!("AND: {} = {:#010x}", rd, anded);
    ApplicationEffect::default().set_modified_register(*rd)
}

pub fn andi<R: RValue>(rd: &Register, rs1: &Register, imm: &Immediate, state: &mut State<R>) -> ApplicationEffect {
    let val1 = state.read_register(rs1);

    let anded = val1 & *imm;
    state.write_register(rd, anded);
    trace!("ANDI: {} = {:#010x}", rd, anded);
    ApplicationEffect::default().set_modified_register(*rd)
}

pub fn sub<R: RValue>(rd: &Register, rs1: &Register, rs2: &Register, state: &mut State<R>) -> ApplicationEffect {
    let val1 = state.read_register(rs1).to_signed();
    let val2 = state.read_register(rs2).to_signed();

    let res = val1 - val2;
    state.write_register(rd, R::to_unsigned(&res));
    trace!("SUB: {} = {:#010x}", rd, R::to_unsigned(&res));
    ApplicationEffect::default().set_modified_register(*rd)
}

pub fn or<R: RValue>(rd: &Register, rs1: &Register, rs2: &Register, state: &mut State<R>) -> ApplicationEffect {
    let val1 = state.read_register(rs1);
    let val2 = state.read_register(rs2);

    let res = val1 | val2;
    state.write_register(rd, res);
    trace!("OR: {} = {:#010x}", rd, res);
    ApplicationEffect::default().set_modified_register(*rd)
}

pub fn ori<R: RValue>(rd: &Register, rs1: &Register, imm: &Immediate, state: &mut State<R>) -> ApplicationEffect {
    let val1 = state.read_register(rs1);

    let res = val1 | *imm;
    state.write_register(rd, res);
    trace!("ORI: {} = {:#010x}", rd, res);
    ApplicationEffect::default().set_modified_register(*rd)
}

pub fn xor<R: RValue>(rd: &Register, rs1: &Register, rs2: &Register, state: &mut State<R>) -> ApplicationEffect {
    let val1 = state.read_register(rs1);
    let val2 = state.read_register(rs2);

    let res = val1 ^ val2;
    state.write_register(rd, res);
    trace!("XOR: {} = {:#010x}", rd, res);
    ApplicationEffect::default().set_modified_register(*rd)
}

pub fn xori<R: RValue>(rd: &Register, rs1: &Register, imm: &Immediate, state: &mut State<R>) -> ApplicationEffect {
    let val1 = state.read_register(rs1);

    let res = val1 ^ *imm;
    state.write_register(rd, res);
    trace!("XORI: {} = {:#010x}", rd, res);
    ApplicationEffect::default().set_modified_register(*rd)
}


pub fn sll<R: RValue>(rd: &Register, rs1: &Register, rs2: &Register, state: &mut State<R>) -> ApplicationEffect {
    let val1 = state.read_register(rs1);
    let val2 = state.read_register(rs2);

    let res = val1 << val2.to_usize();
    state.write_register(rd, res);
    trace!("SLL: {} = {:#010x}", rd, res);
    ApplicationEffect::default().set_modified_register(*rd)
}

pub fn slli<R: RValue>(rd: &Register, rs1: &Register, shamt: &Shamt, state: &mut State<R>) -> ApplicationEffect {
    let val1 = state.read_register(rs1);
    let imm = Immediate(shamt.0 as u32);

    let res = val1 << imm;
    state.write_register(rd, res);
    trace!("SLLI: {} = {:#010x}", rd, res);
    ApplicationEffect::default().set_modified_register(*rd)
}

pub fn srl<R: RValue>(rd: &Register, rs1: &Register, rs2: &Register, state: &mut State<R>) -> ApplicationEffect {
    let val1 = state.read_register(rs1);
    let val2 = state.read_register(rs2);

    let res = val1 >> val2.to_usize();
    state.write_register(rd, res);
    trace!("SRL: {} = {:#010x}", rd, res);
    ApplicationEffect::default().set_modified_register(*rd)
}

pub fn srli<R: RValue>(rd: &Register, rs1: &Register, shamt: &Shamt, state: &mut State<R>) -> ApplicationEffect {
    let val1 = state.read_register(rs1);
    let imm = Immediate(shamt.0 as u32);

    let res = val1 >> imm;
    state.write_register(rd, res);
    trace!("SRLI: {} = {:#010x}", rd, res);
    ApplicationEffect::default().set_modified_register(*rd)
}

pub fn sra<R: RValue>(rd: &Register, rs1: &Register, rs2: &Register, state: &mut State<R>) -> ApplicationEffect {
    let val1 = state.read_register(rs1);
    let val2 = state.read_register(rs2);

    let msb =  if val1 & R::from_u64(1 << (R::size() -1)) == R::default() { 0 } else { 1 };

    //shift
    let res_low = val1 >> val2.to_usize();

    //fill with msb if 1
    let res = if msb == 1 {
        let mut full = R::from_u64(-1i64 as u64);


        let sh = R::to_unsigned(&(R::from_u32(R::size() as u32).to_signed() - val2.to_signed()));

        full = full >> sh.to_usize();
        full = full << sh.to_usize();
        res_low | full
    }
    else {
        res_low
    };


    state.write_register(rd, res);
    trace!("SRA: {} = {:#010x}", rd, res);
    ApplicationEffect::default().set_modified_register(*rd)
}

pub fn srai<R: RValue>(rd: &Register, rs1: &Register, shamt: &Shamt, state: &mut State<R>) -> ApplicationEffect {
    let val1 = state.read_register(rs1);
    let val2 = R::from_u32(shamt.0 as u32);

    let msb =  if val1 & R::from_u64(1 << (R::size() -1)) == R::default() { 0 } else { 1 };

    //shift
    let res_low = val1 >> val2.to_usize();

    //fill with msb if 1
    let res = if msb == 1 {
        let mut full = R::from_u64(-1i64 as u64);


        let sh = R::to_unsigned(&(R::from_u32(R::size() as u32).to_signed() - val2.to_signed()));

        full = full >> sh.to_usize();
        full = full << sh.to_usize();
        res_low | full
    }
    else {
        res_low
    };


    state.write_register(rd, res);
    trace!("SRAI: {} = {:#010x}", rd, res);
    ApplicationEffect::default().set_modified_register(*rd)
}

pub fn auipc<R: RValue>(rd: &Register, imm: &Immediate, state: &mut State<R>) -> ApplicationEffect {
    let pc = state.read_register(&Register::PC);
    let res = pc + *imm;

    state.write_register(rd, res);
    trace!("AUIPC: {} = {:#010x}", rd, res);

    ApplicationEffect::default().set_modified_register(*rd)
}

pub fn lui<R: RValue>(rd: &Register, imm: &Immediate, state: &mut State<R>) -> ApplicationEffect {
    let res = R::from_u32(imm.0);

    state.write_register(rd, res);
    trace!("LUI: {} = {:#010x}", rd, res);

    ApplicationEffect::default().set_modified_register(*rd)
}

pub fn slt<R: RValue>(rd: &Register, rs1: &Register, rs2: &Register, state: &mut State<R>) -> ApplicationEffect {
    let val1 = state.read_register(rs1);
    let val2 = state.read_register(rs2);

    let res = if val1.to_signed() < val2.to_signed() {
        R::from_u64(1u64)
    }
    else {
        R::default()//0
    };

    state.write_register(rd, res);
    trace!("SLT: {} = {:#010x}", rd, res);
    ApplicationEffect::default().set_modified_register(*rd)
}

pub fn sltu<R: RValue>(rd: &Register, rs1: &Register, rs2: &Register, state: &mut State<R>) -> ApplicationEffect {
    let val1 = state.read_register(rs1);
    let val2 = state.read_register(rs2);

    let res = if val1 < val2 {
        R::from_u64(1u64)
    }
    else {
        R::default()//0
    };

    state.write_register(rd, res);
    trace!("SLTU: {} = {:#010x}", rd, res);
    ApplicationEffect::default().set_modified_register(*rd)
}

pub fn slti<R: RValue>(rd: &Register, rs1: &Register, imm: &Immediate, state: &mut State<R>) -> ApplicationEffect {
    let val1 = state.read_register(rs1);
    let val2 = R::from_u32(imm.0);

    let res = if val1.to_signed() < val2.to_signed() {
        R::from_u64(1u64)
    }
    else {
        R::default()//0
    };

    state.write_register(rd, res);
    trace!("SLTI: {} = {:#010x}", rd, res);
    ApplicationEffect::default().set_modified_register(*rd)
}

pub fn sltiu<R: RValue>(rd: &Register, rs1: &Register, imm: &Immediate, state: &mut State<R>) -> ApplicationEffect {
    let val1 = state.read_register(rs1);
    let val2 = R::from_u32(imm.0);

    let res = if val1 < val2 {
        R::from_u64(1u64)
    }
    else {
        R::default()//0
    };

    state.write_register(rd, res);
    trace!("STLIU: {} = {:#010x}", rd, res);
    ApplicationEffect::default().set_modified_register(*rd)
}

#[test]
fn test_sra() {
    let mut state: State<u32> = State::new();
    state.write_register(&Register::X1, 0x85555555);
    state.write_register(&Register::X2, 13);

    sra(&Register::X3, &Register::X1, &Register::X2, &mut state);
    assert_eq!(state.read_register(&Register::X3), 0xFFFC2AAA, "{:x} != {:x}", state.read_register(&Register::X3), 0xFFFC2AAAu32);

}