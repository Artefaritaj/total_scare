/*
 * File: control.rs
 * Project: apply
 * Created Date: Thursday July 4th 2019
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Wednesday, 19th February 2020 3:40:25 pm
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2019 INRIA
 */

use crate::{
    machine::*,
    instructions::*,
    error::ISAError,
};

pub fn beq<R: RValue>(rs1: &Register, rs2: &Register, off: &Offset, state: &State<R>) -> Result<ApplicationEffect, ISAError> {
    let val1 = state.read_register(rs1);
    let val2 = state.read_register(rs2);

    if val1 == val2 {
        let pc = state.read_register(&Register::PC);
        let pc = pc.offset(off)?;

        trace!("BEQ: {} == {} => PC = {:#010x}", rs1, rs2, pc);
        Ok(ApplicationEffect::default().set_program_counter(pc.into()))
    }
    else {
        trace!("BEQ: CONDITION FALSE");
        Ok(ApplicationEffect::default())
    }
}

pub fn bne<R: RValue>(rs1: &Register, rs2: &Register, off: &Offset, state: &State<R>) -> Result<ApplicationEffect, ISAError> {
    let val1 = state.read_register(rs1);
    let val2 = state.read_register(rs2);

    if val1 != val2 {
        let pc = state.read_register(&Register::PC);
        let pc = pc.offset(off)?;
        trace!("BNE: {} != {} => PC = {:#010x}", rs1, rs2, pc);
        Ok(ApplicationEffect::default().set_program_counter(pc.into()))
    }
    else {
        trace!("BNE: CONDITION FALSE");
        Ok(ApplicationEffect::default())
    }
}

pub fn bge<R: RValue>(rs1: &Register, rs2: &Register, off: &Offset, state: &State<R>) -> Result<ApplicationEffect, ISAError> {
    let val1 = state.read_register(rs1);
    let val2 = state.read_register(rs2);

    if val1.to_signed() >= val2.to_signed() {
        let pc = state.read_register(&Register::PC);
        let pc = pc.offset(off)?;
        trace!("BGE: {} >= {} => PC = {:#010x}", rs1, rs2, pc);
        Ok(ApplicationEffect::default().set_program_counter(pc.into()))
    }
    else {
        trace!("BGE: CONDITION FALSE");
        Ok(ApplicationEffect::default())
    }
}

pub fn dispatch<R: RValue>(rs1: &Register, off: &Offset, state: &State<R>) -> Result<ApplicationEffect, ISAError> {
    let val1 = state.read_register(rs1);
    let pc = state.read_register(&Register::PC);

    let off_abs = off.0 as u32;

    if val1.to_u32() < off_abs {
        let pc = pc.offset(&Offset(val1.to_u32() as i32))?;
        Ok(ApplicationEffect::default().set_program_counter(pc.into()))
    }
    else {
        let pc = pc.offset(off)?;
        Ok(ApplicationEffect::default().set_program_counter(pc.into()))
    }

    
}

pub fn bgeu<R: RValue>(rs1: &Register, rs2: &Register, off: &Offset, state: &State<R>) -> Result<ApplicationEffect, ISAError> {
    let val1 = state.read_register(rs1);
    let val2 = state.read_register(rs2);

    if val1 >= val2 {
        let pc = state.read_register(&Register::PC);
        let pc = pc.offset(off)?;
        trace!("BGEU: {} >= {} => PC = {:#010x}", rs1, rs2, pc);
        Ok(ApplicationEffect::default().set_program_counter(pc.into()))
    }
    else {
        trace!("BGEU: CONDITION FALSE");
        Ok(ApplicationEffect::default())
    }
}

pub fn blt<R: RValue>(rs1: &Register, rs2: &Register, off: &Offset, state: &State<R>) -> Result<ApplicationEffect, ISAError> {
    let val1 = state.read_register(rs1);
    let val2 = state.read_register(rs2);

    if val1.to_signed() < val2.to_signed() {
        let pc = state.read_register(&Register::PC);
        let pc = pc.offset(off)?;
        trace!("BLT: {} < {} => PC = {:#010x}", rs1, rs2, pc);
        Ok(ApplicationEffect::default().set_program_counter(pc.into()))
    }
    else {
        trace!("BLT: CONDITION FALSE");
        Ok(ApplicationEffect::default())
    }
}

pub fn bltu<R: RValue>(rs1: &Register, rs2: &Register, off: &Offset, state: &State<R>) -> Result<ApplicationEffect, ISAError> {
    let val1 = state.read_register(rs1);
    let val2 = state.read_register(rs2);

    if val1 < val2 {
        let pc = state.read_register(&Register::PC);
        let pc = pc.offset(off)?;
        trace!("BLTU: {} < {} => PC = {:#010x}", rs1, rs2, pc);
        Ok(ApplicationEffect::default().set_program_counter(pc.into()))
    }
    else {
        trace!("BLTU: CONDITION FALSE");
        Ok(ApplicationEffect::default())
    }
}

pub fn jal<R: RValue>(rd: &Register, off: &Offset, state: &mut State<R>, inst_size: InstructionSize) -> Result<ApplicationEffect, ISAError> {
    let pc2save = state.read_register(&Register::PC);
    let pc2save = pc2save.offset(&inst_size.get_offset())?;
    state.write_register(rd, pc2save);

    let pc = state.read_register(&Register::PC);
    let pc_off = pc.offset(off)?;

    trace!("JAL: PC = {:#010x}, RETURN in {} = {:#010x}", pc_off, rd, pc2save);
    Ok(ApplicationEffect::default().set_program_counter(pc_off.into()))
}

pub fn return_inst<R: RValue>(state: &State<R>) -> Result<ApplicationEffect, ISAError> {
    let add = state.read_register(&Register::X2);//bound to SP register
    trace!("RETURN: PC = {:#010x}", add);
    Ok(ApplicationEffect::default().set_program_counter(add.into()))
}

pub fn jalr<R: RValue>(rd: &Register, rs1: &Register, off: &Offset, state: &mut State<R>, inst_size: InstructionSize) -> Result<ApplicationEffect, ISAError> {
    let pc2save = state.read_register(&Register::PC);
    let pc2save = pc2save.offset(&inst_size.get_offset())?;
    state.write_register(rd, pc2save);

    let add = state.read_register(rs1);
    let add_off = add.offset(off)?;
    trace!("JALR: PC = {:#010x}, RETURN in {} = {:#010x}", add_off, rd, pc2save);
    Ok(ApplicationEffect::default().set_program_counter(add_off.into()))
}