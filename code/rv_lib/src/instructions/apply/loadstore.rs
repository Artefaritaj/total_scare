/*
 * File: load.rs
 * Project: apply
 * Created Date: Wednesday July 3rd 2019
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Tuesday, 20th April 2021 5:05:37 pm
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2019 INRIA
 */

use crate::{
    instructions::*,
    machine::*,
    error::ISAError,
};

pub fn lb<R: RValue, MEM: Memory>(rd: &Register, rs1: &Register, off: &Offset, state: &mut State<R>, mem: &MEM) -> Result<ApplicationEffect, ISAError> {
    let val1 = state.read_register(rs1);
    let address = val1.offset(off)?;

    let res: u8 = mem.readu8(address.into())?;
    state.write_register(rd, R::from_u32(res as u32));
    trace!("LB: {} = {:#010x} <= [{:#010x}]", rd, R::from_u32(res as u32), address);
    Ok(ApplicationEffect::default().set_modified_register(*rd))
}

pub fn lbu<R: RValue, MEM: Memory>(rd: &Register, rs1: &Register, off: &Offset, state: &mut State<R>, mem: &MEM) -> Result<ApplicationEffect, ISAError> {
    let val1 = state.read_register(rs1);
    let address = val1.offset(off)?;

    let res = mem.readu8(address.into())?;
    state.write_register(rd, R::from_u32(res as u32));
    trace!("LBU: {} = {:#010x} <= [{:#010x}]", rd, R::from_u32(res as u32), address);
    Ok(ApplicationEffect::default().set_modified_register(*rd))
}

pub fn lh<R: RValue, MEM: Memory>(rd: &Register, rs1: &Register, off: &Offset, state: &mut State<R>, mem: &MEM) -> Result<ApplicationEffect, ISAError> {
    let val1 = state.read_register(rs1);
    let address = val1.offset(off)?;

    let res: u16 = mem.readu16(address.into())?;
    state.write_register(rd, R::from_u32(res as u32));
    trace!("LH: {} = {:#010x} <= [{:#010x}]", rd, R::from_u32(res as u32), address);
    Ok(ApplicationEffect::default().set_modified_register(*rd))
}

pub fn lhu<R: RValue, MEM: Memory>(rd: &Register, rs1: &Register, off: &Offset, state: &mut State<R>, mem: &MEM) -> Result<ApplicationEffect, ISAError> {
    let val1 = state.read_register(rs1);
    let address = val1.offset(off)?;

    let res = mem.readu16(address.into())?;
    state.write_register(rd, R::from_u32(res as u32));
    trace!("LHU: {} = {:#010x} <= [{:#010x}]", rd, R::from_u32(res as u32), address);
    Ok(ApplicationEffect::default().set_modified_register(*rd))
}


pub fn lw<R: RValue, MEM: Memory>(rd: &Register, rs1: &Register, off: &Offset, state: &mut State<R>, mem: &MEM) -> Result<ApplicationEffect, ISAError> {
    let val1 = state.read_register(rs1);
    let address = val1.offset(off)?;

    let res: u32 = mem.readu32(address.into())?;
    state.write_register(rd, R::from_u32(res as u32));
    trace!("LW: {} = {:#010x} <= [{:#010x}]", rd, R::from_u32(res as u32), address);
    Ok(ApplicationEffect::default().set_modified_register(*rd))
}

pub fn lwu<R: RValue, MEM: Memory>(rd: &Register, rs1: &Register, off: &Offset, state: &mut State<R>, mem: &MEM) -> Result<ApplicationEffect, ISAError> {
    let val1 = state.read_register(rs1);
    let address = val1.offset(off)?;

    let res = mem.readu32(address.into())?;
    state.write_register(rd, R::from_u32(res as u32));
    trace!("LWU: {} = {:#010x} <= [{:#010x}]", rd, R::from_u32(res as u32), address);
    Ok(ApplicationEffect::default().set_modified_register(*rd))
}

pub fn trace_out<R: RValue>(rs1: &Register, state: &mut State<R>) -> Result<ApplicationEffect, ISAError> {
    let val1 = state.read_register(rs1);
    state.io.add_output(val1);
    trace!("TRACEOUT: {} = {:#010x}", rs1, val1);
    Ok(ApplicationEffect::default())
}

pub fn sb<R: RValue, MEM: Memory>(rs1: &Register, rs2: &Register, off: &Offset, state: &mut State<R>, mem: &mut MEM) -> Result<ApplicationEffect, ISAError> {
    let val1 = state.read_register(rs1);
    let address = val1.offset(off)?;

    let val2 = state.read_register(rs2);
    mem.writeu8(address.into(), val2.to_u8())?;

    trace!("SB: [{:#010x}] <= {:#010x}", address, val2);
    Ok(ApplicationEffect::default())
}

pub fn sh<R: RValue, MEM: Memory>(rs1: &Register, rs2: &Register, off: &Offset, state: &mut State<R>, mem: &mut MEM) -> Result<ApplicationEffect, ISAError> {
    let val1 = state.read_register(rs1);
    let address = val1.offset(off)?;

    let val2 = state.read_register(rs2);
    mem.writeu16(address.into(), val2.to_u16())?;

    trace!("SH: [{:#010x}] <= {:#010x}", address, val2);
    Ok(ApplicationEffect::default())
}

pub fn sw<R: RValue, MEM: Memory>(rs1: &Register, rs2: &Register, off: &Offset, state: &mut State<R>, mem: &mut MEM) -> Result<ApplicationEffect, ISAError> {
    let val1 = state.read_register(rs1);
    let address = val1.offset(off)?;

    let val2 = state.read_register(rs2);
    mem.writeu32(address.into(), val2.to_u32())?;
    trace!("SW: [{:#010x}] <= {:#010x}", address, val2);
    Ok(ApplicationEffect::default())
}


#[test]
fn test_lw() {
    let mut state: State<u32> = State::new();
    let mut mem = MemoryMap::default();

    assert!(mem.writeu32(MemoryAddress(0), (-1i32) as u32).is_ok());
    // dbg!(&mem);
    // println!("{}", &mem);

    state.write_register(&Register::X1, 0);
    assert!(lw(&Register::X2, &Register::X1, &Offset(0), &mut state, &mem).is_ok());

    // println!("X2 = 0x{:x}", state.read_register(&Register::X2));
    assert_eq!(state.read_register(&Register::X2), 0xFFFFFFFF);
}