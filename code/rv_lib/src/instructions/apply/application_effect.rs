/*
 * File: application_effect.rs
 * Project: apply
 * Created Date: Tuesday July 2nd 2019
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Tuesday, 20th April 2021 4:53:07 pm
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2019 INRIA
 */

use crate::machine::{MemoryAddress};
use crate::instructions::*;



#[derive(Debug,Clone)]
pub struct ApplicationEffect {
    pub divert_program_counter: Option<MemoryAddress>,
    pub modified_register: Option<Register>,
}

impl Default for ApplicationEffect {
    fn default() -> ApplicationEffect {
        ApplicationEffect { 
            divert_program_counter: None,
            modified_register:      None,
        }
    }
}


impl ApplicationEffect {
    pub fn set_program_counter(mut self, address: MemoryAddress) -> ApplicationEffect {
        self.divert_program_counter = Some(address);
        self
    }

    pub fn set_modified_register(mut self, reg: Register) -> ApplicationEffect {
        self.modified_register = Some(reg);
        self
    }

    // pub fn must_halt(mut self) -> ApplicationEffect {
    //     self.must_halt = true;
    //     self
    // }
}