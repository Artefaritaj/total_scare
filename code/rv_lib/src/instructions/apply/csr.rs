/*
 * File: csr.rs
 * Project: apply
 * Created Date: Monday July 22nd 2019
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Tuesday, 20th April 2021 5:42:08 pm
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2019 INRIA
 */

use crate::{
    machine::*,
    instructions::*,
    error::ISAError,
};

use crate::machine::PRNG;

pub fn csrrc<R: RValue>(rd: &Register, rs1: &Register, csr: &Csr, state: &mut State<R>) -> Result<ApplicationEffect, ISAError> {
    let val1 = state.read_register(rs1);

    let t = state.csr.read32_csr(*csr)?;
    state.csr.write32_csr(*csr, t & !val1.to_u32())?;

    let tu32 = R::from_u32(t);
    state.write_register(rd, tu32);
    if csr == &PRNG {
        state.io.add_input(tu32);
    }
    
    Ok(ApplicationEffect::default().set_modified_register(*rd))
}

pub fn csrrci<R: RValue>(rd: &Register, zimm: &Zimm, csr: &Csr, state: &mut State<R>) -> Result<ApplicationEffect, ISAError> {
    let val1 = zimm.0;

    let t = state.csr.read32_csr(*csr)?;
    state.csr.write32_csr(*csr, t & !(val1 as u32))?;

    let tu32 = R::from_u32(t);
    state.write_register(rd, tu32);
    if csr == &PRNG {
        state.io.add_input(tu32);
    }
    
    Ok(ApplicationEffect::default().set_modified_register(*rd))
}

pub fn csrrs<R: RValue>(rd: &Register, rs1: &Register, csr: &Csr, state: &mut State<R>) -> Result<ApplicationEffect, ISAError> {
    let val1 = state.read_register(rs1);

    let t = state.csr.read32_csr(*csr)?;
    state.csr.write32_csr(*csr, t | val1.to_u32())?;

    trace!("CSRRS[{}]: {} = {:#010x}", csr.0, rd, R::from_u32(t));
    let tu32 = R::from_u32(t);
    state.write_register(rd, tu32);
    if csr == &PRNG {
        state.io.add_input(tu32);
    }
    
    Ok(ApplicationEffect::default().set_modified_register(*rd))
}

pub fn csrrsi<R: RValue>(rd: &Register, zimm: &Zimm, csr: &Csr, state: &mut State<R>) -> Result<ApplicationEffect, ISAError> {
    let val1 = zimm.0;

    let t = state.csr.read32_csr(*csr)?;
    state.csr.write32_csr(*csr, t | (val1 as u32))?;

    let tu32 = R::from_u32(t);
    state.write_register(rd, tu32);
    if csr == &PRNG {
        state.io.add_input(tu32);
    }
    
    Ok(ApplicationEffect::default().set_modified_register(*rd))
}

pub fn csrrw<R: RValue>(rd: &Register, rs1: &Register, csr: &Csr, state: &mut State<R>) -> Result<ApplicationEffect, ISAError> {
    let val1 = state.read_register(rs1);
    let t = state.csr.write32_csr(*csr, val1.to_u32())?;
    state.write_register(rd, R::from_u32(t));
    
    Ok(ApplicationEffect::default().set_modified_register(*rd))
}

pub fn csrrwi<R: RValue>(rd: &Register, zimm: &Zimm, csr: &Csr, state: &mut State<R>) -> Result<ApplicationEffect, ISAError> {
    let val1 = zimm.0;
    let t = state.csr.write32_csr(*csr, val1 as u32)?;
    state.write_register(rd, R::from_u32(t));
    
    Ok(ApplicationEffect::default().set_modified_register(*rd))
}