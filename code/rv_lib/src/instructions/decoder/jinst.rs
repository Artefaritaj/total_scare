/*
 * File: jinst.rs
 * Project: decoder
 * Created Date: Wednesday July 17th 2019
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Tuesday, 19th November 2019 2:32:14 pm
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2019 INRIA
 */

use crate::{
    instructions::*,
    error::*,
    instructions::decoder::*,
    // instructions::decoder::helpers::*,
};

// use std::convert::TryFrom;

#[derive(Debug,Clone,PartialEq,Eq,PartialOrd,Ord,Hash,Copy)]
pub enum JTag {
    Jal,
}

impl Decode for JTag {
    fn decode(icode: u32) -> Result<JTag, ISAError> {
        let opcode: u8 = (icode & 0x7C) as u8;

        match opcode {
            0x6C    => Ok(JTag::Jal),
            _       => Err(ISAError::InvalidInstruction { icode })
        }
    }
}

impl Encode for JTag {
    fn encode(&self) -> Result<u32, ISAError> {
        match self {
            JTag::Jal => Ok(0x6C),
        }
    }
}

#[derive(Debug,Clone)]
pub struct JInst {
    pub rd: Register,
    pub off: Offset,
    pub tag: JTag
}


impl Decode for JInst {
    fn decode(icode: u32) -> Result<JInst, ISAError> {
        let rd_val = (icode >> 7) & 0x1F;
        let rd = Register::decode(rd_val)?;
        
        let imm_sign = (icode & 0x80000000) >> 11;
        let imm_12 = icode & 0xFF000;
        let imm_11 = (icode & 0x100000) >> 9;
        let imm_1 = (icode & 0x7FE00000) >> 20;

        // println!("imm_sign {:x} | imm_12 {:x} | imm_11 {:x} | imm_1 {:x}", imm_sign, imm_12, imm_11, imm_1);

        let off = Offset::sign_extend(imm_sign | imm_12 | imm_11 | imm_1, imm_sign, 20) ;


        let tag = JTag::decode(icode)?;

        Ok(JInst {
            rd,
            off,
            tag
        })
        
    }
}

impl Encode for JInst {
    fn encode(&self) -> Result<u32, ISAError> {
        let source = self.off.0 as u32;

        let s12 = (source >> 12) & 0xFF;
        let s11 = (source >> 11) & 1;
        let s1  = (source >> 1) & 0x3FF;
        let s20 = (source >> 20) & 1;

        let imm = (s12 << 12) | (s11 << 20) | (s1 << 21) | (s20 << 31);
        Ok(self.tag.encode()? | (self.rd.encode()? << 7) | imm)  
    }
}

#[test]
fn test_jinst() {
    let jal_icode = 0x4fc010ec;
    let jal_jinst = JInst::decode(jal_icode).unwrap();

    assert_eq!(jal_jinst.tag, JTag::Jal);
    assert_eq!(jal_jinst.rd, Register::return_address());
    assert_eq!(jal_jinst.off, Offset(0x14FC));
    let jal_reencode = jal_jinst.encode().unwrap();
    assert_eq!(jal_reencode, jal_icode);

    let jal_icode = 0xf01ff0ec;
    let jal_jinst = JInst::decode(jal_icode).unwrap();

    assert_eq!(jal_jinst.tag, JTag::Jal);
    assert_eq!(jal_jinst.rd, Register::return_address());
    assert_eq!(jal_jinst.off, Offset(-0x100));
    let jal_reencode = jal_jinst.encode().unwrap();
    assert_eq!(jal_reencode, jal_icode);
}