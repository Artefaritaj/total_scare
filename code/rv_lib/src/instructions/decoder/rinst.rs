/*
 * File: uinst.rs
 * Project: decoder
 * Created Date: Wednesday July 17th 2019
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Tuesday, 19th November 2019 2:46:45 pm
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2019 INRIA
 */

use crate::{
    instructions::*,
    error::*,
    instructions::decoder::*,
    instructions::decoder::helpers::*,
};

// use std::convert::TryFrom;

#[derive(Debug,Clone,PartialEq,Eq,PartialOrd,Ord,Hash,Copy)]
pub enum RTag {
    Add,
    Sub,
    Sll,
    Slt,
    Sltu,
    Xor,
    Srl,
    Sra,
    Or,
    And,
}

impl Encode for RTag {
    fn encode(&self) -> Result<u32, ISAError> {
        let (opcode, fun12, fun25) = match self {
            RTag::Add   => (0x30, 0, 0),
            RTag::Sub   => (0x30, 0, 0x20),
            RTag::Sll   => (0x30, 1, 0),
            RTag::Slt   => (0x30, 2, 0),
            RTag::Sltu  => (0x30, 3, 0),
            RTag::Xor   => (0x30, 4, 0),
            RTag::Srl   => (0x30, 5, 0),
            RTag::Sra   => (0x30, 5, 0x20),
            RTag::Or    => (0x30, 6, 0),
            RTag::And   => (0x30, 7, 0),
        };

        Ok(opcode | (fun12 << 12) | (fun25 << 25))
    }
}

impl Decode for RTag {
    fn decode(icode: u32) -> Result<RTag, ISAError> {
        let opcode: u8 = (icode & OPCODE_MASK) as u8;
        let func1 = (icode & 0x7000) >> 12;
        let func2 = (icode & 0xFE000000) >> 25;
        let func = func2 << 4 | func1;

        if opcode != 0x30 {
            return Err(ISAError::InvalidInstruction { icode });
        }

        match func {
            0x0     => Ok(RTag::Add),
            0x200   => Ok(RTag::Sub),
            0x1     => Ok(RTag::Sll),
            0x2     => Ok(RTag::Slt),
            0x3     => Ok(RTag::Sltu),
            0x4     => Ok(RTag::Xor),
            0x5     => Ok(RTag::Srl),
            0x205   => Ok(RTag::Sra),
            0x6     => Ok(RTag::Or),
            0x7     => Ok(RTag::And), 
            
            _       => Err(ISAError::InvalidInstruction { icode })
        }
    }
}

#[derive(Debug,Clone)]
pub struct RInst {
    pub rd: Register,
    pub rs1: Register,
    pub rs2: Register,
    pub tag: RTag
}


impl Decode for RInst {
    fn decode(icode: u32) -> Result<RInst, ISAError> {
        let rd_val = (icode >> 7) & 0x1F;
        let rd = Register::decode(rd_val)?;

        let rs1_val = (icode >> 15) & 0x1F;
        let rs1 = Register::decode(rs1_val)?;

        let rs2_val = (icode >> 20) & 0x1F;
        let rs2 = Register::decode(rs2_val)?;
    
        let tag = RTag::decode(icode)?;

        Ok(RInst {
            rd,
            rs1,
            rs2,
            tag
        })
        
    }
}

impl Encode for RInst {
    fn encode(&self) -> Result<u32, ISAError> {
        Ok(self.tag.encode()? | (self.rd.encode()? << 7) | (self.rs1.encode()? << 15) | (self.rs2.encode()? << 20))
    }
}

#[test]
fn test_rinst() {
    let xor_icode = 8865584;
    let xor_instr = RInst::decode(xor_icode).unwrap();
    assert_eq!(xor_instr.tag, RTag::Xor);
    assert_eq!(xor_instr.rd, Register::X14);
    assert_eq!(xor_instr.rs1, Register::X14);
    assert_eq!(xor_instr.rs2, Register::X8);

    let xor_reencode = xor_instr.encode().unwrap();
    assert_eq!(xor_icode, xor_reencode);
}