/*
 * File: instruction_encoding.rs
 * Project: decoder
 * Created Date: Wednesday July 17th 2019
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Tuesday, 19th November 2019 3:01:46 pm
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2019 INRIA
 */

use crate::{
    instructions::{
        // *,
        decoder::*,
        decoder::helpers::*,
    },
    error::*,
};

#[derive(Debug,Clone,Copy,PartialEq,Eq,PartialOrd,Ord,Hash)]
pub enum EncodingType {
    U,
    J,
    I,
    B,
    S,
    R
}

impl EncodingType {
    pub fn from_opcode(opcode: u32) -> Result<EncodingType, ISAError> {
        let opcode = opcode & OPCODE_MASK; //shadow the opcode

        match opcode {
            0x34 | 0x14         => Ok(EncodingType::U),
            0x6C                => Ok(EncodingType::J),
            0x64 | 0x0 | 0x10 | 0xC | 0x70      => Ok(EncodingType::I),
            0x60                => Ok(EncodingType::B),
            0x30                => Ok(EncodingType::R),
            0x20                => Ok(EncodingType::S),

            _                   => Err(ISAError::InvalidInstruction { icode: opcode })
        }
    }
}

#[derive(Debug,Clone)]
pub enum InstructionEncoding {
    U(UInst),
    J(JInst),
    R(RInst),
    I(IInst),
    B(BInst),
    S(SInst),
}

impl Decode for InstructionEncoding {
    fn decode(icode: u32) -> Result<InstructionEncoding, ISAError> {
        let encoding_type = EncodingType::from_opcode(icode)?;
        match encoding_type {
            EncodingType::U     => Ok(InstructionEncoding::U(UInst::decode(icode)?)),
            EncodingType::J     => Ok(InstructionEncoding::J(JInst::decode(icode)?)),
            EncodingType::R     => Ok(InstructionEncoding::R(RInst::decode(icode)?)),
            EncodingType::I     => Ok(InstructionEncoding::I(IInst::decode(icode)?)),
            EncodingType::B     => Ok(InstructionEncoding::B(BInst::decode(icode)?)),
            EncodingType::S     => Ok(InstructionEncoding::S(SInst::decode(icode)?)),
        }
    }
}

impl Encode for InstructionEncoding {
    fn encode(&self) -> Result<u32, ISAError> {
        match self {
            InstructionEncoding::U(uinst)   => uinst.encode(),
            InstructionEncoding::J(jinst)   => jinst.encode(),
            InstructionEncoding::R(rinst)   => rinst.encode(),
            InstructionEncoding::I(iinst)   => iinst.encode(),
            InstructionEncoding::B(binst)   => binst.encode(),
            InstructionEncoding::S(sinst)   => sinst.encode(),
        }
    }
}