# Encoding for new jumps instructions

## Return

On the model of ecall and ebreak.
Return is 0x80000070.

```
return;
```

## Dispatch

```
dispatch x1, 4
```

Jump to PC + x1 if x1 < 4, else jump to pc + 4 (error case).

On the model of a branch instruction (B encoding). The literal value is in the immediate. rs2 is not used.

```
imm[12|10:5] XXXX rs1(4bits) 010 imm[4:1|11] 11000XX 
```