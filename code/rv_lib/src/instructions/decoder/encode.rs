/*
 * File: decode.rs
 * Project: decoder
 * Created Date: Tuesday November 19th 2019
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Tuesday, 19th November 2019 11:57:39 am
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2019 INRIA
 */

use crate::error::*;

pub trait Encode {
    fn encode(&self)-> Result<u32, ISAError>;
}