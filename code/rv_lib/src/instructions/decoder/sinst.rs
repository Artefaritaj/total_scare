/*
 * File: uinst.rs
 * Project: decoder
 * Created Date: Wednesday July 17th 2019
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Tuesday, 19th November 2019 2:52:32 pm
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2019 INRIA
 */

use crate::{
    instructions::*,
    error::*,
    instructions::decoder::*,
    instructions::decoder::helpers::*,
};

// use std::convert::TryFrom;

#[derive(Debug,Clone,PartialEq,Eq,PartialOrd,Ord,Hash,Copy)]
pub enum STag {
    Sb,
    Sh,
    Sw,
}

impl Decode for STag {
    fn decode(icode: u32) -> Result<STag, ISAError> {
        let opcode: u8 = (icode & OPCODE_MASK) as u8;
        let func3 = (icode & 0x7000) >> 12;

        if opcode != 0x20 {
            return Err(ISAError::InvalidInstruction { icode });
        }

        match func3 {
            0x0     => Ok(STag::Sb),
            0x1     => Ok(STag::Sh),
            0x2     => Ok(STag::Sw),
            _       => Err(ISAError::InvalidInstruction { icode })
        }
    }
}

impl Encode for STag {
    fn encode(&self) -> Result<u32, ISAError> {
        let (opcode, fun12) = match self {
            STag::Sb    => (0x20, 0),
            STag::Sh    => (0x20, 1),
            STag::Sw    => (0x20, 2)
        };

        Ok(opcode | (fun12 << 12))
    }
}

#[derive(Debug,Clone)]
pub struct SInst {
    pub rs1: Register,
    pub rs2: Register,
    pub off: Offset,
    pub tag: STag
}


impl Decode for SInst {
    fn decode(icode: u32) -> Result<SInst, ISAError> {
        let rs1_val = (icode >> 15) & 0x1F;
        let rs1 = Register::decode(rs1_val)?;

        let rs2_val = (icode >> 20) & 0x1F;
        let rs2 = Register::decode(rs2_val)?;

        let imm_sign = icode & 0x80000000;
        let imm5 = (icode & 0xFE000000) >> 20;
        let imm0 = (icode & 0xF80) >> 7;

        let off = Offset::sign_extend(imm5 | imm0, imm_sign, 11);
    
        let tag = STag::decode(icode)?;

        Ok(SInst {
            rs1,
            rs2,
            off,
            tag
        })
        
    }
}

impl Encode for SInst {
    fn encode(&self) -> Result<u32, ISAError> {
        let source = self.off.0 as u32;
        let imm7 = source & 0x1F;
        let imm25 = (source >> 5) & 0x7F;

        Ok(self.tag.encode()? | (self.rs2.encode()? << 20) | (self.rs1.encode()? << 15) | (imm7 << 7) | (imm25 << 25))
    }
}

#[test]
fn test_binst() {
    let sw_icode = 0x01912420;
    let sw_inst = SInst::decode(sw_icode).unwrap();

    assert_eq!(sw_inst.tag, STag::Sw);
    assert_eq!(sw_inst.rs1, Register::X2);
    assert_eq!(sw_inst.rs2, Register::X25);
    assert_eq!(sw_inst.off, Offset(8));

    let sw_reencode = sw_inst.encode().unwrap();
    assert_eq!(sw_reencode, sw_icode);
}