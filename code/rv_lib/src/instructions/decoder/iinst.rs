/*
 * File: uinst.rs
 * Project: decoder
 * Created Date: Wednesday July 17th 2019
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Wednesday, 21st April 2021 10:49:47 am
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2019 INRIA
 */

use crate::{
    instructions::*,
    error::*,
    instructions::decoder::*,
    instructions::decoder::helpers::*,
};

// use std::convert::TryFrom;

#[derive(Debug,Clone,PartialEq,Eq,PartialOrd,Ord,Hash,Copy)]
pub enum ITag {
    Jalr,
    Lb,
    Lh,
    Lw,
    Lbu,
    Lhu,
    Lwu,
    Addi,
    Slti,
    Sltiu,
    Xori,
    Ori,
    Andi,
    Slli,
    Srli,
    Srai,
    Fence,
    FenceI,
    Ecall,
    Ebreak,
    Csrrw,
    Csrrs,
    Csrrc,
    Csrrwi,
    Csrrsi,
    Csrrci,
}

impl Decode for ITag {
    fn decode(icode: u32) -> Result<ITag, ISAError> {
        let opcode: u8 = (icode & OPCODE_MASK) as u8;
        let func3 = (icode & 0x7000) >> 12;
        let func7 = (icode & 0xFE000000) >> 25;

        

        match opcode {
            0x64        => {
                Ok(ITag::Jalr)
            },
            0x0         => {//load
                match func3 {
                    0x0     => Ok(ITag::Lb),
                    0x1     => Ok(ITag::Lh),
                    0x2     => Ok(ITag::Lw),
                    0x4     => Ok(ITag::Lbu),
                    0x5     => Ok(ITag::Lhu),
                    0x6     => Ok(ITag::Lwu),
                    _       => Err(ISAError::InvalidInstruction { icode })
                }
            },
            0x10        => {
                match func3 {
                    0x0     => Ok(ITag::Addi),
                    0x2     => Ok(ITag::Slti),
                    0x3     => Ok(ITag::Sltiu),
                    0x4     => Ok(ITag::Xori),
                    0x6     => Ok(ITag::Ori),
                    0x7     => Ok(ITag::Andi),

                    0x1     => Ok(ITag::Slli),
                    0x5     => {
                        match func7 {
                            0x0     => Ok(ITag::Srli),
                            0x20    => Ok(ITag::Srai),
                            _       => Err(ISAError::InvalidInstruction { icode })
                        }
                    },
                    _       => Err(ISAError::InvalidInstruction { icode })
                }
            },

            0x0C        => {
                match func3 {
                    0x0     => Ok(ITag::Fence),
                    0x1     => Ok(ITag::FenceI),
                    _       => Err(ISAError::InvalidInstruction { icode })
                }
            },

            0x70        => {
                match func3 {
                    0x0     => {
                        match (icode & (0xFFF << 20)) >> 20 {
                            0       => Ok(ITag::Ecall),
                            1       => Ok(ITag::Ebreak),
                            _       => Err(ISAError::InvalidInstruction { icode })
                        }
                    }
                    0x1     => Ok(ITag::Csrrw),
                    0x2     => Ok(ITag::Csrrs),
                    0x3     => Ok(ITag::Csrrc),
                    0x5     => Ok(ITag::Csrrwi),
                    0x6     => Ok(ITag::Csrrsi),
                    0x7     => Ok(ITag::Csrrci),
                    _       => Err(ISAError::InvalidInstruction { icode })
                }
            },

            _           => Err(ISAError::InvalidInstruction { icode })
        }
    }
}

impl Encode for ITag {
    fn encode(&self) -> Result<u32, ISAError> {
        let (opcode, fun12, fun20) = match self {

            ITag::Jalr      => (0x64, 0, 0),

            //Load
            ITag::Lb        => (0, 0, 0),
            ITag::Lh        => (0, 1, 0),
            ITag::Lw        => (0, 2, 0),
            ITag::Lbu       => (0, 4, 0),
            ITag::Lhu       => (0, 5, 0),
            ITag::Lwu       => (0, 6, 0),

            //Arithmetic
            ITag::Addi      => (0x10, 0, 0),
            ITag::Slti      => (0x10, 2, 0),
            ITag::Sltiu     => (0x10, 3, 0),
            ITag::Xori      => (0x10, 4, 0),
            ITag::Ori       => (0x10, 6, 0),
            ITag::Andi      => (0x10, 7, 0),
            ITag::Slli      => (0x10, 1, 0),
            ITag::Srli      => (0x10, 5, 0),
            ITag::Srai      => (0x10, 5, 0x400),

            //Misc
            ITag::Fence     => (0xC, 0, 0),
            ITag::FenceI    => (0xC, 1, 0),
            ITag::Ecall     => (0x70, 0, 0),
            ITag::Ebreak    => (0x70, 0, 1),

            //Csr
            ITag::Csrrw     => (0x70, 1, 0),
            ITag::Csrrs     => (0x70, 2, 0),
            ITag::Csrrc     => (0x70, 3, 0),
            ITag::Csrrwi    => (0x70, 5, 0),
            ITag::Csrrsi    => (0x70, 6, 0),
            ITag::Csrrci    => (0x70, 7, 0),
        };

        Ok(opcode | (fun12 << 12) | (fun20 << 20))
    }
}

#[derive(Debug,Clone,PartialEq,Eq,PartialOrd,Ord,Hash)]
pub enum IPayload {
    Immediate(Immediate),
    Offset(Offset),
    Fence(u8,u8),//(pred, succ)
    Csr(Csr),
    Shamt(Shamt),
}

impl Encode for IPayload {
    fn encode(&self) -> Result<u32, ISAError> {
        match self {
            IPayload::Immediate(imm) => {
                Ok(imm.0 << 20)
            },
            IPayload::Offset(off) => {
                Ok((off.0 as u32) << 20)
            },
            IPayload::Fence(pred, succ) => {
                Ok(((*succ as u32) << 20) | ((*pred as u32) << 24))
            },
            IPayload::Csr(csr) => {
                Ok((csr.0 as u32) << 20)
            },
            IPayload::Shamt(shamt) => {
                Ok((shamt.0 as u32) << 20)
            },
            // _=> Ok(0)
        }
    }
}

impl IPayload {

    pub fn from_u32(val: u32, tag: ITag) -> IPayload {
        match tag {
            ITag::Lb | ITag::Lbu | ITag::Jalr | ITag::Lh | ITag::Lhu | ITag::Lw | ITag::Lwu => {
                IPayload::Offset(Offset::sign_extend(val, val & 0x800, 11))
            },
            ITag::Fence => IPayload::Fence(((val >> 4) & 0xF) as u8, (val & 0xF) as u8),
            ITag::Csrrc | ITag::Csrrci | ITag::Csrrs | ITag::Csrrsi | ITag::Csrrw | ITag::Csrrwi => {
                IPayload::Csr(Csr(val as u16))
            },
            ITag::Slli | ITag::Srli | ITag::Srai => IPayload::Shamt(Shamt((val & 0x1F) as u8)),
            // ITag::Addi => IPayload::Offset(Offset::sign_extend(val, val & 0x800, 11)),

             _ => IPayload::Immediate(Immediate::sign_extend(val, val & 0x800, 11))
        }
    }

    pub fn to_imm(self) -> Immediate {
        if let IPayload::Immediate(imm) = self {
            imm
        }
        else {
            unreachable!();
        }
    }

    pub fn to_off(self) -> Offset {
        if let IPayload::Offset(off) = self {
            off
        }
        else {
            unreachable!("{:?}", self);
        }
    }

    pub fn to_csr(self) -> Csr {
        if let IPayload::Csr(csr) = self {
            csr
        }
        else {
            unreachable!();
        }
    }

    pub fn to_shamt(self) -> Shamt {
        if let IPayload::Shamt(shamt) = self {
            shamt
        }
        else {
            unreachable!();
        }
    }

    pub fn to_fence(self) -> (u8, u8) {
        if let IPayload::Fence(pred, succ) = self {
            (pred, succ)
        }
        else {
            unreachable!();
        }
    }
}

#[derive(Debug,Clone)]
pub struct IInst {
    pub rd: Register,
    pub rs1: Register,
    pub zimm: Zimm,
    pub payload: IPayload,
    pub tag: ITag
}


impl Decode for IInst {
    fn decode(icode: u32) -> Result<IInst, ISAError> {
        let rd_val = (icode >> 7) & 0x1F;
        let rd = Register::decode(rd_val)?;

        let rs1_val = (icode >> 15) & 0x1F;
        let rs1 = Register::decode(rs1_val)?;
        let tag = ITag::decode(icode)?;

        let payload = IPayload::from_u32(icode >> 20, tag);
    

        Ok(IInst {
            rd,
            rs1,
            zimm: Zimm(rs1_val as u8),
            payload,
            tag
        })
        
    }
}

impl Encode for IInst {
    fn encode(&self) -> Result<u32, ISAError> {
        Ok(self.payload.encode()? | self.tag.encode()? | (self.rs1.encode()? << 15) | (self.rd.encode()? << 7) | ((self.zimm.0 as u32) << 15))
    }
}

#[test]
fn test_iinst() {
    // let jalr_icode = 0x000900e7;
    // let jalr_iinst = IInst::decode(jalr_icode).unwrap();

    // assert_eq!(jalr_iinst.tag, ITag::Jalr);
    // assert_eq!(jalr_iinst.rd, Register::X1);
    // assert_eq!(jalr_iinst.rs1, Register::X18);
    // assert_eq!(jalr_iinst.payload, IPayload::Offset(Offset(0)));

    let lw_icode = 0x14812400;
    let lw_iinst = IInst::decode(lw_icode).unwrap();

    assert_eq!(lw_iinst.tag, ITag::Lw);
    assert_eq!(lw_iinst.rd, Register::X8);
    assert_eq!(lw_iinst.rs1, Register::X2);
    assert_eq!(lw_iinst.payload, IPayload::Offset(Offset(328)));

    let lw_reencode = lw_iinst.encode().unwrap();
    assert_eq!(lw_icode, lw_reencode);

    // let return_icode = 0x80000070;
    // let return_inst = IInst::decode(return_icode).unwrap();
    // assert_eq!(return_inst.tag, ITag::Return);
    // let return_reencode = return_inst.encode().unwrap();
    // assert_eq!(return_reencode, return_icode);

    let ebreak_icode = 0x100070;
    let ebreak_inst = IInst::decode(ebreak_icode).unwrap();
    assert_eq!(ebreak_inst.tag, ITag::Ebreak);
    let ebreak_reencode = ebreak_inst.encode().unwrap();
    assert_eq!(ebreak_reencode, ebreak_icode);
}