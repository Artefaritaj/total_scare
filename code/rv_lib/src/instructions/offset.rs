/*
 * File: offset.rs
 * Project: instructions
 * Created Date: Wednesday July 3rd 2019
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Monday, 22nd July 2019 3:11:56 pm
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2019 INRIA
 */

// use crate::instructions::*;
use crate::error::ISAError;

use std::fmt;

#[derive(Clone,Copy,PartialEq,Eq,PartialOrd,Ord,Hash)]
pub struct Offset(pub i32);

impl Offset {

    pub fn from_u64(val: u64) -> Offset {
        Offset((val as i64) as i32)
    }

    pub fn sign_extend(val: u32, sign_bit: u32, sign_pos: u32) -> Offset {
        let mut imm_val = val;
        let mut bitselect = 0x80000000;

        if sign_bit != 0 {
            for _ in sign_pos+1..32 {
                imm_val |= bitselect;
                bitselect >>= 1;
            }
        }

        Offset(imm_val as i32)
    }

    pub fn offsetu64(&self, val: u64) -> Result<u64, ISAError> {
        if self.0 >= 0 {
            Ok(val + (self.0 as u64))
        }
        else if val >= ((-self.0) as u64) {
            Ok(val - ((-self.0) as u64))
        }
        else {
            Err(ISAError::OutOfBoundsError { val: self.0 as i64 })
        }
    }

    pub fn offsetu32(&self, val: u32) -> Result<u32, ISAError> {
        // debug!("offsetu32: val = {:x}, offset = {}", val, self.0);

        if self.0 >= 0 {
            Ok(val + (self.0 as u32))
        }
        else if val >= ((-self.0) as u32) {
            Ok(val - ((-self.0) as u32))
        }
        else {
            Err(ISAError::OutOfBoundsError { val: self.0 as i64 })
        }
    }
}

impl fmt::Debug for Offset {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Off {}", self.0)
    }
}
