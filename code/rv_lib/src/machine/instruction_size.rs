/*
 * File: instruction_size.rs
 * Project: machine
 * Created Date: Thursday July 4th 2019
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Thursday, 4th July 2019 11:17:22 am
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2019 INRIA
 */

use crate::instructions::*;

#[derive(Debug,Clone,Copy,PartialEq,Eq,PartialOrd,Ord,Hash)]
pub enum InstructionSize {
    Compressed16,
    Normal32
}

impl InstructionSize {
    pub fn get_offset(&self) -> Offset {
        match self {
            InstructionSize::Compressed16   => Offset(2),
            InstructionSize::Normal32       => Offset(4),
        }
    }
}