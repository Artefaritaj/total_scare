/*
 * File: address.rs
 * Project: machine
 * Created Date: Tuesday July 2nd 2019
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Monday, 18th November 2019 3:22:37 pm
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2019 INRIA
 */

use std::fmt;

use std::num::Wrapping;

#[derive(Clone,Copy,Hash,PartialEq,Eq,PartialOrd,Ord,Add,Sub)]
pub struct MemoryAddress(pub u64);

impl MemoryAddress {
    pub fn inc(&mut self) {
        self.0 += 1
    }

    pub fn offset(&mut self, off: i64) {
        if off >= 0 {
            self.0 += off as u64;
        }
        else {
            let noff = (-off) as u64;
            if noff > self.0 {
                self.0 = 0
            }
            else {
                self.0 -= noff;
            }
        }
    }
}

impl From<u64> for MemoryAddress {
    fn from(val: u64) -> MemoryAddress {
        MemoryAddress(val)
    }
}

impl From<MemoryAddress> for u64 {
    fn from(val: MemoryAddress) -> u64 {
        val.0
    }
}

impl From<u32> for MemoryAddress {
    fn from(val: u32) -> MemoryAddress {
        MemoryAddress(val as u64)
    }
}

impl From<MemoryAddress> for u32 {
    fn from(val: MemoryAddress) -> u32 {
        val.0 as u32
    }
}

impl From<MemoryAddress> for Wrapping<u32> {
    fn from(add: MemoryAddress) -> Wrapping<u32> {
        Wrapping(add.into())
    }
}

impl From<Wrapping<u32>> for MemoryAddress {
    fn from(val: Wrapping<u32>) -> MemoryAddress {
        MemoryAddress(val.0 as u64)
    }
}

impl From<MemoryAddress> for Wrapping<u64> {
    fn from(add: MemoryAddress) -> Wrapping<u64> {
        Wrapping(add.into())
    }
}

impl From<Wrapping<u64>> for MemoryAddress {
    fn from(val: Wrapping<u64>) -> MemoryAddress {
        MemoryAddress(val.0 as u64)
    }
}

impl fmt::LowerHex for MemoryAddress {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "@0x{:x}", self.0)
    }
}


impl fmt::Debug for MemoryAddress {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "@0x{:x}", self.0)
    }
}
