/*
 * File: memory.rs
 * Project: machine
 * Created Date: Tuesday July 2nd 2019
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Friday, 22nd November 2019 11:06:19 am
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2019 INRIA
 */

use crate::{
    machine::*,
    // instructions::*,
    // security::*,
    // program::*,
    error::ISAError,
};

use std::fmt;

pub const SIZE_1M: usize = 0x100000;

#[derive(Clone)]
pub struct MemoryArray1M {
    pub content: Box<[u8; SIZE_1M]>,
}

impl Default for MemoryArray1M {
    fn default() -> MemoryArray1M {
        MemoryArray1M { content: Box::new([0u8; SIZE_1M])  }
    }
}

fn check_address_bounds(add: MemoryAddress) -> Result<usize, ISAError> {
    if (add.0 as usize) < SIZE_1M {
        Ok(add.0 as usize)
    }
    else {
        Err(ISAError::OutOfBoundsError { val: add.0 as i64 })
    }
}

impl Memory for MemoryArray1M {
    fn readu8(&self, add: MemoryAddress) -> Result<u8, ISAError> {
        Ok(self.content[check_address_bounds(add)?])
    }

    fn writeu8(&mut self, add: MemoryAddress, val: u8) -> Result<(), ISAError> {
        self.content[check_address_bounds(add)?] = val;
        Ok(())
    }
}


impl fmt::Display for MemoryArray1M {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Memory:")?;

        let bytes_per_line = 16;
        let header_mask = 0xFFFFFFFFFFFFFFF0u64;

        let mut last_header = 0x0u64;

        let mut drawn_on_line = 0;

        for add in 0..SIZE_1M {
            let val = self.content[add];
            let add = add as u64;

            if add == 0 {
                write!(f, "\n\t@0x{:<10x}\t", add)?;
                last_header = add;
                drawn_on_line = 0;
            }

            //draw header if needed
            let address_header = add & header_mask;
            if last_header != address_header{

                //finish line
                while drawn_on_line < bytes_per_line {
                    write!(f, ".. ")?;
                    drawn_on_line += 1;
                }

                //display address jump
                if (address_header - last_header) / bytes_per_line != 1 {
                    write!(f, "\n\t...")?;
                }

                //display address header
                write!(f, "\n\t@0x{:<10x}\t", address_header)?;
                last_header = address_header;
                drawn_on_line = 0;
            }


            let line_index = add % bytes_per_line;

            //draw place holders
            while drawn_on_line < line_index {
                write!(f, ".. ")?;
                drawn_on_line += 1;
            }

            write!(f, "{:02x} ", val)?;
            drawn_on_line += 1;
        }

        //finish line
        while drawn_on_line < bytes_per_line {
            write!(f, ".. ")?;
            drawn_on_line += 1;
        }
        
        Ok(())
    }
}

#[test]
fn test_mem() {
    let mut mem = MemoryArray1M::default();

    assert!(mem.writeu32(MemoryAddress(0), (-1i32) as u32).is_ok());
    assert!(mem.writeu32(MemoryAddress(8), 0x55555555).is_ok());
    assert!(mem.writeu32(MemoryAddress(16), 0xaaaaaaaa).is_ok());

    assert!(mem.writeu64(MemoryAddress(63), (-2i64) as u64).is_ok());

    assert!(mem.writeu8(MemoryAddress(32), 0x12).is_ok());
    assert!(mem.writeu8(MemoryAddress(35), 0x34).is_ok());

    assert!(mem.writeu8(MemoryAddress(0x3000), 0x77).is_ok());

    // println!("{}", &mem);
}