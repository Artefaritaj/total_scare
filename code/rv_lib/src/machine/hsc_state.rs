/*
 * File: hsc_state.rs
 * Project: machine
 * Created Date: Friday November 15th 2019
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Monday, 13th January 2020 2:43:08 pm
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2019 INRIA
 */

use crate::{
    security::*,
};

#[derive(Debug, Clone)]
pub enum NextKey {
    Clear(ConfidentialityKey),
    Sealed(ConfidentialityKey),
    // Empty
}

#[derive(Debug,Clone)]
pub struct HSCState {
    pub context_key:    ConfidentialityKey,
    pub next_key:       NextKey,
}

impl HSCState { 
    pub fn new() -> HSCState {
        let boot_key = ConfidentialityKey::rand();
        HSCState { context_key: boot_key.clone(), next_key: NextKey::Clear(boot_key) }
    }

    pub fn creation_mode(&self) -> bool {
        match self.next_key {
            NextKey::Clear(_) => true,
            _ => false
        }
    }
}