/*
 * File: io_state.rs
 * Project: machine
 * Created Date: Tuesday April 20th 2021
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Wednesday, 21st April 2021 10:02:43 am
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2021 INRIA
 */


use crate::{
    instructions::*,
    // error::*,
};

#[derive(Debug,Clone)]
pub enum IOop {
    Input,
    Output,
    None,
}

// This state is used to save traces:
// inputs are non deterministic data (such as *rand* instruction)
// outputs are saved with the *tracesave* instruction.
#[derive(Debug,Clone)]
pub struct IOState<R: RValue> {
    pub inputs: Vec<R>,
    pub outputs: Vec<R>,
}


impl<R: RValue> IOState<R> {
    pub fn new() -> IOState<R> {
        IOState {
            inputs: Vec::new(),
            outputs: Vec::new(),
        }
    }

    pub fn reset(&mut self) {
        self.inputs.clear();
        self.outputs.clear();
    }

    pub fn add_io(&mut self, val: R, op: IOop) {
        match op {
            IOop::Input     => self.add_input(val),
            IOop::Output    => self.add_output(val),
            _               => {}
        }
    }

    pub fn add_input(&mut self, val: R) {
        self.inputs.push(val);
    }

    pub fn add_output(&mut self, val: R) {
        self.outputs.push(val);
    }
}