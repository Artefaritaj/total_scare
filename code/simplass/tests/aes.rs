/*
 * File: aes.rs
 * Project: tests
 * Created Date: Friday December 6th 2019
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Tuesday, 17th December 2019 9:11:55 am
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2019 INRIA
 */

use simplass::{
    compiler::*,
    // parser::*,
};

#[test]
fn test_parser() {

    let tledger = TokenLedger::from_file("tests/aes/main.s").unwrap();
    let prelayout = PreLayout::from_token_ledger(tledger);
    // println!("Prelayout: {:?}", prelayout);
    let mut postlayout = PostLayout::from_pre_layout(MemoryAddress(0), prelayout).unwrap();
    // println!("Postlayout before rewriting: {:?}", postlayout);
    postlayout.apply_rewriting().unwrap();
    println!("Postlayout after rewriting: {:?}", postlayout);
}