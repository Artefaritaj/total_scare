
# Compiler for simplass assembly

## 1 - Parse into tokens and extract imports recursively

```
let tledger = TokenLedger::from_file("test.s");
```

## 2 - Extract symbols, data and instructions from tokens

```
let prelayout = PreLayout::from_token_ledger(tledger);
```

### 3 - Code Generation and layouting

```
let postlayout = PostLayout::from_pre_layout(prelayout);
```