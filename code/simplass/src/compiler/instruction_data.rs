/*
 * File: instruction_data.rs
 * Project: compiler
 * Created Date: Wednesday December 11th 2019
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Wednesday, 11th December 2019 4:28:30 pm
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2019 INRIA
 */

use byteorder::{/*ReadBytesExt, */WriteBytesExt, LittleEndian};

use rv_lib::instructions::{
    *,
    decoder::*,
};

use crate::parser::*;

#[derive(Debug, Clone)]
pub enum InstructionData {
    Instruction(RV32I),
    Data(Data)
}

impl InstructionData {
    pub fn to_bytes(&self) -> Vec<u8> {
        match self {
            InstructionData::Data(data) => data.payload.to_bytes(),
            InstructionData::Instruction(i) => {
                let word = i.encode().unwrap(); // panic if error: shouldnot happen
                let mut wtr: Vec<u8> = Vec::new();
                wtr.write_u32::<LittleEndian>(word).unwrap();
                wtr
            }
        }
    }
}