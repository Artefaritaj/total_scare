/*
 * File: symbol_ledger.rs
 * Project: compiler
 * Created Date: Monday December 9th 2019
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Monday, 16th December 2019 2:14:15 pm
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2019 INRIA
 */

use rv_lib::instructions::*;

use std::collections::{HashMap, BTreeMap};

use crate::parser::*;
use crate::compiler::*;

#[derive(Debug)]
pub struct PreLayoutSymbolLedger {
    symbols:    HashMap<String, PreLayoutSymbolDeclarationEntry>, // the key is: symbol path = symbol scope + "/" + symbol name
    links:      HashMap<String, Vec<PreLayoutSymbolLinkEntry>>,// the key is: symbol path = symbol scope + "/" + symbol name
}

impl PreLayoutSymbolLedger {
    pub fn new() -> PreLayoutSymbolLedger {
        PreLayoutSymbolLedger { symbols: HashMap::new(), links: HashMap::new() }
    }

    pub fn from_tokens(tcompiler: &mut TokenLedger) -> PreLayoutSymbolLedger {
        let mut ledger = PreLayoutSymbolLedger::new();
        
        for (path, tokens) in tcompiler.token_map.iter_mut() {
            let mut instruction_index = 0;
            tokens.retain(|tok| {
                match tok {
                    Token::SymbolDeclaration(sdecl) => {
                        ledger.add_symbol(sdecl.clone(), path, instruction_index);

                        false
                    },
                    Token::Instructions(ivec) => {
                        instruction_index += ivec.len();
                        true
                    },
                    Token::Data(d) => {
                        ledger.add_symbol(
                            SymbolDeclaration {
                                metatags: Vec::new(),
                                symbol: AddressSymbol(d.name.clone())
                            }, 
                            path, 
                            instruction_index);
                        instruction_index += 1;
                        true
                    },
                    _ => { true }
                }
            });
        }


        ledger
    }

    pub fn get_explicitely_placed_symbols_by_file(&self, filepath: &str) -> BTreeMap<usize, (String, MemoryAddress)> {// (address, symbol_path, instruction_index)
        let mut explicitly_placed: BTreeMap<usize, (String, MemoryAddress)> = BTreeMap::new();
        
        for (symbol_path, sde) in self.symbols.iter() {
            if sde.point_to_file == filepath {
                for tag in sde.tags.iter() {
                    match tag {
                        Metatag::Address(add) => {
                            explicitly_placed.insert(sde.point_to_index, (symbol_path.clone(), MemoryAddress(*add)));
                            break;
                        },
                        _ => {}
                    }
                }
            }
        }

        explicitly_placed
    }

    pub fn get_links_by_file(&self, filepath: &str) -> HashMap<String, Vec<PreLayoutSymbolLinkEntry>> {// (symbol path, links)
        let mut result_links: HashMap<String, Vec<PreLayoutSymbolLinkEntry>> = HashMap::new();
        
        for (symbol_path, sle_vec) in self.links.iter() {

            let mut links_in_this_file = Vec::new();

            for sle in sle_vec.iter() {
                if sle.point_to_file == filepath {
                    links_in_this_file.push(sle.clone());
                }
            }

            result_links.insert(symbol_path.clone(), links_in_this_file);
        }

        result_links
    }

    pub fn get_symbols_by_file(&self, filepath: &str) -> HashMap<String, PreLayoutSymbolDeclarationEntry> {// (symbol path, symbol description)
        let mut result_symbols: HashMap<String, PreLayoutSymbolDeclarationEntry> = HashMap::new();
        
        for (symbol_path, sde) in self.symbols.iter() {
            if sde.point_to_file == filepath {
                result_symbols.insert(symbol_path.clone(), sde.clone());
            }
        }

        result_symbols
    }


    pub fn union(&mut self, other: PreLayoutSymbolLedger) {
        for (k, v) in other.symbols.into_iter() {
            self.symbols.insert(k, v);
        }
    }

    pub fn get_symbol_scope(&self, current_path: &str, symbol_name: &str) -> String {
        //test at global scope first
        let global_scope = format!("/{}", symbol_name);
        if self.symbols.contains_key(&global_scope) {
            global_scope
        }
        else {
            format!("{}/{}", current_path, symbol_name)
        }
    }

    pub fn add_link(&mut self, current_path: &str, instruction_index: usize, link: &SymbolLink) {
        let symbol_path = self.get_symbol_scope(current_path, &link.symbol.0);

        let mut previous = match self.links.remove(&symbol_path) {
            Some(p) => p,
            None => Vec::new()
        };

        previous.push(PreLayoutSymbolLinkEntry { 
                        point_to_file: current_path.to_owned(), 
                        point_to_index: instruction_index, 
                        part: link.part, 
                        pointer_kind: link.pointer_kind });


        self.links.insert(symbol_path, previous);
    }

    pub fn add_symbol(&mut self, symbol_decl: SymbolDeclaration, current_path: &str, instruction_index: usize) {
        let (tags, symbol_name) = (symbol_decl.metatags, symbol_decl.symbol.0);
        let global = tags.contains(&Metatag::Global);

        let symbol_path = if global == true {
            format!("/{}", symbol_name)
        }
        else {
            format!("{}/{}", current_path, symbol_name)
        };
        self.symbols.insert(symbol_path, PreLayoutSymbolDeclarationEntry { point_to_file: current_path.to_owned(), point_to_index: instruction_index, tags });
    }
}


#[derive(Debug, Clone)]
pub struct PreLayoutSymbolDeclarationEntry {
    /// file path where we can find the instruction this symbol points to
    pub point_to_file:          String,

    ///Index of the instruction this symbols points to, in the self.point_to_file file
    pub point_to_index:         usize,

    pub tags:                   Vec<Metatag>,
}

#[derive(Debug, Clone)]
pub struct PreLayoutSymbolLinkEntry {
    /// file path where we can find the instruction this symbol points to
    pub point_to_file:          String,

    ///Index of the instruction this symbols points to, in the self.point_to_file file
    pub point_to_index:         usize,

    pub part:                   InstructionPartSelector,

    pub pointer_kind:           SymbolPointerKind,
}