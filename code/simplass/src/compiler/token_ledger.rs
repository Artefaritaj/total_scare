/*
 * File: token_compiler.rs
 * Project: compiler
 * Created Date: Tuesday December 10th 2019
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Friday, 14th February 2020 11:28:11 am
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2019 INRIA
 */

use indexmap::IndexMap;

// use std::collections::HashMap;
use std::path::Path;

// use crate::compiler::*;
use crate::parser::*;
use crate::errors::*;

#[derive(Debug)]
pub struct TokenLedger {
    pub token_map: IndexMap<String, Vec<Token>>
}


impl TokenLedger {
    pub fn from_file<P: AsRef<Path>>(path: P) -> Result<TokenLedger, CompilerError> {

        let mut tcompiler = TokenLedger { token_map: IndexMap::new() };
        TokenLedger::rec_from_file(path, &mut tcompiler)?;

        Ok(tcompiler)
    }

    fn extract_import_files(mut tokens: Vec<Token>) -> (Vec<Token>, Vec<String>) {
        let mut imports: Vec<String> = Vec::new();

        tokens.retain(|tok|{ // retain only non import tokens
            match tok {
                Token::Import(ipath) => {//if token, extract path
                    imports.push(ipath.path.clone());
                    false
                },
                _ => true
            }
        });

        (tokens, imports)
    }

    fn rec_from_file<P: AsRef<Path>>(path: P, tcompiler: &mut TokenLedger) -> Result<(), CompilerError> {
        //stringify path
        let fullpath = path.as_ref().canonicalize().map_err(|e| CompilerError::FileNotFoundError { input: format!("{}: {}", path.as_ref().display(), e) })?;
        let fullpath_string = fullpath.display().to_string();
        let mut parent_pathbuf = path.as_ref().to_path_buf();
        parent_pathbuf.pop();

        //get tokens and children
        let tokens = Token::parse_tokens(path)  .map_err(|e| CompilerError::TokenError { input: format!("{}", e) })?;
        let (tokens_wo_imports, children_files) = TokenLedger::extract_import_files(tokens);

        //add this file to hashmap
        tcompiler.token_map.insert(fullpath_string, tokens_wo_imports);

        //browse children recursively
        for import_child in children_files.iter() {
            let mut new_path = parent_pathbuf.clone();
            new_path.push(import_child);

            TokenLedger::rec_from_file(new_path, tcompiler)?;
        }

        Ok(())
    }
}