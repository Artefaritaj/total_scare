/*
 * File: address_symbol.rs
 * Project: srv_isa
 * Created Date: Tuesday November 26th 2019
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Monday, 9th December 2019 2:42:16 pm
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2019 INRIA
 */

use crate::parser::*;

#[derive(Debug,Clone,PartialEq,Eq,PartialOrd,Ord,Hash)]
pub struct AddressSymbol(pub String);

#[derive(Debug,Clone,PartialEq,Eq)]
pub struct SymbolDeclaration {
    pub metatags:    Vec<Metatag>,
    pub symbol:     AddressSymbol,
}