/*
 * File: branch_conditions.rs
 * Project: parser
 * Created Date: Friday December 6th 2019
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Friday, 6th December 2019 10:32:42 am
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2019 INRIA
 */

use std::str::FromStr;

use crate::errors::*;

#[derive(Debug,Clone,PartialEq,Eq,PartialOrd,Ord,Hash)]
pub enum BranchCondition {
    Equal,
    NotEqual,
    SignedStrictlyGreaterThan,
    SignedStrictlyLowerThan,
    SignedGreaterOrEqual,
    SignedLowerOrEqual,
    UnsignedStrictlyGreaterThan,
    UnsignedStrictlyLowerThan,
    UnsignedGreaterOrEqual,
    UnsignedLowerOrEqual,
}

#[derive(Debug,Clone,PartialEq,Eq,PartialOrd,Ord,Hash)]
pub enum BranchConditionReduced {
    Equal,
    NotEqual,
    SignedStrictlyLowerThan,
    SignedGreaterOrEqual,
    UnsignedStrictlyLowerThan,
    UnsignedGreaterOrEqual,
}

impl FromStr for BranchCondition {
    type Err = AssError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "=="        => Ok(BranchCondition::Equal),
            "!="        => Ok(BranchCondition::NotEqual),
            "<s"        => Ok(BranchCondition::SignedStrictlyLowerThan),
            "<=s"       => Ok(BranchCondition::SignedLowerOrEqual),
            ">s"        => Ok(BranchCondition::SignedStrictlyGreaterThan),
            ">=s"       => Ok(BranchCondition::SignedGreaterOrEqual),
            "<u"        => Ok(BranchCondition::UnsignedStrictlyLowerThan),
            "<=u"       => Ok(BranchCondition::UnsignedLowerOrEqual),
            ">u"        => Ok(BranchCondition::UnsignedStrictlyGreaterThan),
            ">=u"       => Ok(BranchCondition::UnsignedGreaterOrEqual),

            e => Err(AssError::BranchConditionParsingError { input: e.to_owned() })
        }
    }
} 

impl BranchCondition {
    pub fn to_reduced(&self) -> (BranchConditionReduced, bool) { // bool is true if operands need to be switchd
        match self {
            BranchCondition::Equal                      => (BranchConditionReduced::Equal, false),
            BranchCondition::NotEqual                   => (BranchConditionReduced::NotEqual, false),
            BranchCondition::SignedStrictlyGreaterThan  => (BranchConditionReduced::SignedStrictlyLowerThan, true),
            BranchCondition::SignedStrictlyLowerThan    => (BranchConditionReduced::SignedStrictlyLowerThan, false),
            BranchCondition::SignedGreaterOrEqual       => (BranchConditionReduced::SignedGreaterOrEqual, false),
            BranchCondition::SignedLowerOrEqual         => (BranchConditionReduced::SignedGreaterOrEqual, true),
            BranchCondition::UnsignedStrictlyGreaterThan  => (BranchConditionReduced::UnsignedStrictlyLowerThan, true),
            BranchCondition::UnsignedStrictlyLowerThan    => (BranchConditionReduced::UnsignedStrictlyLowerThan, false),
            BranchCondition::UnsignedGreaterOrEqual       => (BranchConditionReduced::UnsignedGreaterOrEqual, false),
            BranchCondition::UnsignedLowerOrEqual         => (BranchConditionReduced::UnsignedGreaterOrEqual, true),
        }
    }
}