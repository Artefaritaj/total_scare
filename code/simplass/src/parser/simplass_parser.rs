/*
 * File: badass_parser.rs
 * Project: parser
 * Created Date: Friday December 6th 2019
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Thursday, 22nd April 2021 9:36:11 am
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2019 INRIA
 */

use rv_lib::instructions::Register;
  
use std::str::FromStr;
use std::char;

use crate::parser::*;
use rv_lib::instructions::*;
// use crate::errors::*;

pub use self::simplass_parser::*;

peg::parser!{pub grammar simplass_parser() for str {

  pub rule tokens() -> Vec<Token> = toks:token()* { toks }

  pub rule token() -> Token
    = import:import() { Token::Import(import) }
    / data:data() { Token::Data(data) }
    / symb_decl:symbol_declaration() { Token::SymbolDeclaration(symb_decl) }

    // instructions
    //arithmetic
    / i:arithmetic() { Token::Instructions(vec![(i, None)])}
    / i:arithmetic_immediate() { Token::Instructions(vec![(i, None)])}
    / i:reg_move() { Token::Instructions(vec![(i, None)])}
    / v:push() { Token::Instructions(v.into_iter().map(|i|(i, None)).collect())}
    / v:pop() { Token::Instructions(v.into_iter().map(|i|(i, None)).collect())}

    //branches and jumps
    / j:jump() { Token::Instructions(vec![(j, None)])}
    / ij:indirect_jump() { Token::Instructions(vec![(ij, None)])}
    / js:jump_symbol() { Token::Instructions(vec![(js.0, Some(js.1))])}
    / b:branch() { Token::Instructions(vec![(b, None)])}
    / bs:branch_symbol() { Token::Instructions(vec![(bs.0, Some(bs.1))])}

    //Memory
    / ql:quick_load() { Token::Instructions(ql.into_iter().map(|i|(i, None)).collect())}
    / qs:quick_store() { Token::Instructions(qs.into_iter().map(|i|(i, None)).collect())}
    / l:load() { Token::Instructions(vec![(l, None)]) }
    / to:trace_out() { Token::Instructions(vec![(to, None)])}
    / s:store() { Token::Instructions(vec![(s, None)]) }
    / qls:quick_load_symbol() { Token::Instructions(qls) }
    / qss:quick_store_symbol() { Token::Instructions(qss) }

    //Immediate
    / aui:auipc() { Token::Instructions(aui.into_iter().map(|i|(i, None)).collect())}
    / li:load_immediate() { Token::Instructions(li.into_iter().map(|i|(i, None)).collect())}
    / lsa:load_symbol_address() { Token::Instructions(lsa.into_iter().map(|(i,s)| (i, Some(s))).collect()) }
    

    //CSR
    / c:csr_read() { Token::Instructions(vec![(c,None)])}
    / c:csr_write() { Token::Instructions(vec![(c, None)])}


  pub rule csr_read() -> RV32I // csrrs rd, csr, x0
    = __() rd:register() __() "<-" __() "CSR[" __() csr:csr_val() __() "]" __() ";" __() { RV32I::Csrrs(rd, csr, Register::X0) }

  pub rule csr_write() -> RV32I // csrrw x0, csr, rs1
    = __() "CSR[" __() csr:csr_val() __() "]" __() "<-" __() rs1:register() __() ";" __() { RV32I::Csrrw(Register::X0, csr, rs1) }

  rule csr_val() -> Csr
    = u:u32() { Csr(u as u16) }

  rule jump_destination() -> JumpDestination
    = symbol:address_symbol() { JumpDestination::AddressSymbol(symbol) }
    / off:offset() { JumpDestination::Literal(off) }

  pub rule jump() -> RV32I
    = __() "goto" __() off:offset() __() ";" __() { RV32I::Jal(Register::X0, off) }
    / __() "call" __() off:offset() __() ";" __() { RV32I::Jal(Register::X1, off) }

  pub rule jump_symbol() -> (RV32I, SymbolLink)
  = __() "goto" __() symbol:address_symbol() __() ";" __() { (RV32I::Jal(Register::X0, Offset(0)), SymbolLink { symbol, part: InstructionPartSelector::ImmJ, pointer_kind: SymbolPointerKind::Relative}) }
  / __() "call" __() symbol:address_symbol() __() ";" __() { (RV32I::Jal(Register::X1, Offset(0)), SymbolLink { symbol, part: InstructionPartSelector::ImmJ, pointer_kind: SymbolPointerKind::Relative}) }

  pub rule indirect_jump() -> RV32I
    = __() "icall" __() rs1:register() __() "+"? __() off:offset()? __() ";" __() { RV32I::Jalr(Register::X1, rs1, off.unwrap_or(Offset(0)))}
    / __() "icall" __() "[" __()  rd:register() __() "]" __() rs1:register() __() "+"? __() off:offset()? __() ";" __() { RV32I::Jalr(rd, rs1, off.unwrap_or(Offset(0)))}
    / __() "return" __() ";" __() { RV32I::Jalr(Register::X0, Register::X1, Offset(0))}
    / __() "return" __() "[" __() rs1:register() __() "]" __() ";" __() { RV32I::Jalr(Register::X0, rs1, Offset(0))}

  pub rule branch() -> RV32I
    = __() "if" __() "(" __() rs1:register() __() condition:branch_condition() __() rs2:register() __() ")" __() "goto" __() "pc" __() sign_str:$['+' | '-'] __() off:u32() __() ";" __() {
      let sign: i32 = match sign_str {
        "-" => -1,
        _ => 1,
      };

      let offset = Offset((off as i32) * sign);
      let (reduced_condition, switch) = condition.to_reduced();

      let (rs1, rs2) = if switch == true {
        (rs2, rs1)
      }
      else {
        (rs1, rs2)
      };

      match reduced_condition {
        BranchConditionReduced::Equal                     => RV32I::Beq(rs1, rs2, offset),
        BranchConditionReduced::NotEqual                  => RV32I::Bne(rs1, rs2, offset),
        BranchConditionReduced::SignedStrictlyLowerThan   => RV32I::Blt(rs1, rs2, offset),
        BranchConditionReduced::SignedGreaterOrEqual      => RV32I::Bge(rs1, rs2, offset),
        BranchConditionReduced::UnsignedStrictlyLowerThan => RV32I::Bltu(rs1, rs2, offset),
        BranchConditionReduced::UnsignedGreaterOrEqual    => RV32I::Bgeu(rs1, rs2, offset),
      }
    }

  pub rule branch_symbol() -> (RV32I, SymbolLink)
    = __() "if" __() "(" __() rs1:register() __() condition:branch_condition() __() rs2:register() __() ")" __() "goto" __() symbol:address_symbol() __() ";" __() {
      let (reduced_condition, switch) = condition.to_reduced();

      let (rs1, rs2) = if switch == true {
        (rs2, rs1)
      }
      else {
        (rs1, rs2)
      };

      let inst = match reduced_condition {
        BranchConditionReduced::Equal                     => RV32I::Beq(rs1, rs2, Offset(0)),
        BranchConditionReduced::NotEqual                  => RV32I::Bne(rs1, rs2, Offset(0)),
        BranchConditionReduced::SignedStrictlyLowerThan   => RV32I::Blt(rs1, rs2, Offset(0)),
        BranchConditionReduced::SignedGreaterOrEqual      => RV32I::Bge(rs1, rs2, Offset(0)),
        BranchConditionReduced::UnsignedStrictlyLowerThan => RV32I::Bltu(rs1, rs2, Offset(0)),
        BranchConditionReduced::UnsignedGreaterOrEqual    => RV32I::Bgeu(rs1, rs2, Offset(0)),
      };

      (inst, SymbolLink { symbol, part: InstructionPartSelector::ImmB, pointer_kind: SymbolPointerKind::Relative })
    }

  rule branch_condition() -> BranchCondition
    = cond:$("==" / "!=" / "<s" / "<=s" / ">s" / ">=s" / "<u" / "<=u" / ">u" / ">=u") {?
      BranchCondition::from_str(cond).map_err(|_|"Cannot parse branch condition")
  }

  pub rule quick_load() -> Vec<RV32I>
    = __() rs1:register() __() "<-" temp_reg:register() __() "[" __() address:u32() __() "]" qualif:$['w' | 'h' | 'b']? __() ";" __() {
      let mut insts = Vec::new();

      let r_previous = if address & 0xFFFFF000 != 0 {
        insts.push(RV32I::Lui(temp_reg, Immediate(address & 0xFFFFF000)));
        temp_reg
      }
      else {
        Register::X0
      };

      insts.push(RV32I::Addi(temp_reg, r_previous, Immediate(address & 0xFFF)));

      let load_inst = match qualif {
        Some("b") => RV32I::Lb(rs1, temp_reg, Offset(0)),
        Some("h") => RV32I::Lh(rs1, temp_reg, Offset(0)),
        _ => RV32I::Lw(rs1, temp_reg, Offset(0))
      };

      insts.push(load_inst);

      insts
    }

    pub rule quick_load_symbol() -> Vec<(RV32I, Option<SymbolLink>)>
    = __() rs1:register() __() "<-" temp_reg:register() __() "[" __() symbol:address_symbol() __() sign_str_opt:$['+' | '-']? __() off_opt:u32()? __()  "]" qualif:$['w' | 'h' | 'b']? __() ";" __() {
      let mut insts: Vec<(RV32I, Option<SymbolLink>)> = Vec::new();

      let sign: i32 = if let Some(sign_str) = sign_str_opt {
        match sign_str {
          "+" => 1,
          "-" => -1,
          _ => 0
        }
      }
      else {
        0
      };

      let offset: Offset = Offset( sign * (off_opt.unwrap_or(0) as i32));
      
      insts.push((RV32I::Lui(temp_reg, Immediate(0)), Some(SymbolLink { symbol: symbol.clone(), part: InstructionPartSelector::ImmU, pointer_kind: SymbolPointerKind::Absolute })));
      insts.push( (RV32I::Addi(temp_reg, temp_reg, Immediate(0)), Some(SymbolLink { symbol, part: InstructionPartSelector::ImmI, pointer_kind: SymbolPointerKind::Absolute })));

      let load_inst = match qualif {
        Some("b") => RV32I::Lb(rs1, temp_reg, offset),
        Some("h") => RV32I::Lh(rs1, temp_reg, offset),
        _ => RV32I::Lw(rs1, temp_reg, offset)
      };

      insts.push((load_inst, None));

      insts
    }

  pub rule load() -> RV32I
    = __() rs1:register() __()  "<-" __() "[" __() rs2:register() __() sign_str_opt:$['+' | '-']? __() off_opt:u32()? __() "]" qualif:$['w' | 'h' | 'b']? __() ";" __() {
      let sign: i32 = if let Some(sign_str) = sign_str_opt {
        match sign_str {
          "+" => 1,
          "-" => -1,
          _ => 0
        }
      }
      else {
        0
      };

      let offset: Offset = Offset( sign * (off_opt.unwrap_or(0) as i32));

      match qualif {
        Some("b") => RV32I::Lb(rs1, rs2, offset),
        Some("h") => RV32I::Lh(rs1, rs2, offset),
        _ => RV32I::Lw(rs1, rs2, offset)
      }
      
    }

  pub rule trace_out() -> RV32I
    = __() "output" __() rs1:register() __() ";" __() {
      RV32I::Lwu(Register::X0, rs1, Offset(0))
    }

  pub rule quick_store() -> Vec<RV32I>
    = __() "[" __() address:u32() __() "]" qualif:$['w' | 'h' | 'b']? __() "<-" temp_reg:register() __() rs2:register() __() ";" __() {
      let mut insts = Vec::new();


      let r_previous = if address & 0xFFFFF000 != 0 {
        insts.push(RV32I::Lui(temp_reg, Immediate(address & 0xFFFFF000)));
        temp_reg
      }
      else {
        Register::X0
      };

      insts.push(RV32I::Addi(temp_reg, r_previous, Immediate(address & 0xFFF)));

      let store_inst = match qualif {
        Some("b") => RV32I::Sb(temp_reg, rs2, Offset(0)),
        Some("h") => RV32I::Sh(temp_reg, rs2, Offset(0)),
        _ => RV32I::Sw(temp_reg, rs2, Offset(0))
      };

      insts.push(store_inst);

      insts
    }

  pub rule quick_store_symbol() -> Vec<(RV32I, Option<SymbolLink>)>
    = __() "[" __() symbol:address_symbol() __() sign_str_opt:$['+' | '-']? __() off_opt:u32()? __()  "]" qualif:$['w' | 'h' | 'b']? __() "<-" temp_reg:register() __() rs2:register() __() ";" __() {
      let mut insts: Vec<(RV32I, Option<SymbolLink>)> = Vec::new();

      let sign: i32 = if let Some(sign_str) = sign_str_opt {
        match sign_str {
          "+" => 1,
          "-" => -1,
          _ => 0
        }
      }
      else {
        0
      };

      let offset: Offset = Offset( sign * (off_opt.unwrap_or(0) as i32));

      insts.push((RV32I::Lui(temp_reg, Immediate(0)), Some(SymbolLink { symbol: symbol.clone(), part: InstructionPartSelector::ImmU, pointer_kind: SymbolPointerKind::Absolute })));
      insts.push((RV32I::Addi(temp_reg, temp_reg, Immediate(0)), Some(SymbolLink { symbol: symbol, part: InstructionPartSelector::ImmI, pointer_kind: SymbolPointerKind::Absolute})));

      let store_inst = match qualif {
        Some("b") => RV32I::Sb(temp_reg, rs2, offset),
        Some("h") => RV32I::Sh(temp_reg, rs2, offset),
        _ => RV32I::Sw(temp_reg, rs2, offset)
      };

      insts.push((store_inst, None));

      insts
    }

  pub rule store() -> RV32I
    = __() "[" __() rs1:register() __() sign_str_opt:$['+' | '-']? __() off_opt:u32()? __() "]" qualif:$['w' | 'h' | 'b']? __() "<-" __() rs2:register() __() ";" __() {
      let sign: i32 = if let Some(sign_str) = sign_str_opt {
        match sign_str {
          "+" => 1,
          "-" => -1,
          _ => 0
        }
      }
      else {
        0
      };

      let offset: Offset = Offset( sign * (off_opt.unwrap_or(0) as i32));

      match qualif {
        Some("b") => RV32I::Sb(rs1, rs2, offset),
        Some("h") => RV32I::Sh(rs1, rs2, offset),
        _ => RV32I::Sw(rs1, rs2, offset)
      }
      
    }

  pub rule auipc() -> Vec<RV32I>
    = __() rd:register() __() "<-" __() "pc" __() sign_str:$['+' | '-'] __() u:u32() __() ";" __() {
      let sign: i32 = 
        match sign_str {
          "+" => 1,
          "-" => -1,
          _ => 0
        };

      let offset_i = sign * (u as i32);
      let mut insts = Vec::new();
      insts.push(RV32I::Auipc(rd, Immediate(offset_i as u32)));

      if ((offset_i as u32) & 0xFFF) != 0 {
        insts.push(RV32I::Addi(rd, rd, Immediate(((offset_i as u32) & 0xFFF) as u32)));
      }
      
      insts
    }

  rule register_spaced() -> Register
  = __() reg:register() __() { reg }

  pub rule push() -> Vec<RV32I>
  = __() "push" __() regs:register_spaced() ** "," __() ";" __() { 
    let mut instructions: Vec<RV32I> = Vec::new();
    for (i, reg) in regs.iter().enumerate() {
      instructions.push(RV32I::Sw(Register::X2, *reg, Offset((4*i) as i32)));
    }
    instructions.push(RV32I::Addi(Register::X2, Register::X2, Immediate((4*instructions.len()) as u32)));

    instructions
  }

  pub rule pop() -> Vec<RV32I>
  = __() "pop" __() regs:register_spaced() ** "," __() ";" __() { 
    let mut instructions: Vec<RV32I> = Vec::new();
    for (i, reg) in regs.iter().enumerate() {
      instructions.push(RV32I::Lw(*reg, Register::X2, Offset(
        -4i32 * (i+1) as i32
      )));
    }
    instructions.push(RV32I::Addi(Register::X2, Register::X2, Immediate(
      (-4i32 * (instructions.len() as i32)) as u32
    )));

    instructions
  }

  pub rule load_immediate() -> Vec<RV32I>
    = __() rd:register() __() "<-" __() i:i64() __() ";" __() {

      //from https://stackoverflow.com/questions/50742420/risc-v-build-32-bit-constants-with-lui-and-addi
      let mut insts = Vec::new();

      let n = i as i32;
      // let low_i = ((n << 20) >> 20);
      // let low = Immediate(low_i as u32);
      let low = Immediate::sign_extend(n as u32 & 0xFFF, n as u32 & 0x800, 11);
      // let high = Immediate((((n-low_i) >> 12) << 12) as u32);
      let diff = (n-(low.0 as i32)) as u32;
      // let high = Immediate::sign_extend(diff, diff & 0x100000, 19);
      let high = Immediate(diff);

      

      //lui
      let r_inter = if high.0 != 0 {
        insts.push(RV32I::Lui(rd, high));
        rd
      }
      else {
        Register::X0
      };

      //addi
      insts.push(RV32I::Addi(rd, r_inter, low));

      insts
    }

  pub rule load_symbol_address() -> Vec<(RV32I, SymbolLink)>
    = __() rd:register() __() "<-" __() symbol:address_symbol() __() ";" __() {

      let mut insts = Vec::new();
      insts.push((RV32I::Lui(rd, Immediate(0)), SymbolLink { symbol: symbol.clone(), part: InstructionPartSelector::ImmU, pointer_kind: SymbolPointerKind::Absolute }));
      insts.push((RV32I::Addi(rd, rd, Immediate(0)), SymbolLink { symbol, part: InstructionPartSelector::ImmI, pointer_kind: SymbolPointerKind::Absolute }));

      insts
    } 

  pub rule reg_move() -> RV32I
    = __() rd:register() __() "<-" __() rs1:register() __() ";" __() {
      RV32I::Addi(rd, rs1, Immediate(0))
    }

  pub rule arithmetic_immediate() -> RV32I
    = __() rd:register() __() "<-" __() rs1:register() __() op:arithmetic_operator() __() imm:i32() __() ";" __() {

      let imm_cropped = Immediate((imm as u32) & 0xFFF);
      let neg_imm_cropped = Immediate((-imm as u32) & 0xFFF);
      let shamt = Shamt(((imm & 0x1F) as u8) );

      match op {
        ArithmeticOperator::Add   => RV32I::Addi(rd, rs1, imm_cropped),
        ArithmeticOperator::And   => RV32I::Andi(rd, rs1, imm_cropped),
        ArithmeticOperator::Or    => RV32I::Ori(rd, rs1, imm_cropped),
        ArithmeticOperator::Sll   => RV32I::Slli(rd, rs1, shamt),
        ArithmeticOperator::Slt   => RV32I::Slti(rd, rs1, imm_cropped),
        ArithmeticOperator::Sltu  => RV32I::Sltiu(rd, rs1, imm_cropped),
        ArithmeticOperator::Sra   => RV32I::Srai(rd, rs1, shamt),
        ArithmeticOperator::Srl   => RV32I::Srli(rd, rs1, shamt),
        ArithmeticOperator::Sub   => RV32I::Addi(rd, rs1, neg_imm_cropped),
        ArithmeticOperator::Xor   => RV32I::Xori(rd, rs1, imm_cropped),
      }
    }

  pub rule arithmetic() -> RV32I
    = __() rd:register() __() "<-" __() rs1:register() __() op:arithmetic_operator() __() rs2:register() __() ";" __() {
      match op {
        ArithmeticOperator::Add   => RV32I::Add(rd, rs1, rs2),
        ArithmeticOperator::And   => RV32I::And(rd, rs1, rs2),
        ArithmeticOperator::Or    => RV32I::Or(rd, rs1, rs2),
        ArithmeticOperator::Sll   => RV32I::Sll(rd, rs1, rs2),
        ArithmeticOperator::Slt   => RV32I::Slt(rd, rs1, rs2),
        ArithmeticOperator::Sltu  => RV32I::Sltu(rd, rs1, rs2),
        ArithmeticOperator::Sra   => RV32I::Sra(rd, rs1, rs2),
        ArithmeticOperator::Srl   => RV32I::Srl(rd, rs1, rs2),
        ArithmeticOperator::Sub   => RV32I::Sub(rd, rs1, rs2),
        ArithmeticOperator::Xor   => RV32I::Xor(rd, rs1, rs2),
      }
    }

  rule offset() -> Offset
    = i:i32() { Offset(i) }

  rule arithmetic_operator() -> ArithmeticOperator
    = op:$("+" / "-" / "<<" / "^" / ">>s" / ">>" / "|" / "&" / "<s" / "<u" / "add" / "sub" / "sll" /
       "xor" / "srl" / "or" / "and" / "sra" / "slt" / "sltu") {? ArithmeticOperator::from_str(op).map_err(|_| "Cannot parse arithmetic operator.") }

  pub rule symbol_declaration() -> SymbolDeclaration = __() metatags:metatags()? __() "@" __() id:identifier() ":" { SymbolDeclaration { metatags: metatags.unwrap_or(Vec::new()), symbol: AddressSymbol(id) } }
  pub rule address_symbol() -> AddressSymbol = "&" __() name:identifier() { AddressSymbol(name) }

  pub rule metatags() -> Vec<Metatag> = "#[" tags:metatag() ** "," "]" { tags }
  pub rule metatag() -> Metatag
    = __() "address" __() "(" __() add:u64() __() ")" __() { Metatag::Address(add) }
    / __() "global" __() { Metatag::Global }

  pub rule import() -> ImportFile
    = __() "import" __() path:string() __() ";" __() { ImportFile { path } }

  pub rule data() -> Data
    = __() "def" __() name:identifier() __() ":" __() payload:data_payload() __() ";" __()
    { Data { name, payload }}

  rule data_payload() -> DataPayload
    = "[u8]" __() "=" __() b:bytes() { DataPayload::Bytes(b) }
    / "String" __() "=" __()  s:string() { DataPayload::String(s) }
    / "u32" __() "=" __() u:u32() { DataPayload::U32(u) }
    / "i32" __() "=" __() i:i32() { DataPayload::I32(i) }

  rule bytes() -> Vec<u8>
    = "[" v:u8_spaced() ** "," "]"  { v }

  rule string() -> String
    = s:doubleQuotedString() { s }

  rule u8_spaced() -> u8
    = __() u:unsigned_integer_literal() __() { u as u8 }

  rule u64() -> u64
    = u:unsigned_integer_literal() { u }

  rule i64() -> i64
    = s:signed_integer_literal() { s }

  rule u32() -> u32
    = u:unsigned_integer_literal() { u as u32 }

  rule i32() -> i32
    = i:signed_integer_literal() { i as i32 }


  pub rule register() -> Register
    = xx:$( "x10" / "x11" / "x12" / "x13" / "x14" / "x15" /
      "x16" / "x17" / "x18" / "x19" / "x20" / "x21" / "x22" / "x23" / "x24" / "x25" / "x26" / "x27" / "x28" / "x29" / "x30" / "x31" /
      "x0" / "x1" / "x2" / "x3" / "x4" / "x5" / "x6" / "x7" / "x8" / "x9" /
      "zero" / "ra" / "sp" / "gp" / "tp" / "t0" / "t1" / "t2" / "t3" / "t4" / "t5" / "t6" / "s0" / "s1" / "s2" / "s3" / "s4" / "s5" / 
      "s6" / "s7" / "s8" / "s9" / "s10" / "s11" / "fp" / "a0" / "a1" / "a2" / "a3" / "a4" / "a5" / "a6" / "a7" / "pc") {? Register::from_str(xx).map_err(|_| "Cannot parse register name.")  }

  rule signed_integer_literal() -> i64
      = s:$['-']? ("0x" / "h") hex:$(hexDigit()+) { 
        let i = u64::from_str_radix(hex, 16).unwrap() as i64;
        if s.is_some() {
          -i
        } else { 
          i
        }
      }
      / s:$['-']? "d" dec:$(digit()+) { 
        let i = u64::from_str_radix(dec, 10).unwrap() as i64;
        if s.is_some() {
          -i
        } else { 
          i
        }
      }
      / s:$['-']? "o" oct:$(digit()+) { 
        let i = u64::from_str_radix(oct, 8).unwrap() as i64;
        if s.is_some() {
          -i
        } else { 
          i
        }
      }
      / s:$['-']? "b" bin:$(digit()+) { 
        let i = u64::from_str_radix(bin, 2).unwrap() as i64;
        if s.is_some() {
          -i
        } else { 
          i
        }
      }
      / s:$['-']? dec:$(digit()+) { 
        let i = i64::from_str_radix(dec, 10).unwrap() as i64;
        if s.is_some() {
          -i
        } else { 
          i
        }
      }
    
    

  rule unsigned_integer_literal() -> u64
      =  ("0x" / "h") hex:$(hexDigit()+) { u64::from_str_radix(hex, 16).unwrap() }
      / "d" dec:$(digit()+) { u64::from_str_radix(dec, 10).unwrap() }
      / "o" oct:$(digit()+) { u64::from_str_radix(oct, 8).unwrap() }
      / "b" bin:$(digit()+) { u64::from_str_radix(bin, 2).unwrap() }
      / dec:$(digit()+) { u64::from_str_radix(dec, 10).unwrap() }


  rule arg_separator() = __() ","? __()

  // infra

  pub rule identifier() -> String = s:$(['a'..='z' | 'A'..='Z' | '_']['a'..='z' | 'A'..='Z' | '0'..='9' | '_']*) { s.to_owned() }

  rule __() = quiet!{(whitespace() / eol() / comment())*}

  rule comment()
    = singleLineComment()
    / multiLineComment()

  rule singleLineComment()
    = "//" (!eolChar() [_])*

  rule multiLineComment()
    = "/*" (!"*/" [_])* "*/"

  rule doubleQuotedString() -> String
    = "\"" s:doubleQuotedCharacter()* "\"" { s.into_iter().collect() }

  rule doubleQuotedCharacter() -> char
    = simpleDoubleQuotedCharacter()
    / simpleEscapeSequence()
    / zeroEscapeSequence()
    / hex2EscapeSequence()
    / unicodeEscapeSequence()
    / eolEscapeSequence()

  rule simpleDoubleQuotedCharacter() -> char
    = !("\"" / "\\" / eolChar()) c:$[_] { c.chars().next().unwrap() }

  rule singleQuotedString() -> String
    = "'" s:singleQuotedCharacter()* "'" { s.into_iter().collect() }

  rule singleQuotedCharacter() -> char
    = simpleSingleQuotedCharacter()
    / simpleEscapeSequence()
    / zeroEscapeSequence()
    / hex2EscapeSequence()
    / unicodeEscapeSequence()
    / eolEscapeSequence()

  rule simpleSingleQuotedCharacter() -> char
    = !("'" / "\\" / eolChar()) c:$([_]) { c.chars().next().unwrap() }


  rule simpleEscapeSequence() -> char
    = "\\" !(digit() / "x" / "u" / eolChar()) c:$([_]) {
        match c.chars().next().unwrap() {
          //'b' => '\b',
          //'f' => '\f',
          'n' => '\n',
          'r' => '\r',
          't' => '\t',
          //'v' => '\v',
          x  => x
        }
      }

  rule zeroEscapeSequence() -> char
    = "\\0" !digit() { 0u8 as char }

  rule hex2EscapeSequence() -> char
    = "\\x" value:$(hexDigit() hexDigit()) {
        char::from_u32(u32::from_str_radix(value, 16).unwrap()).unwrap()
      }

  rule digit()
    = ['0'..='9']

  rule hexDigit()
    = ['0'..='9' | 'a'..='f' | 'A'..='F']


  rule unicodeEscapeSequence() -> char
    = "\\u{" value:$(hexDigit()+) "}" {
        char::from_u32(u32::from_str_radix(value, 16).unwrap()).unwrap()
      }

  rule eolEscapeSequence() -> char
    = "\\" eol() { '\n' }


  // /* Taken from rust-peg examples */
  // /* Modeled after ECMA-262, 5th ed., 7.3. */
  rule eol()
    = "\n"
    / "\r\n"
    / "\r"
    / "\u{2028}"
    / "\u{2029}"

  rule eolChar()
    = ['\n' | '\r' | '\u{2028}' | '\u{2029}']

  /* Modeled after ECMA-262, 5th ed., 7.2. */
  rule whitespace()
    = [ ' ' | '\t' | '\u{00A0}' | '\u{FEFF}' | '\u{1680}' | '\u{180E}' | '\u{2000}'..='\u{200A}' | '\u{202F}' | '\u{205F}' | '\u{3000}'] // \v\f removed

}}
