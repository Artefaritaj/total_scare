/*
 * File: token.rs
 * Project: parser
 * Created Date: Monday December 9th 2019
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Monday, 9th December 2019 4:53:39 pm
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2019 INRIA
 */

use rv_lib::instructions::*;
use crate::parser::*;
use crate::errors::*;

use std::path::Path;
use std::fs;


#[derive(Debug,Clone,PartialEq,Eq)]
pub enum Token {
    Import(ImportFile),
    Data(Data),
    SymbolDeclaration(SymbolDeclaration),
    Instructions(Vec<(RV32I, Option<SymbolLink>)>),
}

impl Token {
    pub fn parse_tokens<P: AsRef<Path>>(path: P) -> Result<Vec<Token>, AssError> {
        let fullpath = path.as_ref().canonicalize().map_err(|e|AssError::IOError { input: format!("Cannot find path {}: {}", path.as_ref().display(), e) })?;
        let fullpath_string = fullpath.display().to_string();

        let this_file_content = fs::read_to_string(path).map_err(|e| AssError::IOError { input: format!("{}: {:?}", e, fullpath_string) })?;
        let tokens = simplass_parser::tokens(&this_file_content).map_err(|e|AssError::ParserError { input: format!("Failed to parse {}: {}", fullpath_string, e) })?;

        Ok(tokens)
    }
}