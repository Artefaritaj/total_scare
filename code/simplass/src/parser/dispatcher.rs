/*
 * File: dispatcher.rs
 * Project: parser
 * Created Date: Friday December 6th 2019
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Friday, 6th December 2019 2:39:39 pm
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2019 INRIA
 */

use rv_lib::instructions::*;
use crate::parser::*;
use crate::errors::*;

use std::collections::HashMap;

#[derive(Debug,Clone,PartialEq,Eq)]
pub enum JumpDestination {
    Literal(Offset),
    AddressSymbol(AddressSymbol),
}

#[derive(Debug,Clone,PartialEq,Eq)]
pub struct JumpDispatcher {
    pub input_register: Register,
    pub destinations:   Vec<JumpDestination>,
    pub error_handler:  JumpDestination
}

impl JumpDispatcher {

    pub fn fanout(&self) -> usize {
        self.destinations.len()
    }

    pub fn from_entries(input_register: Register, entries: Vec<JumpDispatcherEntry>) -> Result<JumpDispatcher, AssError> {
        let mut error_handler_opt: Option<JumpDestination> = None;
        let mut entries_hm: HashMap<u64, JumpDestination> = HashMap::new();

        let mut fanout = 0;
        for entry in entries {
            match entry {
                JumpDispatcherEntry::Indexed(i, dest) => {
                    if entries_hm.contains_key(&i) {
                        return Err(AssError::DispatcherCreationError { input: format!("Index {} is present several times.", i) })
                    }
                    else {
                        if (i+1) > fanout {
                            fanout = i+1;
                        }
                        entries_hm.insert(i,dest);
                    }
                },
                JumpDispatcherEntry::Wildcard(dest) => {
                    if error_handler_opt.is_some() == true {
                        return Err(AssError::DispatcherCreationError { input: format!("Wildcard is present several times in the dispatcher.") })
                    }
                    else {
                        error_handler_opt = Some(dest);
                    }
                }
            }
        }

        if let Some(error_handler) = error_handler_opt {
            let mut destinations: Vec<JumpDestination> = Vec::new();
            for i in 0..fanout {
                match entries_hm.get(&i) {
                    Some(d) => { destinations.push(d.clone()); },
                    None    => { destinations.push(error_handler.clone()); },
                }
            }

            Ok(JumpDispatcher { input_register, destinations, error_handler })
        }
        else {
            return Err(AssError::DispatcherCreationError { input: format!("Dispatcher must handle the error case with a wildcard '_'.")});
        }
    }
}

#[derive(Debug,Clone,PartialEq,Eq)]
pub enum JumpDispatcherEntry {
    Indexed(u64, JumpDestination),
    Wildcard(JumpDestination)
}