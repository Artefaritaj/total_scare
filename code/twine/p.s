/*
 * File: s.s
 * Project: twine
 * Created Date: Wednesday June 30th 2021
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Wednesday, 30th June 2021 4:33:44 pm
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2021 INRIA
 */

#[global]
@p://expecting input as a0, return as a0
    t1 <- &ptab;

    t0 <- a0 & 0xF;
    t0 <- t0 + t1;
    a0 <- [t0]b;

    return;
    

def ptab: [u8] = [5,0,1,4,7,12,3,8,13,6,9,2,15,10,11,14];