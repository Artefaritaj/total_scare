/*
 * File: key_sch.s
 * Project: twine
 * Created Date: Wednesday June 30th 2021
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Wednesday, 30th June 2021 4:33:59 pm
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2021 INRIA
 */


// @0x1040: key nibbles up to 0x1053
// each key round work on [work key] = (@0x1040) to update [work key] and generate [round key] (@0x1070)
#[global]
@key_round80: //a0 contains the round
    push ra, s0, s1;
    
    t0 <- &con;
    t0 <- t0 + a0;
    a1 <- [t0]b;//a1 holds CON^{i-1} value

    //WK1 = WK1 xor S(WK0)
    a0 <-t3 [0x1040]b;
    call &s;
    t1 <-t3 [0x1041]b;
    t1 <- t1 xor a0;
    [0x1041]b <-t3 t1;

    //WK4 = WK4 xor S(WK16)
    a0 <-t3 [0x1050]b;
    call &s;
    t1 <-t3 [0x1044]b;
    t1 <- t1 xor a0;
    [0x1044]b <-t3 t1;

    //WK7=WK7 xor CON_H^{i-1}
    a0 <-t3 [0x1047]b;
    t1 <- a1 >> 3;//CON_H
    t1 <- t1 xor a0;
    [0x1047]b <-t3 t1;

    //WK19 = WK19 xor CON_L{i-1}
    a0 <-t3 [0x1053]b;
    t1 <- a1 & 7;//CON_L
    t1 <- t1 xor a0;
    [0x1053]b <-t3 t1;

    //tmp0 = WK0
    //tmp1 = WK1
    //tmp2 = WK2
    //tmp3 = WK3
    t0 <-t3 [0x1040]b;
    t1 <-t3 [0x1041]b;
    t2 <-t3 [0x1042]b;
    t3 <-t3 [0x1043]b;
    push t0, t1, t2, t3;

    s0 <- 0;//loop counter
@kr_loop_start:

    s1 <- s0 << 2; // *4
    t0 <- 0x1040;
    s1 <- s1 + t0;

    //WKj*4 = WKj*4+4
    t0 <- [s1 + 4]b;
    [s1]b <- t0;

    //WKj*4+1 = WKj*4+5
    t0 <- [s1 + 5]b;
    [s1 + 1]b <- t0;

    //WKj*4+2 = WKj*4+6
    t0 <- [s1 + 6]b;
    [s1 + 2]b <- t0;

    //WKj*4+3 = WKj*4+7
    t0 <- [s1 + 7]b;
    [s1 + 3]b <- t0;

    s0 <- s0 + 1;
    t0 <- 4;
    if (s0 <u t0) goto &kr_loop_start;
//end of loop

    //WK16 = tmp1
    //WK17 = tmp2
    //WK18 = tmp3
    //WK19 = tmp0
    pop t3, t2, t1, t0;
    [0x1050]b <-t4 t1;
    [0x1051]b <-t4 t2;
    [0x1052]b <-t4 t3;
    [0x1053]b <-t4 t0;

    call &work2roundkey;


    pop s1, s0, ra;
    return;

#[global]
@work2roundkey:
    t0 <-t1 [0x1041]b;
    [0x1070]b <-t1 t0;
    t0 <-t1 [0x1043]b;
    [0x1071]b <-t1 t0;
    t0 <-t1 [0x1044]b;
    [0x1072]b <-t1 t0;
    t0 <-t1 [0x1046]b;
    [0x1073]b <-t1 t0;
    t0 <-t1 [0x104d]b;
    [0x1074]b <-t1 t0;
    t0 <-t1 [0x104e]b;
    [0x1075]b <-t1 t0;
    t0 <-t1 [0x104f]b;
    [0x1076]b <-t1 t0;
    t0 <-t1 [0x1050]b;
    [0x1077]b <-t1 t0;

    return;

def con: [u8] = [0x01, 0x02, 0x04, 0x08, 0x10, 0x20, 0x3,0x6,0xC,0x18,0x30,0x23,0x5,0xA,0x14,0x28,0x13,0x26,0xF,0x1E,0x3C,0x3B,0x35,0x29,0x11,0x22,0x7,0xE,0x1C,0x38,0x33,0x25,0x9,0x12,0x24,0xB];