using Pkg
Pkg.activate("scare")

include("../analysis_final/information.jl")
include("../analysis_final/loaders.jl")
include("../analysis_final/function_identifiers.jl")
include("../analysis_final/generators.jl")
include("../analysis_final/predictor.jl")
include("../analysis_final/printing.jl")

include("../analysis_final/perfect.jl")
include("../analysis_final/lossy.jl")
include("../analysis_final/noisy.jl")

using ProgressMeter
using Serialization
using LightGraphs

using Plots
using StatsPlots

function find_in_traces(traces::Array{UInt8, 2}, texts::Array{UInt8})
    #input2dual
    mcount = min(size(traces, 1), 5000)
    text_byte_count = size(texts,2)
    trace_len = size(traces, 2)
    idt = zeros(Int64, text_byte_count)

    # println("Detecting timing relation between input bytes and dual traces...")
    found = 0

    #detect time relations
    @showprogress "Detecting inputs in traces..." for b in 1:text_byte_count
        #because of dual values simplification, byte b can be linked to only one time in dual_traces
        for t in 1:trace_len
            if fractional_added_information(traces[1:mcount,t], texts[1:mcount,b]) < 0.1
                #link detected
                # println("$b -> $t")
                idt[b] = t
                found += 1
                break
            end
        end
        # print("\e[2K\e[1G $b/$text_byte_count ($found found)")
    end

    return idt
end

function test2to1(traces::Array{UInt8, 2}, i1::Int64, i2::Int64)
    (traces_count, traces_len) = size(traces)
    tcount = min(traces_count, 100000)

    
    dists = @showprogress map(i3 -> fractional_added_information2(traces[1:tcount,i3], traces[1:tcount,[i1,i2]]), 1:traces_len)
    return dists
    
end

function test1to1(traces::Array{UInt8, 2}, i1::Int64)
    (traces_count, traces_len) = size(traces)
    tcount = min(traces_count, 100000)

    
    dists = @showprogress map(i3 -> fractional_added_information(traces[1:tcount,i3], traces[1:tcount,i1]), 1:traces_len)
    return dists
    
end

# AES test vector from FIPS 197
# pt = [0x00, 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88, 0x99, 0xAA, 0xBB, 0xCC, 0xDD, 0xEE, 0xFF]
# ct = [0x5f, 0x72, 0x64, 0x15, 0x57, 0xf5, 0xbc, 0x92, 0xf7, 0xbe, 0x3b, 0x29, 0x1d, 0xb9, 0xf9, 0x1a]
# k =  [0x0, 0x1, 0x2, 0x3, 0x4, 0x5, 0x6, 0x7, 0x8, 0x9, 0xa, 0xb, 0xc, 0xd, 0xe, 0xf]

# Twine test vector
pt = [0x01, 0x23, 0x45, 0x67, 0x89, 0xAB, 0xCD, 0xEF]
ct = [0x7C, 0x1F, 0x0F, 0x80, 0xB1, 0xDF, 0x9C, 0x28]
key = [0x00, 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88, 0x99]


entropy_estimators = deserialize("entropy_estimators.bin")

traces_path = "traces.binu8"
# traces_path = "hw_traces.binu8"
# # # traces_path = "hw_noisy_traces.binu8"
inputs_path = "io.in"
# inputs_path = "io.out"
outputs_path = "io.out"
# outputs_path = "io.in"
traces_count = 200000
# # # dconfig = DetectionConfiguration(0.9, 0.9)
# # dconfig = DetectionConfiguration(0.72, 0.82)
dconfig = DetectionConfiguration(0.1, 0.1)
traces = load_traces(traces_path, 0, traces_count)
inputs = load_traces(inputs_path, 0, traces_count)
outputs = load_traces(outputs_path, 0, traces_count)
input_byte_count = size(inputs, 2)
traces = hcat(inputs, traces, outputs)

(traces, s1a) = stage_1a(traces)
(s1b_traces, s1b) = stage_1b_v2(traces, dconfig.fai_1to1_threshold)
# s1c = stage_1c(s1b_traces, collect(1:input_byte_count), dconfig, 128)

# # (rows, cols) = size(traces)

# one2ones = identify_1to1(traces, dconfig)
# (dict_centers, dict_thresholds) = compute_cluster_centers(traces, one2ones)
# one2ones = deserialize("one2ones")
# dict_centers = deserialize("dict_centers")
# dict_thresholds = deserialize("dict_thresholds")
# key_col = 647
# follower_cols = one2ones[key_col]
# cluster_cols = sort(vcat(key_col, follower_cols))
# init_col = minimum(cluster_cols)
# t = traces[1:5000,cluster_cols]
# d = compute_rows_distances(t)

# (traces, s1a) = stage_1a(traces)
# (traces, s1b) = stage_1b(traces, dconfig)
# s1c = stage_1c(traces, collect(1:input_byte_count), dconfig)

# generate_compress("traces.binu8", "traces2.binu8")
# generate_reverse("traces2.binu8", "traces_reversed.binu8")

# Perfect case

# perfect_config = DetectionConfiguration(0.1, 0.1)
# cts_name = "io.out"
# pts_name = "io.in"
# traces_count = 2000000
# stage1data = load_stage1_perfect("traces.binu8", pts_name, cts_name, perfect_config)
# serialize("s1data_perfect", stage1data)
# # stage1data = deserialize("s1data_reversed_perfect")
# traces = load_stage2_perfect("traces.binu8", pts_name, cts_name, stage1data, 200000, traces_count)
# write_traces("traces_s2.binu8", traces)
# # traces = load_traces("traces_reversed_s2.binu8")
# pts = load_traces(pts_name, 0, traces_count)
# cts = load_traces(cts_name, 0, traces_count)
# p = build_predictor(traces, pts, cts, stage1data)
# @assert(predict(p, pt) == ct)

# Perfect decrypt
# traces_count = 2000000
# cts_name = "io.in"
# pts_name = "io.out"
# # stage1data = load_stage1_perfect("traces_decrypt.binu8", pts_name, cts_name, perfect_config)
# # serialize("s1data_perfect", stage1data)
# stage1data = deserialize("s1data")
# # traces = load_stage2_perfect("traces_decrypt.binu8", pts_name, cts_name, stage1data, 200000, traces_count)
# # write_traces("traces_decrypt_s2.binu8", traces)
# traces = load_traces("traces_decrypt_s2.binu8")
# pts = load_traces(pts_name, 0, traces_count)
# cts = load_traces(cts_name, 0, traces_count)
# p = build_predictor(traces, pts, cts, stage1data)
# @assert(predict(p, ct) == pt)

# Lossy case

# lossy_config = DetectionConfiguration(0.72, 0.72)

# need to modify identify_1to1, to deal with more complex cases
# Row: 1 Lead col: 1652Not just 1 inside threshold: 0
# Best: 53, 0.5102665969863205 < 1.4142135623730951

# traces = load_traces(traces_path, 0, traces_count)
# inputs = load_traces(inputs_path, 0, traces_count)
# outputs = load_traces(outputs_path, 0, traces_count)
# input_byte_count = size(inputs, 2)
# traces = hcat(inputs, traces, outputs)

# (traces, s1a) = stage_1a(traces)

# (rows, cols) = size(traces)
# (reduced_traces, s1b) = stage_1b(traces, dconfig)
# s1c = stage_1c(reduced_traces, collect(1:input_byte_count), dconfig, 128)

# generating data 
# generate_hw("traces.binu8", "hw_traces.binu8", 10000)
# stage1data = load_stage1_perfect("hw_traces.binu8", "io.in", "io.out", lossy_config)
# serialize("s1data", stage1data)
# # stage1data = deserialize("s1data")
# traces = load_stage2_perfect("hw_traces.binu8", "io.in", "io.out", stage1data)
# write_traces("hw_traces_s2.binu8", traces)
# # traces = load_traces("hw_traces_s2.binu8")
# pts = load_traces("io.in")
# cts = load_traces("io.out")
# println(size(traces))
# io_indices = find_io_indices(traces, pts, cts)
# println("IO indices: $io_indices")
# enrich(traces, stage1data.c.triplets, io_indices)

# p = build_predictor(traces, pts, cts, stage1data)
# @assert(predict(p, pt) == ct)

# Lossy decrypt

# stage1data = load_stage1_perfect("hw_traces_decrypt.binu8", "io.out", "io.in", lossy_config)
# serialize("s1data", stage1data)
# # stage1data = deserialize("s1data")
# traces = load_stage2_perfect("hw_traces_decrypt.binu8", "io.out", "io.in", stage1data, 200000,6000000)
# write_traces("hw_traces_decrypt_s2.binu8", traces)
# # traces = load_traces("hw_traces_s2.binu8")
# pts = load_traces("io.out")
# cts = load_traces("io.in")
# println(size(traces))
# io_indices = find_io_indices(traces, pts, cts)
# println("IO indices: $io_indices")
# enrich(traces, stage1data.c.triplets, io_indices)

# p = build_predictor(traces, pts, cts, stage1data)
# @assert(predict(p, ct) == pt)

# Noisy case
# generate_hw_noisy("traces.binu8", "hw_noisy_traces.binu8")

# noisy_config = DetectionConfiguration(0.8, 0.8)
# (stage1data, traces) = load_stage1_noisy("hw_noisy_traces.binu8", "io.in", "io.out", noisy_config)
# serialize("s1data", stage1data)
# stage1data = deserialize("s1data")
# (triplets, traces) = load_stage2_noisy("hw_noisy_traces.binu8", "io.in", "io.out", stage1data, 200000)
# # traces = load_stage2_noisy("hw_noisy_traces.binu8", "pts.binu8", "cts.binu8", stage1data)
# write_traces("hw_noisy_traces_s2.binu8", traces)

# traces = load_traces("hw_noisy_traces_s2.binu8") #****
# triplets = deserialize("triplets") #****
# traces_len = size(traces, 2) #****
# rich_state = zeros(Bool, traces_len) #****

# rich_state[1:64] .= true

# pts = load_traces("io.in") 
# cts = load_traces("io.out") 
# find_and_replace(traces, pts, rich_state) 
# find_and_replace(traces, cts, rich_state, :end) 

# enrich_noisy(traces, triplets, rich_state)
# p = build_predictor(traces, pts, cts, triplets, true)