# @Author: Hélène Le Bouder, Ronan Lashermes
# @Date:   19-10-2016
# @Email:  helene.le-bouder@inria.fr, ronan.lashermes@inria.fr
# @License: CC-by-nc

#--------------------------------------------------------
# AES
#--------------------------------------------------------


function AES(text::Array{UInt8,1},master_key::Array{UInt8,1})

	state=reshape(text,4,4)
	key=KeyGen(master_key)

	state=AddRoundKey(state,key,0)

	for i=1:9
		state=SubBytes(state)
		state=ShiftRows(state)
		state=Mixcolumns(state)
		state=AddRoundKey(state,key,i)

	end

	state=SubBytes(state)
	state=ShiftRows(state)
	state=AddRoundKey(state,key,10)
	return reshape(state,16,1)[:] # cipher
end

function round1(text::Array{UInt8,1},master_key::Array{UInt8,1})

	state=reshape(text,4,4)
	key=KeyGen(master_key)

	state=AddRoundKey(state,key,0)
	state=SubBytes(state)
	state=ShiftRows(state)
	state=Mixcolumns(state)

	return reshape(state,16,1)[:] # cipher
end

#-----------------------------------------------------------------------------------------------------------------------------
#--------------------------------------------------------
# SubBytes
#--------------------------------------------------------

function SubBytes(state_in::Array{UInt8,2})::Array{UInt8,2}
	sbox.(state_in)
end


sboxtab=convert.(UInt8, [99, 124, 119, 123, 242, 107, 111, 197, 48, 1, 103, 43, 254, 215, 171, 118, 202, 130, 201, 125, 250, 89, 71, 240, 173, 212, 162, 175, 156, 164, 114, 192, 183, 253, 147, 38, 54, 63, 247, 204, 52, 165, 229, 241, 113, 216, 49, 21, 4, 199, 35, 195, 24, 150, 5, 154, 7, 18, 128, 226, 235, 39, 178, 117, 9, 131, 44, 26, 27, 110, 90, 160, 82, 59, 214, 179, 41, 227, 47, 132, 83, 209, 0, 237, 32, 252, 177, 91, 106, 203, 190, 57, 74, 76, 88, 207, 208, 239, 170, 251, 67, 77, 51, 133, 69, 249, 2, 127, 80, 60, 159, 168, 81, 163, 64, 143, 146, 157, 56, 245, 188, 182, 218, 33, 16, 255, 243, 210, 205, 12, 19, 236, 95, 151, 68, 23, 196, 167, 126, 61, 100, 93, 25, 115, 96, 129, 79, 220, 34, 42, 144, 136, 70, 238, 184, 20, 222, 94, 11, 219, 224, 50, 58, 10, 73, 6, 36, 92, 194, 211, 172, 98, 145, 149, 228, 121, 231, 200, 55, 109, 141, 213, 78, 169, 108, 86, 244, 234, 101, 122, 174, 8, 186, 120, 37, 46, 28, 166, 180, 198, 232, 221, 116, 31, 75, 189, 139, 138, 112, 62, 181, 102, 72, 3, 246, 14, 97, 53, 87, 185, 134, 193, 29, 158, 225, 248, 152, 17, 105, 217, 142, 148, 155, 30, 135, 233, 206, 85, 40, 223, 140, 161, 137, 13, 191, 230, 66, 104, 65, 153, 45, 15, 176, 84, 187, 22])
invsboxtab=convert.(UInt8, [82, 9, 106, 213, 48, 54, 165, 56, 191, 64, 163, 158, 129, 243, 215, 251, 124, 227, 57, 130, 155, 47, 255, 135, 52, 142, 67, 68, 196, 222, 233, 203, 84, 123, 148, 50, 166, 194, 35, 61, 238, 76, 149, 11, 66, 250, 195, 78, 8, 46, 161, 102, 40, 217, 36, 178, 118, 91, 162, 73, 109, 139, 209, 37, 114, 248, 246, 100, 134, 104, 152, 22, 212, 164, 92, 204, 93, 101, 182, 146, 108, 112, 72, 80, 253, 237, 185, 218, 94, 21, 70, 87, 167, 141, 157, 132, 144, 216, 171, 0, 140, 188, 211, 10, 247, 228, 88, 5, 184, 179, 69, 6, 208, 44, 30, 143, 202, 63, 15, 2, 193, 175, 189, 3, 1, 19, 138, 107, 58, 145, 17, 65, 79, 103, 220, 234, 151, 242, 207, 206, 240, 180, 230, 115, 150, 172, 116, 34, 231, 173, 53, 133, 226, 249, 55, 232, 28, 117, 223, 110, 71, 241, 26, 113, 29, 41, 197, 137, 111, 183, 98, 14, 170, 24, 190, 27, 252, 86, 62, 75, 198, 210, 121, 32, 154, 219, 192, 254, 120, 205, 90, 244, 31, 221, 168, 51, 136, 7, 199, 49, 177, 18, 16, 89, 39, 128, 236, 95, 96, 81, 127, 169, 25, 181, 74, 13, 45, 229, 122, 159, 147, 201, 156, 239, 160, 224, 59, 77, 174, 42, 245, 176, 200, 235, 187, 60, 131, 83, 153, 97, 23, 43, 4, 126, 186, 119, 214, 38, 225, 105, 20, 99, 85, 33, 12, 125])


function sbox(byte::UInt8)::UInt8
	return sboxtab[byte+1]#index starts at 1!!!
end
function invsbox(byte::UInt8)::UInt8
	return invsboxtab[byte+1]#index starts at 1!!!
end

#-----------------------------------------------------------------------------------------------------------------------------
#--------------------------------------------------------
# ShiftRows
#--------------------------------------------------------

function ShiftRows(state_in::Array{UInt8,2})::Array{UInt8,2}

	state_out = zeros(UInt8, 4,4)

	#shift row 1
	state_out[1,1]= state_in[1,1];
	state_out[1,2]= state_in[1,2];
	state_out[1,3]= state_in[1,3];
	state_out[1,4]= state_in[1,4];

	#shift row 2
	state_out[2,1]= state_in[2,2];
	state_out[2,2]= state_in[2,3];
	state_out[2,3]= state_in[2,4];
	state_out[2,4]= state_in[2,1];

	#shift row 3
	state_out[3,1]= state_in[3,3];
	state_out[3,2]= state_in[3,4];
	state_out[3,3]= state_in[3,1];
	state_out[3,4]= state_in[3,2];

	#shift row 4
	state_out[4,1]= state_in[4,4];
	state_out[4,2]= state_in[4,1];
	state_out[4,3]= state_in[4,2];
	state_out[4,4]= state_in[4,3];


	return state_out
end
#-----------------------------------------------------------------------------------------------------------------------------

#-----------------------------------------------------------------------------------------------------------------------------
#--------------------------------------------------------
# fonction Mixcolumn
#--------------------------------------------------------

function Mixcolumns(state_in::Array{UInt8,2})::Array{UInt8,2}

	columnA=zeros(UInt8, 4,1)
	columnB=zeros(UInt8, 4,1)
	state_out=zeros(UInt8, 4,4)

	for j=1:4

		columnA=state_in[:,j]

		columnB[1] = xtimes(columnA[1]) ⊻ times3(columnA[2]) ⊻columnA[3] ⊻ columnA[4]
		columnB[2] = columnA[1]  ⊻ xtimes(columnA[2]) ⊻ times3(columnA[3]) ⊻ columnA[4]
		columnB[3] = columnA[1]  ⊻ columnA[2]   ⊻ xtimes(columnA[3]) ⊻ times3(columnA[4])
		columnB[4] = times3(columnA[1]) ⊻ columnA[2]   ⊻ columnA[3]   ⊻ xtimes(columnA[4])

		state_out[:,j]=columnB

	end

	return state_out
end

function xtimes(a::UInt8)::UInt8
	ai = convert(Int32, a)

	i::Int32 = if(ai < 128)
		2*ai
	else
		0xFF&((2*ai)⊻0x1B)
	end

	convert(UInt8, i)
end

function times3(a::UInt8)::UInt8
	xtimes(a)⊻a
end

#-----------------------------------------------------------------------------------------------------------------------------
#--------------------------------------------------------
# AddRoundKey
#--------------------------------------------------------

function AddRoundKey(state_in::Array{UInt8,2},cle::Array{UInt8,3},r::Int64)::Array{UInt8,2}
	K=reshape(cle[r+1,:,:],4,4)
	return xor.(state_in, K)
end
#-----------------------------------------------------------------------------------------------------------------------------


#-----------------------------------------------------------------------------------------------------------------------------
#--------------------------------------------------------
# Key generation
#--------------------------------------------------------
function KeyGen(master_key::Array{UInt8,1})::Array{UInt8,3}
	key=zeros(UInt8, 11,4,4)
	key[1,:,:]=reshape(master_key,4,4)

	for r=2:11
		FillColumn1(key,r)
		FillOtherColumns(key,r)
	end

	return key
end

#--------------------------------------------------------
#First column of a round key
#--------------------------------------------------------
rcon=convert.(UInt8, [1 ,2, 4, 8, 16, 32, 64, 128, 27, 54])
function FillColumn1(key::Array{UInt8,3}, r::Int64)::Array{UInt8,3}

	column=zeros(UInt8, 4,1)

	#la rotation
	column[1]=key[r-1,2,4]
	column[2]=key[r-1,3,4]
	column[3]=key[r-1,4,4]
	column[4]=key[r-1,1,4]

	


	#passage sbox + rcon + xor
	key[r,1,1]=sbox(column[1]) ⊻ rcon[r-1] ⊻ key[r-1,1,1]

	for i=2:4
		key[r,i,1]=sbox(column[i])⊻key[r-1,i,1]
	end

	return key
end

#--------------------------------------------------------
# Other columns in a round key
#--------------------------------------------------------
function FillOtherColumns(key::Array{UInt8,3}, r::Int64)::Array{UInt8,3}
	for i=2:4
		for j=1:4
			key[r,j,i]=key[r-1,j,i]⊻key[r,j,i-1]
		end
	end
	return key
end
#-----------------------------------------------------------------------------------------------------------------------------
