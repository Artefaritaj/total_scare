using ProgressMeter

function trace_count(path::String)
  f = open(path, "r")

  if(eof(f) == true)# if file empty, quit
    close(f)
    return 0
  end

  trace_len::Int32 = read(f, Int32)# read trace length
  trace_count::Int32 = read(f, Int32)# read trace count

  return trace_count
end


function loading_simplify_by_parts(simplify, path::String, batch_size::Int64 = 10000, max::Int64 = 0)

  if max == 0
    max = trace_count(path)
  end

  @assert max % batch_size == 0

  traces::Array{UInt8,2} = simplify(load_traces_no_progress(path, 0, batch_size))
  # index::Int64 = 1
  loop_count::Int64 = (max / batch_size) - 1

  @showprogress for index in 1:loop_count
  # while index*batch_size < max
      traces = vcat(traces, simplify(load_traces_no_progress(path, index*batch_size, batch_size)))
      index += 1
  end

  traces
end

function loading_zip_simplify_by_parts(simplify, path1::String, path2::String, path3::String, batch_size::Int64, max::Int64 = 0)

  if max == 0
    max = trace_count(path1)
  end

  @assert max % batch_size == 0

  traces::Array{UInt8,2} = simplify(
                              hcat(
                                load_traces_no_progress(path1, 0, batch_size),
                                load_traces_no_progress(path2, 0, batch_size),
                                load_traces_no_progress(path3, 0, batch_size)  
                              ))
  # index::Int64 = 1
  loop_count::Int64 = (max / batch_size) - 1

  @showprogress for index in 1:loop_count
  # while index*batch_size < max
      traces = vcat(traces, simplify(
                              hcat(
                                load_traces_no_progress(path1, index*batch_size, batch_size),
                                load_traces_no_progress(path2, index*batch_size, batch_size),
                                load_traces_no_progress(path3, index*batch_size, batch_size)
                              )))
      index += 1
  end

  traces
end

function load_traces_no_progress(filepath::String, min::Int64 = 0, read_count::Int64 = 0)::Array{UInt8, 2}
  f = open(filepath, "r")

  if(eof(f) == true)# if file empty, quit
    close(f)
    return 0
  end

  trace_len::Int32 = read(f, Int32)# read trace length
  trace_count::Int32 = read(f, Int32)# read trace count

  if read_count == 0
    read_count = trace_count
  end

  # max = min + read_count

  #println("Trace len $(trace_len), trace count $(trace_count )")

  M=zeros(UInt8, read_count, trace_len)#init the result matrix

  # i::Int32 = 1
  temp = Array{UInt8}(undef,trace_len)

  seek(f, 8 + min*trace_len)

  # while eof(f) == false && i <= read_count # read each trace and put in matrix
  for i in 1:read_count
    if eof(f) == true
      break
    end

    # M[i, :] = read(f, Int8, trace_len)
    read!(f, temp)
    M[i,:] = temp
    i+=1
  end

  close(f)
  return M

end


function load_traces(filepath::String, min::Int64 = 0, read_count::Int64 = 0)::Array{UInt8, 2}
  f = open(filepath, "r")

  if(eof(f) == true)# if file empty, quit
    close(f)
    return 0
  end

  trace_len::Int32 = read(f, Int32)# read trace length
  trace_count::Int32 = read(f, Int32)# read trace count

  if read_count == 0
    read_count = trace_count
  end

  # max = min + read_count

  #println("Trace len $(trace_len), trace count $(trace_count )")

  M=zeros(UInt8, read_count, trace_len)#init the result matrix

  # i::Int32 = 1
  temp = Array{UInt8}(undef,trace_len)

  seek(f, 8 + min*trace_len)

  # while eof(f) == false && i <= read_count # read each trace and put in matrix
  @showprogress 1 "Loading traces ($filepath)... " for i in 1:read_count
    if eof(f) == true
      break
    end

    # M[i, :] = read(f, Int8, trace_len)
    read!(f, temp)
    M[i,:] = temp
    i+=1
  end

  close(f)
  return M

end

function write_traces(filepath::String, traces::Array{UInt8, 2})
  f = open(filepath, "w")

  (trace_count::Int32, trace_len::Int32) = size(traces)

  write(f, trace_len)
  write(f, trace_count)

  #println("Trace len $(trace_len), trace count $(trace_count )")

  for i in 1:trace_count
    write(f, traces[i,:])
  end

  close(f)
end

function transform_write_traces(transformation, input_path::String, output_path::String, batch_size::Int64 = 100000)

  traces = transformation(load_traces(input_path, 0, batch_size))
  output_len::Int32 = size(traces, 2)
  # println("Outpul length is $output_len")

  traces_count::Int32 = trace_count(input_path)
  

  of = open(output_path, "w")

  write(of, output_len)
  write(of, traces_count)


  @assert traces_count % batch_size == 0

  loop_count::Int64 = (traces_count / batch_size) - 1


  @showprogress "Transforming traces..." for index in 0:loop_count
      traces = transformation(load_traces_no_progress(input_path, index*batch_size, batch_size))
      for i in 1:size(traces, 1)
        write(of, traces[i,:])
      end
      index += 1
  end

  close(of)

end