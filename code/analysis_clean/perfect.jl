
@inline function allequal(x)
    length(x) < 2 && return true
    e1 = x[1]
    @inbounds for i=2:length(x)
        x[i] == e1 || return false
    end
    return true
end

struct Stage1aData
    constants::Set{Int64}
    same::Set{Int64}
end

struct Stage1bData
    one2ones::Dict{Int64, Array{Int64, 1}}
    centers::Dict{Int64,Array{Float64,2}}
    thresholds::Dict{Int64,Float64}
    mappings::Array{Int64,1}
end

struct Stage1cData
    triplets::Array{Int64,2}
end

struct Stage1Data
    a::Stage1aData
    b::Stage1bData
    c::Stage1cData
    active_nodes::Array{Int64,1}
end

function remove_constants(m::Array{UInt8,2})::Array{UInt8,2}
	# convert to vector of vectors (on columns)	

	vv = [m[:,i] for i in 1:size(m, 2)]
	f = filter!(v -> allequal(v) == false,vv)
    hcat(f...)
end

function identify_constants(m::Array{UInt8, 2})::Set{Int64}
    (rows, cols) = size(m)
    s::Set{Int64} = Set()
    for i in 1:cols
        if allequal(m[:,i]) == true
            push!(s,i)
        end
    end

    return s
end

function remove_same(m::Array{UInt8,2})::Array{UInt8, 2}
    # convert to vector of vectors (on columns)	
    vv = [m[:,i] for i in 1:size(m, 2)]
    vv = unique(vv)
    hcat(vv...)
end

function identify_same(m::Array{UInt8, 2})::Set{Int64}
    (rows, cols) = size(m)
    s::Set{Int64} = Set()

    # @showprogress "Identify same..." for i in 1:cols
    #     for j in i+1:cols
    #         if isequal(m[:,i], m[:,j])
    #             push!(s,j)
    #         end
    #     end
    # end

    idx = unique(z -> m[:, z], 1:size(m,2))
    return Set(setdiff(1:size(m,2), idx))
end

function delete_columns(traces::Array{UInt8,2}, col_indices::Set{Int64})
    new_indices = setdiff(1:size(traces,2), col_indices)
    return copy(traces[:,new_indices])
end

function keep_columns(traces::Array{UInt8,2}, col_indices::Set{Int64})
    return copy(traces[:,col_indices])
end

# Loading is done in separate stages to be able to load sufficient data without filling the available RAM
# In Stage 1 we load less data to identify the simplifications
# 1.a: remove constants and identical columns (for perfect and lossy only). Produce a list of columns to delete.
# 1.b: identify 1to1, reduce traces by clustering the 1 to 1 data. Produce "clustered" data, and the clustering functions.
# 1.c: identify 2to1, identify all points of interest. Produce the list of columns of interest, plus the 2to1 indices (called "triplets"). Traces can be discarded at this step.
# Stage 2: load more traces, while doing the simplication per batch.

# we can do this simplification in perfect and lossy cases only
function loading_by_parts_no_constants_no_same(traces_path::String, batch_size::Int64, max::Int64)
    return loading_simplify_by_parts(traces_path, batch_size, max) do x::Array{UInt8, 2}
        remove_same(remove_constants(x))
    end
end

function load_stage1_perfect(traces_path::String, inputs_path::String, outputs_path::String, dconfig::DetectionConfiguration, traces_count::Int64 = 100000)::Stage1Data
    traces = load_traces(traces_path, 0, traces_count)
    inputs = load_traces(inputs_path, 0, traces_count)
    outputs = load_traces(outputs_path, 0, traces_count)
    input_byte_count = size(inputs, 2)
    traces = hcat(inputs, traces, outputs)

    (traces, s1a) = stage_1a(traces)
    (traces, s1b) = stage_1b(traces, dconfig)
    s1c = stage_1c(traces, collect(1:input_byte_count), dconfig, 128)

    active_times = active_times_from_triplets(s1c.triplets)

    return Stage1Data(s1a, s1b, s1c, active_times)
end

function load_stage2_perfect(traces_path::String, inputs_path::String, output_path::String, stage1data::Stage1Data, batch_size::Int64 = 100000, traces_count::Int64 = 0)
    return loading_zip_simplify_by_parts(inputs_path, traces_path, output_path, batch_size, traces_count) do traces::Array{UInt8,2}
        traces = delete_columns(traces, stage1data.a.constants)
        traces = delete_columns(traces, stage1data.a.same)
        
        (traces, _) = reduce_one2ones(traces, stage1data.b.one2ones, stage1data.b.centers,stage1data.b.thresholds)
        traces
    end
end

# remove constants and redundant columns
function stage_1a(traces::Array{UInt8,2})
    print("Removing constants... ")
    constants = identify_constants(traces)
    traces = delete_columns(traces, constants)
    println("Traces have $(size(traces,2)) columns")

    GC.gc()

    print("Removing duplicates... ")
    same = identify_same(traces)
    traces = delete_columns(traces, same)
    println("Traces have $(size(traces,2)) columns")

    GC.gc()

    return (traces, Stage1aData(constants, same))
end

function identify_1to1(traces::Array{UInt8,2}, dconfig::DetectionConfiguration)::Dict{Int64, Array{Int64, 1}}
    (rows, cols) = size(traces)

    one2ones::Dict{Int64, Array{Int64, 1}} = Dict()
    treated::Set{Int64} = Set()

    @showprogress "Identifying 1 to 1 functions..." for i in 1:cols
        if !(i in treated)
            related = OneToOne_find(traces, i, dconfig)
            union!(treated, Set(related))
            if length(related) > 0
                one2ones[i] = related
            end
        end 
    end

    return one2ones
end

using LightGraphs
function identify_1to1_v2(traces::Array{UInt8,2}, noise_threshold::Float64)::Dict{Int64, Array{Int64, 1}}
    (rows, cols) = size(traces)

    one2ones::Dict{Int64, Array{Int64, 1}} = Dict()
    g = SimpleDiGraph(cols)
    

    @showprogress "Identifying 1 to 1 functions (v2)..." for i in 1:cols
        related = OneToOne_find_total(traces, i, noise_threshold)
        
        for r in related
            add_edge!(g, i, r)
        end 
    end

    c = connected_components(g)
    for i in 1:size(c,1)
        cluster = sort(c[i])
        key = cluster[1]
        followers = cluster[2:end]
        if length(followers) > 0
            one2ones[key] = followers
        end
    end

    return one2ones
end


# function identify_1to1_gpu(traces::Array{UInt8, 2}, noise_threshold::Float32)#::Dict{Int64, Array{Int64, 1}}
#     (rows, cols) = size(traces)
#     tcount = max(rows, 50000)

#     gpuTraces = CuArray(traces[1:tcount,:])
#     gpuFAI = fill(0.0f0, cols, cols)

#     @showprogress "FAI matrix computation..." 1 for i1 in 1:(cols-1)
#         for i2 in (i1+1):cols
#             gpuFAI[i1, i2] = fractional_added_information_gpu(gpuTraces[:,i2], gpuTraces[:,i1])
#         end
#     end

#     return Array(gpuFAI)

# end

function identify_1to1_cpu(traces::Array{UInt8, 2}, noise_threshold::Float32)#::Dict{Int64, Array{Int64, 1}}
    (rows, cols) = size(traces)
    tcount = max(rows, 50000)

    cpuTraces = traces[1:tcount,:]
    cpuFAI = fill(0.0f0, cols, cols)

    @showprogress "FAI matrix computation..." 1 for i1 in 1:(cols-1)
        for i2 in (i1+1):cols
            f = fractional_added_information(cpuTraces[:,i2], cpuTraces[:,i1])
            cpuFAI[i1, i2] = f
            cpuFAI[i2, i1] = f
        end
    end

    return cpuFAI

end

function compute_cluster_centers(traces::Array{UInt8,2}, one2ones::Dict{Int64,Array{Int64,1}})#::Dict{Int64,Array{Float64,2}}
    dict_centers::Dict{Int64,Array{Float64,2}} = Dict()
    dict_thresholds::Dict{Int64,Float64} = Dict()

    @showprogress "Finding clusters..." for (key_col, follower_cols) in one2ones
        cluster_cols = sort(vcat(key_col, follower_cols))
        init_col = minimum(cluster_cols)
        (centers, threshold) = kmedoids_centers(traces[:,cluster_cols])
        dict_centers[init_col] = centers
        dict_thresholds[init_col] = threshold
    end

    return (dict_centers, dict_thresholds)
end

function TwoToOne_detect(traces::Array{UInt8, 2}, newcol::Array{UInt8, 1}, i, noise_threshold, window::Int64 = 0)
    (rows, cols) = size(traces)
    tcount = min(rows, 50000)
    
    
    ts = 1:cols

    if window != 0
        ts = max(1, i-window):min(cols, i+window)
    end

    println(ts)

    # 1) newcol is output
    
    th = fractional_added_information(newcol[1:tcount], traces[1:tcount,i]) - noise_threshold
    println("Threshold is $th")
    selected_out = filter(j-> 
    begin
        fractional_added_information2(newcol[1:tcount], traces[1:tcount,[i,j]]) < th
    end,
    # pre_selected)
    ts)


    if length(selected_out) > 0
        println(selected_out)
        return true
    end

    # 2) newcol is input
    selected_in = filter(j-> 
    begin
        th = fractional_added_information(traces[1:tcount,max(i,j)], newcol[1:tcount]) - noise_threshold
        fractional_added_information2(traces[1:tcount,max(i,j)], hcat(traces[1:tcount,min(i,j)], newcol)) < th
    end,
    ts)

    if length(selected_in) > 0
        return true
    end

    return false
end

function compute_cluster_center(traces::Array{UInt8,2}, key_col::Int64, follower_cols::Array{Int64,1}, noise_threshold, window::Int64 = 0)
    # 1) clustering
    cluster_cols = sort(vcat(key_col, follower_cols))
    init_col = minimum(cluster_cols)
    (centers, threshold) = kmedoids_centers(traces[:,cluster_cols])

    # 2) create unified column
    (rows, cols) = size(traces)
    new_col = zeros(UInt8, rows)
    @showprogress for i in 1:rows
        v = traces[i,cluster_cols]
        # print("\e[2K\e[1G Row: $i Lead col: $pre_mapping_init  ($cluster_cols) ($v)")
        new_col[i] = assign_center(v, centers, threshold)
    end

    # 3) Evaluate if times in cluster can be absorbed
    # they cannot if part of a 2-to-1 function
    for i in cluster_cols
        not_absorbed = TwoToOne_detect(traces, new_col, i, noise_threshold, window)
        println("$i -> Absorbed = $(!not_absorbed)")
    end
end

# Identify and reduce 1to1
function stage_1b(traces::Array{UInt8, 2}, dconfig::DetectionConfiguration)
    (rows, cols) = size(traces)

    one2ones = identify_1to1_v2(traces, dconfig.fai_1to1_threshold)
    (dict_centers, dict_thresholds) = compute_cluster_centers(traces, one2ones)
    (reduced_traces, mappings) = reduce_one2ones(traces, one2ones, dict_centers, dict_thresholds)
    return (reduced_traces, Stage1bData(one2ones, dict_centers, dict_thresholds, mappings))
end

function reduce_one2ones(traces::Array{UInt8,2}, one2ones::Dict{Int64, Array{Int64, 1}}, dict_centers::Dict{Int64,Array{Float64,2}}, dict_thresholds::Dict{Int64, Float64})
    # ::Array{UInt8, 2}
    (rows, _)= size(traces)
    # maps old indices to new ones
    mappings = collect(1:size(traces,2))
    
    @showprogress "Reducing 1 to 1..."  for (key_col, follower_cols) in one2ones
        cluster_cols = sort(vcat(key_col, follower_cols))
        init_col = minimum(cluster_cols)
        pre_mapping_init = copy(init_col)
        # dictionnaries key is the init_col before remapping
        threshold = dict_thresholds[init_col]
        centers = dict_centers[init_col]

        # println("Trace length: $(size(traces, 2))")
        # println("Initial cluster: $cluster_cols")
        
        # apply mapping
        init_col = mappings[init_col]
        cluster_cols = mappings[cluster_cols]

        # println("Mapped cluster: $cluster_cols")
        # replace the whole cluster by only one column
        # compute this column here
        new_col = zeros(UInt8, rows)
        for i in 1:rows
            v = traces[i,cluster_cols]
            # print("\e[2K\e[1G Row: $i Lead col: $pre_mapping_init  ($cluster_cols) ($v)")
            new_col[i] = assign_center(v, centers, threshold)
        end

        new_indices = setdiff(1:size(traces,2), cluster_cols)
        # keep chronological order
        before = filter(i -> i < init_col, new_indices)
        after = filter(i -> i > init_col, new_indices)

        # update mappings

        push!(new_indices, init_col)
        new_indices = sort(new_indices)
        revert_indices = zeros(Int64,size(traces,2))

        for i in 1:length(new_indices)
            revert_indices[new_indices[i]] = i
        end

        for i in 1:length(mappings)
            ri = revert_indices[mappings[i]]
            if ri == 0
                # println("$i: $(mappings[i])")
                mappings[i] = init_col
            else
                mappings[i] = ri
            end
        end

        traces = hcat(traces[:, before], new_col, traces[:,after ])
    end

    return (traces, mappings)
end

function stage_1c(traces::Array{UInt8,2}, input_indices::Array{Int64,1}, dconfig::DetectionConfiguration, window::Int64 = 0)
    triplets = two2one_graph(traces, input_indices, dconfig, window)
    return Stage1cData(triplets)
end

function build_predictor(traces::Array{UInt8,2}, inputs::Array{UInt8,2}, outputs::Array{UInt8,2}, stage1data::Stage1Data)
    return Predictor(traces,inputs, outputs, stage1data.c.triplets)
end

function build_predictor(traces::Array{UInt8,2}, inputs::Array{UInt8,2}, outputs::Array{UInt8,2}, triplets::Array{Int64,2}, noisy::Bool)
    return Predictor(traces,inputs, outputs, triplets, noisy)
end

function build_predictor(traces::Array{UInt8,2}, inputs::Array{UInt8,2}, outputs::Array{UInt8,2}, triplets::Array{Int64,2})
    return Predictor(traces,inputs, outputs, triplets)
end





function computing_dual_traces(traces::Array{UInt8, 2}, noise_threshold::Float64, window = 0)
    (rows, _) = size(traces)
    tcount = min(rows, 100000)

    # 1) find one to one function
    # 1.a) identify connected information graph
    oneToOnes = identify_1to1_v2(traces, noise_threshold)

    # 1.b) verify that 1-to-1 chain nodes can be absorbed
    
    
    return traces
end