
struct Stage1bDataNoisy
    one2ones::Dict{Int64, Array{Int64, 1}}
    centers::Dict{Int64,Array{Float64,2}}
    mappings::Array{Int64,1}
end

struct Stage1DataNoisy
    # no constants, no same because of noise
    b::Stage1bDataNoisy
    c::Stage1cData
    active_nodes::Array{Int64,1}
end


function load_stage1_noisy(traces_path::String, inputs_path::String, outputs_path::String, dconfig::DetectionConfiguration, traces_count::Int64 = 100000)#::Stage1DataNoisy
    traces = load_traces(traces_path, 0, traces_count)
    inputs = load_traces(inputs_path, 0, traces_count)
    outputs = load_traces(outputs_path, 0, traces_count)
    input_byte_count = size(inputs, 2)
    traces = hcat(inputs, traces, outputs)


    (traces, s1b) = stage_1b_noisy(traces, dconfig)
    s1c = stage_1c(traces, collect(1:input_byte_count), dconfig)

    active_times = active_times_from_triplets(s1c.triplets)

    return (Stage1DataNoisy(s1b, s1c, active_times), traces)
end

function compute_cluster_centers_noisy(traces::Array{UInt8,2}, one2ones::Dict{Int64,Array{Int64,1}}, thstd::Float64 = 3.0)#::Dict{Int64,Array{Float64,2}}
    dict_centers::Dict{Int64,Array{Float64,2}} = Dict()

    @showprogress "Finding clusters..." for (key_col, follower_cols) in one2ones
        cluster_cols = sort(vcat(key_col, follower_cols))
        init_col = minimum(cluster_cols)
        centers = stochastic_centers(traces[:,cluster_cols], thstd)
        dict_centers[init_col] = centers
    end 

    return dict_centers
end

# Identify and reduce 1to1
function stage_1b_noisy(traces::Array{UInt8, 2}, dconfig::DetectionConfiguration)
    (rows, cols) = size(traces)

    one2ones = identify_1to1(traces, dconfig)
    dict_centers = compute_cluster_centers_noisy(traces, one2ones)
    (reduced_traces, mappings) = reduce_one2ones_noisy(traces, one2ones, dict_centers)
    return (reduced_traces, Stage1bDataNoisy(one2ones, dict_centers, mappings))
end

function reduce_one2ones_noisy(traces::Array{UInt8,2}, one2ones::Dict{Int64, Array{Int64, 1}}, dict_centers::Dict{Int64,Array{Float64,2}})
    # ::Array{UInt8, 2}
    (rows, _)= size(traces)
    # maps old indices to new ones
    mappings = collect(1:size(traces,2))
    
    @showprogress "Reducing 1 to 1..."  for (key_col, follower_cols) in one2ones
        # println("$key_col")
        cluster_cols = sort(vcat(key_col, follower_cols))
        init_col = minimum(cluster_cols)
        # dictionnaries key is the init_col before remapping
        centers = dict_centers[init_col]

        if size(centers,1) > 256
            println("oups: $init_col")
        end

        # println("Trace length: $(size(traces, 2))")
        # println("Initial cluster: $cluster_cols")
        
        # apply mapping
        init_col = mappings[init_col]
        cluster_cols = mappings[cluster_cols]

        # println("Mapped cluster: $cluster_cols")
        # replace the whole cluster by only one column
        # compute this column here
        new_col = zeros(UInt8, rows)
        for i in 1:rows
            # print("\e[2K\e[1G Row: $i Lead col: $key_col")
            v = traces[i,cluster_cols]
            
            new_col[i] = assign_center_noisy(v, centers)
            
        end

        new_indices = setdiff(1:size(traces,2), cluster_cols)
        # keep chronological order
        before = filter(i -> i < init_col, new_indices)
        after = filter(i -> i > init_col, new_indices)

        # update mappings

        push!(new_indices, init_col)
        new_indices = sort(new_indices)
        revert_indices = zeros(Int64,size(traces,2))

        for i in 1:length(new_indices)
            revert_indices[new_indices[i]] = i
        end

        for i in 1:length(mappings)
            ri = revert_indices[mappings[i]]
            if ri == 0
                # println("$i: $(mappings[i])")
                mappings[i] = init_col
            else
                mappings[i] = ri
            end
        end

        traces = hcat(traces[:, before], new_col, traces[:,after ])
    end

    return (traces, mappings)
end

function assign_center_noisy(vec::Array{UInt8,1}, centers::Array{Float64,2})
    dists = map(i-> euclidean(vec, centers[i,:]), 1:size(centers,1))
    return argmin(dists)-1
    # sorted = sortperm(dists)
    # best = sorted[1]
    # second_best = sorted[2]

    # if best != 0 && second_best / best > 1.5
    #     return best-1 #-1 to convert to UInt8
    # else
    #     return nothing
    # end
end

function load_stage2_noisy(traces_path::String, inputs_path::String, output_path::String, stage1data::Stage1DataNoisy, batch_size::Int64 = 100000, traces_count::Int64 = 0)
    
    rev_map = reverse_mapping_from_active_nodes(stage1data.active_nodes)
    m = i -> rev_map[i]
    triplets = m.(stage1data.c.triplets)
    
    
    return (triplets, loading_zip_simplify_by_parts(inputs_path, traces_path, output_path, batch_size, traces_count) do traces::Array{UInt8,2}        
        (traces, _) = reduce_one2ones_noisy(traces, stage1data.b.one2ones, stage1data.b.centers)
        traces[:,stage1data.active_nodes]
    end)
end

function reverse_mapping_from_active_nodes(active_nodes::Array{Int64,1})
    m = maximum(active_nodes)
    rev_map = zeros(Int64, m)

    for i in 1:size(active_nodes,1)
        rev_map[active_nodes[i]] = i
    end

    return rev_map
end

function find_and_replace(traces::Array{UInt8,2}, replace_with::Array{UInt8,1}, rich_state::Array{Bool, 1}, start_by = :start, threshold::Float64 = 0.2)
    (rows, cols) = size(traces)
    mcount = min(rows, 5000)

    if start_by == :start
        @showprogress "Find and replace..." for t in 1:cols
            if fractional_added_information(traces[1:mcount,t], replace_with[1:mcount]) < threshold
                traces[:,t] = replace_with
                rich_state[t] = true
                # println("Found at $t")
                break
            end
        
        end
    else
        @showprogress "Find and replace..." for t in cols:-1:1
            if fractional_added_information(traces[1:mcount,t], replace_with[1:mcount]) < threshold
                traces[:,t] = replace_with
                # println("Found at $t")
                rich_state[t] = true
                break
            end
        end
    end
end

function find_and_replace(traces::Array{UInt8,2}, replace_with::Array{UInt8,2}, rich_state::Array{Bool, 1}, start_by = :start, threshold::Float64 = 0.2)
    (_, replace_count) = size(replace_with)

    for c in 1:replace_count
        find_and_replace(traces, replace_with[:,c], rich_state, start_by, threshold)
    end
end

function enrich_noisy(traces::Array{UInt8, 2}, triplets::Array{Int64,2}, rich_state::Array{Bool,1})
    @assert size(triplets, 2) == 3

    (tcount, tlen) = size(traces)


    #enrich all times
    # @showprogress "Enriching..." 1 
    for t in 1:tlen
        if rich_state[t] == false
            println("Enriching $t/$tlen...")
            enrich_at_noisy(traces, triplets, t)        
            rich_state[t] = true
        end
    end

end

using Distributed
function enrich_at_noisy(traces::Array{UInt8, 2}, triplets::Array{Int64, 2}, t3::Int64)
    (tcount, tlen) = size(traces)
    triplet_count = size(triplets,1)

    before_ent = length(Set(traces[:,t3]))
    if before_ent == 256
        return #nothing to do
    end

    parent_triplet_indices = findall(i->triplets[i,3] == t3, 1:triplet_count)

    for parent_i in parent_triplet_indices
        (t1, t2, t3_2) = triplets[parent_i,1:3]
        @assert t3 == t3_2

        println("Parent triplet: ($t1, $t2) -> $t3")

        children_triplet_indices_left = findall(i-> triplets[i,1] == t3, 1:triplet_count)
        children_triplet_indices_right = findall(i-> triplets[i,2] == t3, 1:triplet_count)

        for child_i in children_triplet_indices_right
            (t4, t3_4, t5) = triplets[child_i,1:3]
            @assert t3 == t3_4
            println("Child triplet: ($t4, $t3) -> $t5")
            if t4 > t3
                println("t4 ($t4) > t3 ($t3). Aborting this second order evaluation.")
                continue
            end
            split_r = split_rows(traces[:,[t1,t2]])
            # return split_r
            f_mat = second_order_matrices(traces[:,[t1,t2,t3,t4,t5]], split_r)
            f_mat = @showprogress "Applying convolution..." pmap(apply_gate_conv, f_mat)#apply a convolution because of undersampling
            clusters::Array{UInt8,2} = clusters_from_observation_noisy(f_mat)
            
            # obs = observation_vectors(traces[:,[t1,t2,t3,t4,t5]])
            # out_sets = sets_from_observation(obs)
            # f = sets2fun(out_sets)
            update_traces!(traces,clusters,t1,t2,t3)
        end

        for child_i in children_triplet_indices_left
            (t3_3, t4, t5) = triplets[child_i,1:3]
            @assert t3 == t3_3
            println("Child triplet: ($t3, $t4) -> $t5")
            if t4 > t3
                println("t4 ($t4) > t3 ($t3). Aborting this second order evaluation.")
                continue
            end
            split_r = split_rows(traces[:,[t1,t2]])
            # return split_r
            f_mat = second_order_matrices(traces[:,[t1,t2,t3,t4,t5]], split_r)
            f_mat = @showprogress "Applying convolution..." pmap(apply_gate_conv, f_mat)#apply a convolution because of undersampling
            clusters::Array{UInt8,2} = clusters_from_observation_noisy(f_mat)
            
            # obs = observation_vectors(traces[:,[t1,t2,t3,t4,t5]])
            # out_sets = sets_from_observation(obs)
            # f = sets2fun(out_sets)
            update_traces!(traces,clusters,t1,t2,t3)
        end
    end

    after_ent = length(Set(traces[:,t3]))
    println("Before: $before_ent, after: $after_ent")
end

function quintuplet(traces::Array{UInt8, 2}, triplets::Array{Int64, 2}, t3::Int64)
    (tcount, tlen) = size(traces)
    triplet_count = size(triplets,1)


    parent_triplet_indices = findall(i->triplets[i,3] == t3, 1:triplet_count)

    for parent_i in parent_triplet_indices
        (t1, t2, t3_2) = triplets[parent_i,1:3]
        @assert t3 == t3_2

        println("Parent triplet: ($t1, $t2) -> $t3")

        children_triplet_indices_left = findall(i-> triplets[i,1] == t3, 1:triplet_count)
        children_triplet_indices_right = findall(i-> triplets[i,2] == t3, 1:triplet_count)

        for child_i in children_triplet_indices_right
            (t4, t3_4, t5) = triplets[child_i,1:3]
            @assert t3 == t3_4
            println("Child triplet: ($t4, $t3) -> $t5")
            
        end

        for child_i in children_triplet_indices_left
            (t3_3, t4, t5) = triplets[child_i,1:3]
            @assert t3 == t3_3
            println("Child triplet: ($t3, $t4) -> $t5")
            
        end
    end
end

# traces[:,[t1,t2]]
function split_rows(traces::Array{UInt8,2})::Array{Array{Int64,1},2}
    s::Array{Array{Int64,1},2} = fill([],256,256)

    @showprogress "Splitting rows..." 1 for i in 1:size(traces,1)
        push!(s[traces[i,1]+1,traces[i,2]+1],i)
    end

    return s
end

#UNSAFE: we do not check for consistency.
#checking consistency is really costly
function clusters_from_observation_noisy(obs::Array{Array{Float16,2}, 2})::Array{UInt8,2}
    cumulative_fun_matrices::Array{Array{Float16,2}, 1} = []
    clusters::Array{UInt16,2} = fill(dontcare, 256, 256) # assign t3 as a function of t1,t2

    #1. evaluate threshold
    
    refi1 = 1
    refi2 = 1

    while sum(obs[refi1, refi2]) < 128.0
        refi1 = rand(1:256)
        refi2 = rand(1:256)
    end
    obsref = obs[refi1, refi2]

    dists_to_ref = @showprogress "Evaluating threshold..." pmap(x-> fun_matrix_distance(x, obsref), obs)

    threshold = estimate_second_order_threshold(dists_to_ref[:])
    println("threshold = $threshold")

    # 2. detect cluster using threshold
    @showprogress "Building value sets for enrichment..." 1 for i1 in 1:256
        for i2 in 1:256
            this_fm = obs[i1,i2] # the function t4 -> t5 given fixed (t1,t2) as a 256*256 matrix

            #compare to all existing cumulative matrices
            found = false
            # for (c, cfm) in enumerate(cumulative_fun_matrices)
            #     if funconv_matrix_distance(this_fm, cfm) < threshold
            #         found = true
            #         cfm += this_fm
            #         clusters[i1,i2] = c-1
            #         break
            #     end
            # end

            #instead of cumulative, we choose representative for speed up
            for (c, cfm) in enumerate(cumulative_fun_matrices)
                d = fun_matrix_distance(cfm, this_fm)
                if d < threshold
                    found = true
                    clusters[i1,i2] = c-1
                    # println("found!")
                    break
                end
            end

            #if new cluster
            if found == false
                c = length(cumulative_fun_matrices)
                push!(cumulative_fun_matrices, this_fm)
                clusters[i1,i2] = c
                # println("$(length(cumulative_fun_matrices)) clusters")

                ol = length(cumulative_fun_matrices)
                if ol > 256
                    throw(ArgumentError("($i1, $i2) not consistent ($ol sets > 256) => observations are not dense enough => more executions are needed."))
                end
            end
            # print("\e[2K\e[1G $i1,$i2 ($(length(cumulative_fun_matrices)))")
        end 
    end

    

    return convert.(UInt8,clusters)
end

function normalize_columns(m::Array{T, 2}) where {T<:AbstractFloat}
    m2 = zeros(T, size(m))
    for c in 1:size(m,2)
        m2[:,c] ./= sum(m[:,c])
    end

    return m2
end

# traces[:,[t1,t2,t3]]
function first_order_distance(traces::Array{UInt8,2}, split_r::Array{Array{Int64,1},2}, v1::UInt8, v2::UInt8)

    ref = mean(traces[split_r[v1+1,v2+1],3])
    m = zeros(256,256)

    for i1 in 1:256
        for i2 in 1:256
            m[i1,i2] = abs(mean(traces[split_r[i1,i2], 3]) - ref)
        end
    end

    return m
end

# traces[:,[t1,t2,t3]]
function first_order_input_matrix(traces::Array{UInt8, 2}, split_r::Array{Array{Int64,1},2}, v1::UInt8, v2::UInt8)
    threshold = 16 # magic value ! depends on sigma, the gaussian noise std

    dists = first_order_input_matrix(traces, split_r, v1, v2)
    return dists .< 16
end


# traces[:,[t1,t2,t3,t4,t5]]
function second_order_distance(traces::Array{UInt8,2}, split_r::Array{Array{Int64,1},2}, v1::UInt8, v2::UInt8)
    ref_rows = split_r[v1+1,v2+1]
    ref_m = second_order_matrix(traces[ref_rows,4], traces[ref_rows, 5])
    dists = zeros(256,256)

    @showprogress "Second order distance..." 1 for i1 in 1:256
        for i2 in 1:256
            rows = split_r[i1,i2]
            m = second_order_matrix(traces[rows, 4], traces[rows, 5])
            dists[i1,i2] = fun_matrix_distance(ref_m, m)
        end
    end

    return dists
end


csize = 5 # magic value ! depends on sigma, the gaussian noise std
gate = zeros(Float64, csize)
gate[1:csize] .= 1/csize

function apply_gate_conv(col::Array{T,1}) where {T<:AbstractFloat}
    if sum(col) != 0
        col2 = convert.(Float64, col)
        return conv(col2, gate)
    else
        return zeros(T,256 + csize - 1)
    end
end

function apply_gate_conv(mi::Array{T,2}) where {T<:AbstractFloat}
    m = zeros(T, size(mi,1)+ csize - 1, size(mi,2))

    for c in 1:size(mi, 2)
        m[:,c] = apply_gate_conv(mi[:,c])
    end

    return m
end

# traces[:,[t1,t2,t3,t4,t5]]
function second_order_distance_conv(traces::Array{UInt8,2}, split_r::Array{Array{Int64,1},2}, v1::UInt8, v2::UInt8)
    ref_rows = split_r[v1+1,v2+1]
    ref_m = second_order_matrix(traces[ref_rows,4], traces[ref_rows, 5])
    dists = zeros(256,256)

    ref_m_conv = apply_gate_conv(ref_m)

    @showprogress "Second order distance..." 1 for i1 in 1:256
        for i2 in 1:256
            rows = split_r[i1,i2]
            m = second_order_matrix(traces[rows, 4], traces[rows, 5])
            dists[i1,i2] = funconv2_matrix_distance(ref_m_conv, m)
        end
    end

    return dists
end

# # traces[:,[t1,t2,t3,t4,t5]]
# # traces rows for the same pair t1,t2
# function second_order_matrix(traces::Array{UInt8,2}, v1::UInt8, v2::UInt8)

#     println("Mean is $(mean(traces[:,3]))")

#     m = fun_matrix_from_samples(traces[same_val,4], traces[same_val,5])

#     return m
# end

# traces[:,[t1,t2,t3,t4,t5]]
function second_order_matrices(traces::Array{UInt8,2}, split_r::Array{Array{Int64,1},2} = nothing)::Array{Array{Float16,2}, 2}

    if split_r === nothing
        split_r = split_rows(traces[:,[1,2]])
    end

    mat::Array{Array{Float16,2}, 2} = fill(zeros(Float16, 256,256), 256,256)
    @showprogress "Second order matrices..." 1 for i1 in 1:256
        for i2 in 1:256
            rows = split_r[i1,i2]
            mat[i1,i2] = second_order_matrix(traces[rows,4], traces[rows,5])
        end
    end

    return mat
end

function second_order_matrix(input::Array{UInt8,1}, output::Array{UInt8,1})::Array{Float32,2}

    l = length(input)
    @assert(l == length(output))# both size must be equal
    m = zeros(Float32, 256,256)

    for i in 1:l
        m[output[i]+1, input[i]+1] += 1
    end

    # normalize colums

    for c in 1:256
        s = sum(m[:,c])
        if s != 0.0
            m[:,c] /= s
        end
    end

    return m
end

using Distances
using DSP
function funconv_matrix_distance(fm1::Array{T,2},fm2::Array{T,2}) where {T<:AbstractFloat}

    d = 0
    for c in 1:256
        d += sqeuclidean(apply_gate_conv(fm1[:,c]),apply_gate_conv(fm2[:,c]))
    end

    return sqrt(d)
end

function funconv2_matrix_distance(fm1conv::Array{T,2},fm2::Array{T,2}) where {T<:AbstractFloat}

    d = 0
    for c in 1:256
        d += sqeuclidean(fm1conv[:,c],apply_gate_conv(fm2[:,c]))
    end

    return sqrt(d)
end

using Distributed
function fun_matrix_distance(fm1::Array{T,2},fm2::Array{T,2}) where {T<:AbstractFloat}
   

    
    # d = pmap(c -> sqeuclidean(fm1[:,c], fm2[:,c]), 1:256)
    # return sqrt(sum(d))

    d = 0
    for c in 1:256
        d += sqeuclidean(fm1[:,c],fm2[:,c])
    end

    return sqrt(d)
end

using StatsBase
function estimate_second_order_threshold(distances)::Float64
    h = fit(Histogram, distances, nbins = 100)
    hw = h.weights
    hw[1] -= 1 # remove self distance equal to 0

    first = false
    first_ended = false
    end_of_first = 0
    start_of_second = 0

    for i in 1:size(hw,1)
        if hw[i] != 0
            if first == false
                first = true
                # println("First !")
            elseif first_ended == true
                start_of_second = i
                # println("Start of second = $i")
                middle_index =  round(Int64,(end_of_first + start_of_second)/2)

                if middle_index + 1 < start_of_second # hack against noise
                    middle_index += 1
                else
                    println("threshold is probably incorrect: $end_of_first < $middle_index < $start_of_second")
                end
                return collect(h.edges[1])[middle_index]
            end
        else
            if first == true && first_ended == false
                end_of_first = i
                first_ended = true
                # println("End of first = $i")
            end
                
        end
    end

    return 0.0
end