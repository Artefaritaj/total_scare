# A Twine cipher implementation

stab_twine=convert.(UInt8, [0xC, 0x0, 0xF, 0xA, 0x2, 0xB, 0x9, 0x5, 0x8, 0x3, 0xD, 0x7, 0x1, 0xE, 0x6, 0x4])
ptab_twine=convert.(Int64, [5,0,1,4,7,12,3,8,13,6,9,2,15,10,11,14])
ptabinv_twine=convert.(Int64, [1,2,11,6,3,0,9,4,7,10,13,14,5,8,15,12])
contab_twine = convert.(UInt8,[0x1,0x2,0x4,0x8,0x10,0x20,0x3,0x6,0xC,0x18,0x30,0x23,0x5,0xA,0x14,0x28,0x13,0x26,0xF,0x1E,0x3C,0x3B,0x35,0x29,0x11,0x22,0x7,0xE,0x1C,0x38,0x33,0x25,0x9,0x12,0x24,0xB])

# state is composed of 16 4-bit nibbles
function bytes2nibbles(bytes::Array{UInt8, 1})::Array{UInt8,1}
    bytes_len = length(bytes)

    nibbles = zeros(UInt8, bytes_len*2)

    for i in 1:bytes_len
        nibbles[2*i-1]    = (bytes[i] >> 4) & 0xF 
        nibbles[2*i]      = (bytes[i]) & 0xF
    end

    return nibbles
end

function nibbles2bytes(nibbles::Array{UInt8, 1})::Array{UInt8,1}
    bytes_len::Int64 = length(nibbles)/2
    bytes = zeros(UInt8,bytes_len)

    for i in 1:bytes_len
        bytes[i] = nibbles[2*i] | (nibbles[2*i-1] << 4)
    end

    return bytes
end

function S(nibble::UInt8)::UInt8
	return stab_twine[nibble+1]#index starts at 1!!!
end

function P(nibble::Int64)::Int64
    return ptab_twine[nibble+1]
end

function Pinv(nibble::Int64)::Int64
    return ptabinv_twine[nibble+1]
end

function F(state_nibble::UInt8, round_key_nibble::UInt8)::UInt8
    inter_nibble = xor(state_nibble, round_key_nibble)
    return S(inter_nibble)
end

function round_nibbles(state_nibbles::Array{UInt8,1}, rk_nibbles::Array{UInt8,1})::Array{UInt8,1}
    for d in 1:16
        print("0x$(string(state_nibbles[d], base=16)) ")
    end

    println()

    for j in 1:8
        state_nibbles[2*j] = xor(F(state_nibbles[2*j-1], rk_nibbles[j]), state_nibbles[2*j])
    end

    state_out_nibbles = zeros(UInt8, 16)
    for h in 0:15
        state_out_nibbles[P(h)+1] = state_nibbles[h+1]
    end

    return state_out_nibbles
end

function last_round_nibbles(state_nibbles::Array{UInt8,1}, rk_nibbles::Array{UInt8,1})::Array{UInt8,1}
    for d in 1:16
        print("0x$(string(state_nibbles[d], base=16)) ")
    end
    println()
    for j in 1:8
        state_nibbles[2*j] = xor(F(state_nibbles[2*j-1], rk_nibbles[j]), state_nibbles[2*j])
    end
    for d in 1:16
        print("0x$(string(state_nibbles[d], base=16)) ")
    end
    println()

    return state_nibbles
end

function CON(i::Int64)::UInt8
    return contab_twine[i]
end

function key_gen80(key_bytes::Array{UInt8, 1})::Array{UInt8,2}
    @assert(length(key_bytes) == 10)
    all_rk_nibbles::Array{UInt8, 2} = zeros(UInt8, 36, 8)
    master_key_nibbles = bytes2nibbles(key_bytes)
    (wk_nibbles, rk_nibbles) = init_key_round80(master_key_nibbles)

    print("WK round 1: ")
    for d in 1:20
        print("0x$(string(wk_nibbles[d], base=16)) ")
    end
    println()
    all_rk_nibbles[1,:] = rk_nibbles

    for round in 2:36
        (wk_nibbles, rk_nibbles) = key_round80(wk_nibbles, round)
        print("WK round $round: ")
        for d in 1:20
            print("0x$(string(wk_nibbles[d], base=16)) ")
        end
        println()
        all_rk_nibbles[round,:] = rk_nibbles
    end

    return all_rk_nibbles
end

rk_select = [2,4,5,7,14,15,16,17]
function key_round80(work_key_nibbles::Array{UInt8,1}, round::Int64)
    work_key_nibbles[2] = xor(work_key_nibbles[2], S(work_key_nibbles[1]))
    work_key_nibbles[5] = xor(work_key_nibbles[5], S(work_key_nibbles[17]))
    work_key_nibbles[8] = xor(work_key_nibbles[8], (CON(round-1)>>3) & 0x7) 
    work_key_nibbles[20] = xor(work_key_nibbles[20], CON(round-1) & 0x7)   

    t = work_key_nibbles[1:4]

    for j in 0:3
        for off in 1:4
            work_key_nibbles[j*4+off] = work_key_nibbles[j*4+off+4]
        end
    end

    work_key_nibbles[17] = t[2]
    work_key_nibbles[18] = t[3]
    work_key_nibbles[19] = t[4]
    work_key_nibbles[20] = t[1]

    rk_nibbles::Array{UInt8, 1} = zeros(8)
    rk_nibbles = work_key_nibbles[rk_select]

    return (work_key_nibbles, rk_nibbles)
end

function init_key_round80(master_key_nibbles::Array{UInt8,1})
    rk_nibbles::Array{UInt8,1} = zeros(8)
    rk_nibbles = master_key_nibbles[rk_select]

    return (master_key_nibbles, rk_nibbles)
end

function Twine(plaintext_bytes::Array{UInt8, 1}, key_bytes::Array{UInt8, 1})::Array{UInt8, 1}
    @assert(length(key_bytes) == 10)
    @assert(length(plaintext_bytes) == 8)
    all_keys = key_gen80(key_bytes)

    state::Array{UInt8,1} = bytes2nibbles(plaintext_bytes)

    for i in 1:35
        state = round_nibbles(state, all_keys[i,:])
    end

    state = last_round_nibbles(state, all_keys[36,:])

    return nibbles2bytes(state)
end

pt_test = convert.(UInt8, [0x01, 0x23, 0x45, 0x67, 0x89, 0xAB, 0xCD, 0xEF])
key_test = convert.(UInt8, [0x00, 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88, 0x99])