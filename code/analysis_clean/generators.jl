using SpecialFunctions
sqrt2 = sqrt(2)
RandGaussianNoise(sigma) = sigma * sqrt2 * erfinv(2.0*rand()-1.0)

HWNoise(x, sigma) = RandGaussianNoise(sigma) + count_ones(x)
Noise(x, sigma) = RandGaussianNoise(sigma) + x

function sample(val::AbstractFloat, min_max::Tuple{AbstractFloat, AbstractFloat})::UInt8
    (min_val, max_val) = min_max
    

    if val <= min_val
        return 0x0
    elseif val >= max_val
        return 0xFF
    else
        span = max_val-min_val

        return round(UInt8, (val - min_val)*255.0 / span)
    end
end

function sample_traces(traces::Array{Float16, 2}, min_max::Tuple{AbstractFloat, AbstractFloat} = (-Inf, Inf))::Array{UInt8,2}
    tend = min(size(traces,2), 50000)

    (min_val, max_val) = min_max

    if min_val == -Inf
        min_val = minimum(traces[1:tend,:])
    end

    if max_val == Inf
        max_val = maximum(traces[1:tend,:])
    end

    (tcount, tlen) = size(traces)

    
    # @showprogress 1 "Sampling... " 
    for exe in 1:tcount
        for t in 1:tlen
            traces[exe,t] = sample(traces[exe,t], (min_val, max_val))
        end
        # print("\e[2K\e[1G$exe/$tcount")
    end
    # print("\e[2K\e[1G")

    return convert.(UInt8, traces)
end

function measure_min_max(traces::Array{UInt8, 2}, sigma::Float64)
    (tcount, tlen ) = size(traces)
    noisy_traces::Array{Float16, 2} = zeros(Float16, tcount, tlen)

    @showprogress 1 "Applying noise... " for i in 1:tcount
        noisy_traces[i,:] = convert.(Float16, Noise.(traces[i,:], sigma))
        # c += 1
        # print("\e[2K\e[1G$c/$tcount")
    end

    min_val = minimum(noisy_traces)
    max_val = maximum(noisy_traces)

    return (min_val, max_val)
end

# using Base.Threads
function apply_noise(traces::Array{UInt8, 2}, sigma::Float64, min_max::Tuple{AbstractFloat, AbstractFloat} = (-Inf, Inf))::Array{Float16, 2}
    (tcount, tlen ) = size(traces)
    noisy_traces::Array{Float16, 2} = zeros(Float16, tcount, tlen)

    # c = 0
    # @showprogress 1 "Applying noise... " 
    for i in 1:tcount
        noisy_traces[i,:] = convert.(Float16, Noise.(traces[i,:], sigma))
        # c += 1
        # print("\e[2K\e[1G$c/$tcount")
    end
    # print("\e[2K\e[1G")
    noisy_traces = sample_traces(noisy_traces, min_max)

    return noisy_traces
end

function apply_noise!(traces::Array{UInt8, 2}, sigma::Float64, min_max::Tuple{AbstractFloat, AbstractFloat} = (-Inf, Inf))
    (tcount, tlen ) = size(traces)
    batch_size = 100000
    batch_count = round(Int64, tcount / batch_size)

    for b in 1:batch_count
        bstart::Int64 = (b-1)*batch_size + 1
        bend::Int64 = b*batch_size
        # println("$bstart:$bend")

        traces[bstart:bend,:] = apply_noise(traces[bstart:bend,:], sigma, min_max)
    end

end

function generate_compress(input_traces_path::String, output_traces_path::String, batch_size::Int64 = 10000)
    transform_write_traces(input_traces_path, output_traces_path, batch_size) do traces::Array{UInt8,2}
        a = remove_constants(traces)
        b = remove_same(a)
        b
    end
end

function generate_hw(input_traces_path::String, output_traces_path::String, batch_size::Int64 = 100000)
    transform_write_traces(input_traces_path, output_traces_path, batch_size) do traces::Array{UInt8,2}
        convert.(UInt8, count_ones.(remove_same(remove_constants(traces))))
    end
end

function reverse_order!(traces::Array{UInt8, 2})
    (tcount, tlen) = size(traces)
    swaplen = floor(Int64, tlen/2)

    @showprogress for i in 1:swaplen
        tmpcol = copy(traces[:,i])
        traces[:,i] = traces[:,tlen-i+1]
        traces[:,tlen-i+1] = tmpcol
    end
end

function generate_reverse(input_traces_path::String, output_traces_path::String, batch_size::Int64 = 100000)
    transform_write_traces(input_traces_path, output_traces_path, batch_size) do traces::Array{UInt8,2}
        traces = remove_same(remove_constants(traces))
        reverse_order!(traces)
        traces
    end
end

function generate_hw_noisy(input_traces_path::String, output_traces_path::String, sigma::Float64 = 0.1, batch_size::Int64 = 100000)

    tdata = load_traces(input_traces_path, 0, batch_size)
    tdata = convert.(UInt8, count_ones.(remove_constants(tdata)))
    min_max = measure_min_max(tdata, sigma)

    println(min_max)

    tdata = 0
    transform_write_traces(input_traces_path, output_traces_path, batch_size) do traces::Array{UInt8,2}
        traces = convert.(UInt8, count_ones.(remove_constants(traces)))
        apply_noise!(traces, sigma, min_max)
        return traces
    end
end

function generate_rand_probability_vector(zero_bins::Int64 = 0)::Array{Float64,1}
    r = rand(Float64, 256)

    r[end-zero_bins+1:end] .= 0

    r = r./sum(r)

    return r
end

function generate_uniform_probability_vector(zero_bins::Int64)::Array{Float64, 1}
    r = ones(Float64, 256)
    r[end-zero_bins+1:end] .= 0

    r = r./sum(r)

    return r
end

function samples_from_probability_vector(probability_vector::Array{Float64,1}, number_of_samples::Int64, dims::Int64)::Array{UInt8,2}
    a = zeros(UInt8, number_of_samples, dims)

    for s in 1:number_of_samples
        for d in 1:dims
            r = rand()
            acc = 0
            for i in 1:256
                acc += probability_vector[i]
                if acc >= r
                    a[s,d] = i-1
                    break
                end
            end
        end
    end

    return a
end

function evaluate_se()
    m = 200000
    step = 10000
    
    xs = collect(step:step:m)
    series = collect(1:50:251)

    se = zeros(Float64,length(xs), length(series))
    dims = 3

    
    
    @showprogress "Sampling..." 1 for bi in 1:length(series)
        b = series[bi]
        p = generate_rand_probability_vector(b)
    
        for si in 1:length(xs)
            scount = xs[si]
            se[si,bi] = shannon_entropy(samples_from_probability_vector(p, scount,dims))
        end
    end

    scatter(
        step:step:m,
        se,
        xlim = (0, m),
        xticks = 0:(m/5):m,
        ylim = (0, dims*8),
        yticks = 0:dims:dims*8,
        xscale = :log10,
        yscale = :log10,
    )


end

function generate_entropy_estimator_table(number_of_samples::Int64, dims::Int64, iterations::Int64 = 5)::Array{Float64,2}

    tab = zeros(Float64, 256, 2)
    @showprogress "Generating estimator..." 1 for b in 0:255
        p = generate_uniform_probability_vector(b)
        se = maximum(map(i-> shannon_entropy(samples_from_probability_vector(p, number_of_samples, dims), false), 1:iterations))
        tab[256-b,1] = se
        tab[256-b,2] = se_from_probability_vector(p)*dims
    end

    return tab
end