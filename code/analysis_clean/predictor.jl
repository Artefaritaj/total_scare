struct Predictor
    #input2dual
    input_dual_time::Array{Int64}  # input_dual_time[1] = 5 means that first byte of input is at time 5 in dual_traces
    input2dual_fun::Dict{Int64, Dict{UInt8,UInt8}} #input2dual_fun[byte] = f (byte) -> byte
    #dual2output
    output_dual_time::Array{Int64}  # output_dual_time[1] = 5 means that first byte of output is at time 5 in dual_traces
    dual2output_fun::Dict{Int64, Dict{UInt8,UInt8}} #output2dual_fun[byte] = f (byte) -> byte

    #inside dual_traces
    dual_traces_len::Int64
    relations2to1::Dict{Int64, Tuple{Int64, Int64}}# t3 => (t1, t2) means that t3 = t1 op t2. op is given in dual_fun3
    dual_fun3::Dict{Int64, Dict{Tuple{UInt8, UInt8},UInt8}} #for each t3 give function (t1, t2) => t3

    function Predictor(dual_traces::Array{UInt8,2}, inputs::Array{UInt8, 2}, outputs::Array{UInt8, 2}, triplets::Array{Int64,2}, noisy::Bool = false)
        (traces_count, dual_traces_len) = size(dual_traces)
        @assert traces_count == size(inputs,1)
        @assert traces_count == size(outputs,1)
        @assert size(triplets,2) == 3

        mcount = min(traces_count, 5000)
        
        #input2dual
        input_byte_count = size(inputs,2)
        idt = zeros(Int64, input_byte_count)
        i2df::Dict{Int64, Dict{UInt8,UInt8}} = Dict()

        # println("Detecting timing relation between input bytes and dual traces...")
        found = 0

        #detect time relations
        @showprogress "Detecting inputs in traces..." for b in 1:input_byte_count
            #because of dual values simplification, byte b can be linked to only one time in dual_traces
            for t in 1:dual_traces_len
                # if shannon_entropy2(hcat(inputs[1:mcount,b], dual_traces[1:mcount,t])) <= 8.0
                if fractional_added_information(dual_traces[1:mcount,t], inputs[1:mcount,b]) < 0.1
                    #link detected
                    # println("$b -> $t")
                    idt[b] = t
                    found += 1
                    break
                end
            end
            # print("\e[2K\e[1G $b/$input_byte_count ($found found)")
        end
        # println("\n$found/$input_byte_count timing relation found.")

        # println("Extracting relations between input bytes and dual traces...")

        #extract functions input2dual
        if noisy == false
            @showprogress "Building input functions..." for b in 1:input_byte_count
                if idt[b] > 0 #0 is no link detected
                    fun2 = extract_fun2(hcat(inputs[:,b], dual_traces[:,idt[b]]))
                    push!(i2df, b => fun2)
                end
                # print("\e[2K\e[1G $b/$input_byte_count")
            end
        else
            @showprogress "Building input functions..." for b in 1:input_byte_count
                if idt[b] > 0 #0 is no link detected
                    fun2 = extract_fun2_noisy(hcat(inputs[:,b], dual_traces[:,idt[b]]))
                    push!(i2df, b => fun2)
                end
                # print("\e[2K\e[1G $b/$input_byte_count")
            end
        end


        #extract 2 to 1 function inside dual_traces
        # println("\nExtracting relations inside dual traces from triplets...")

        df3::Dict{Int64, Dict{Tuple{UInt8, UInt8},UInt8}} = Dict()
        relations_count = size(triplets,1)
        if noisy == false
            @showprogress "Building dual functions..." for r in 1:relations_count
                # println("New triplet: ", triplets[r,:])
                fun3 = extract_fun3(dual_traces[:,triplets[r,:]])
                push!(df3, triplets[r,3] => fun3)
                # print("\e[2K\e[1G $r/$relations_count")
            end
        else
            @showprogress "Building dual functions..." for r in 1:relations_count
                # println("New triplet: ", triplets[r,:])
                fun3 = extract_fun3_noisy(dual_traces[:,triplets[r,:]])
                push!(df3, triplets[r,3] => fun3)
                # print("\e[2K\e[1G $r/$relations_count")
            end
        end

        rel2to1::Dict{Int64, Tuple{Int64, Int64}} = Dict()
        for r in 1:size(triplets,1)
            push!(rel2to1, triplets[r,3] => (triplets[r,1], triplets[r,2]))
        end

        #dual2output
        output_byte_count = size(outputs,2)
        odt = zeros(Int64, output_byte_count)
        o2df::Dict{Int64, Dict{UInt8,UInt8}} = Dict()

        # println("\nDetecting timing relation between dual traces and output bytes...")
        found = 0

        #detect time relations
        @showprogress "Detecting outputs in traces..." for b in 1:output_byte_count
            #because of dual values simplification, byte b can be linked to only one time in dual_traces
            for t in dual_traces_len:-1:1#iterating in reverse is more efficient
                # if shannon_entropy2(hcat(outputs[1:mcount,b], dual_traces[1:mcount,t])) <= 8.0
                if fractional_added_information(dual_traces[1:mcount,t], outputs[1:mcount,b]) < 0.1
                    #link detected
                    # println("$b -> $t")
                    odt[b] = t
                    found += 1
                    break
                end
            end
            # print("\e[2K\e[1G $b/$output_byte_count ($found found)")
        end
        # println("\n$found/$output_byte_count timing relation found.")

        # println("Extracting relations between dual traces and output bytes...")

        #extract functions dual2output
        if noisy == false
            @showprogress "Building output functions..." for b in 1:output_byte_count
                if odt[b] > 0 #0 is no link detected
                    fun2 = extract_fun2(hcat(dual_traces[:,odt[b]], outputs[:,b]))
                    push!(o2df, b => fun2)
                end
                # print("\e[2K\e[1G $b/$input_byte_count")
            end
        else
            @showprogress "Building output functions..." for b in 1:output_byte_count
                if odt[b] > 0 #0 is no link detected
                    fun2 = extract_fun2_noisy(hcat(dual_traces[:,odt[b]], outputs[:,b]))
                    push!(o2df, b => fun2)
                end
                # print("\e[2K\e[1G $b/$input_byte_count")
            end
        end

        # println()
        new(idt, i2df, odt, o2df, dual_traces_len, rel2to1, df3)
    end
end

#compute output from input and predictor
function predict(pr::Predictor, input::Array{UInt8,1})#::Array{UInt8,1}
    dual_trace::Array{UInt8,1} = zeros(UInt8, pr.dual_traces_len)
    times_set::Array{Bool, 1} = zeros(Bool, pr.dual_traces_len)

    #convert input to dual values
    for b in 1:size(input,1)
        tinput = pr.input_dual_time[b]
        times_set[tinput] = true
        dual_trace[tinput] = pr.input2dual_fun[b][input[b]]
    end

    #propagate dual values
    
    for t3 in 1:pr.dual_traces_len
        if times_set[t3] == false
            (t1, t2) = pr.relations2to1[t3]
            f = pr.dual_fun3[t3]

            if times_set[t1] == false#should never happens
                println("Error: $t1 is not set before computing $t1")
            end

            if times_set[t2] == false#should never happens
                println("Error: $t2 is not set before computing $t3")
            end

            dual_trace[t3] = f[(dual_trace[t1], dual_trace[t2])]
            times_set[t3] = true
        else
            # println("$t3 already set before")
        end
        # print("\e[2K\e[1G $t3/$(pr.dual_traces_len)")
    end
    println()

    #convert dv to output
    res = zeros(UInt8, size(pr.output_dual_time,1))
    for b in 1:size(res,1)
        toutput = pr.output_dual_time[b]

        if times_set[toutput] == false
            println("Error: $toutput is not set.")
        end
        res[b] = pr.dual2output_fun[b][dual_trace[toutput]]
    end

    res
end