
dontcare = 0xFFFF

# traces[:,[t1, t2, t3, t4, t5]]
function observation_vectors(traces::Array{UInt8,2})
    @assert size(traces, 2) == 5
    tcount = size(traces,1)

    obs::Dict{Tuple{UInt8, UInt8},Array{UInt16,1}} = Dict()
    t4vals = Set(traces[:,4])
    t4vals_count = length(t4vals)

    #init by allocating new arrays
    for t1 in 0:255
        for t2 in 0:255
            obs[(t1,t2)] = fill(dontcare,1+t4vals_count)#dontcare by default
        end
    end

    #first order observation
    for i in 1:tcount
        obs[(traces[i,1],traces[i,2])][1] = traces[i,3]
    end

    #second order observation
    for (bi, b) in enumerate(t4vals)
        iset = findall(i->traces[i,4] == b, 1:tcount)

        for i in iset
            obs[(traces[i,1], traces[i,2])][bi+1] = traces[i,5]
        end
        print("\e[2K\e[1G$bi/$t4vals_count")
    end
    print("\e[2K\e[1G")
    obs
end

# traces[:,[t1, t2, t3, t4 (l), t5 (r), t6]]
function observation_vectors16(traces::Array{UInt8,2})
    @assert size(traces, 2) == 6
    tcount = size(traces,1)

    obs::Dict{Tuple{UInt8, UInt8},Array{UInt16,1}} = Dict()
    t45vals = Set(eachrow(traces[:,[4,5]]))
    t45vals_count = length(t45vals)

    #init by allocating new arrays
    for t1 in 0:255
        for t2 in 0:255
            obs[(t1,t2)] = fill(dontcare,1+t45vals_count)#dontcare by default
        end
    end

    #first order observation
    for i in 1:tcount
        obs[(traces[i,1],traces[i,2])][1] = traces[i,3]
    end

    #second order observation
    for (bi, b) in enumerate(t45vals)
        iset = findall(i->traces[i,[4,5]] == b, 1:tcount)

        for i in iset
            obs[(traces[i,1], traces[i,2])][bi+1] = traces[i,6]
        end
        print("\e[2K\e[1G$bi/$t45vals_count")
    end
    print("\e[2K\e[1G")
    obs
end

# traces[:,[t1, t2, t3, t4, t5]]
function observation_set_vectors(traces::Array{UInt8,2})
    @assert size(traces, 2) == 5
    tcount = size(traces,1)

    obs::Dict{Tuple{UInt8, UInt8},Array{Set{UInt8},1}} = Dict()
    t4vals = Set(traces[:,4])
    t4vals_count = length(t4vals)

    #init by allocating new arrays
    for t1 in 0:255
        for t2 in 0:255
            obs[(t1,t2)] = fill(Set(),1+t4vals_count)#dontcare by default
        end
    end

    #first order observation
    for i in 1:tcount
        obs[(traces[i,1],traces[i,2])][1] = Set([traces[i,3]])
    end

    #second order observation
    for (bi, b) in enumerate(t4vals)
        iset = findall(i->traces[i,4] == b, 1:tcount)

        for i in iset
            pair = (traces[i,1], traces[i,2])
            prev_set_array = obs[pair]
            # obs[(traces[i,1], traces[i,2])][bi+1] = traces[i,5]
            push!(prev_set_array[bi+1], traces[i,5])
            obs[pair] = prev_set_array
        end
        print("\e[2K\e[1G$bi/$t4vals_count")
    end
    print("\e[2K\e[1G")
    obs
end



function find_io_indices(traces::Array{UInt8, 2}, pts::Array{UInt8,2}, cts::Array{UInt8,2})
    io_indices::Array{Int64, 1} = zeros(size(pts, 2) + size(cts,2))
    (tcount, tlen) = size(traces)

    mcount = min(tcount, 5000)
    io_ind = 1

    #pts
    for i in 1:size(pts,2)
        for t in 1:tlen
            # if shannon_entropy(hcat(pts[1:mcount,i], traces[1:mcount,t])) <= 8.0
            if fractional_added_information(traces[1:mcount,t], pts[1:mcount,i]) < 0.1
                io_indices[io_ind] = t
                break
            end
        end
        io_ind += 1
        print("\e[2K\e[1G $io_ind/$(size(io_indices,1))")
    end

    #cts
    for i in 1:size(cts,2)
        for t in tlen:-1:1
            # if shannon_entropy(hcat(cts[1:mcount,i], traces[1:mcount,t])) <= 8.0
            if fractional_added_information(traces[1:mcount,t], cts[1:mcount,i]) < 0.1
                io_indices[io_ind] = t
                break
            end
        end
        io_ind += 1
        print("\e[2K\e[1G $io_ind/$(size(io_indices,1))")
    end
    print("\e[2K\e[1G")

    io_indices
end

function enrich(traces::Array{UInt8, 2}, triplets::Array{Int64,2}, io_indices::Array{Int64,1})
    @assert size(triplets, 2) == 3

    (tcount, tlen) = size(traces)

    #track what times (indices) have been enriched
    enriched::Array{Bool, 1} = fill(false, tlen)
    for t in io_indices
        enriched[t] = true
    end

    #enrich all times
    # @showprogress "Enriching..." 1 
    for t in 1:tlen
        if enriched[t] == false
            l = length(Set(traces[:,t]))
            
            if l != 256
                println("Enriching $t/$tlen...")
                enrich_at(traces, triplets, t)
            else
                println("$t seems already rich")
            end            
            enriched[t] = true
        end
    end

end

#complete traces as input
function enrich_at(traces::Array{UInt8, 2}, triplets::Array{Int64, 2}, t3::Int64)
    # @assert size(traces, 2) == 3

    (tcount, tlen) = size(traces)
    triplet_count = size(triplets,1)

    before_ent = length(Set(traces[:,t3]))
    if before_ent == 256
        return #nothing to do
    end

    parent_triplet_indices = findall(i->triplets[i,3] == t3, 1:triplet_count)

    for parent_i in parent_triplet_indices
        (t1, t2, t3_2) = triplets[parent_i,1:3]
        @assert t3 == t3_2

        # println("Parent triplet: ($t1, $t2) -> $t3")

        children_triplet_indices_left = findall(i-> triplets[i,1] == t3, 1:triplet_count)
        children_triplet_indices_right = findall(i-> triplets[i,2] == t3, 1:triplet_count)

        for child_i in children_triplet_indices_right
            (t4, t3_4, t5) = triplets[child_i,1:3]
            @assert t3 == t3_4
            # println("Child triplet: ($t4, $t3) -> $t5")
            obs = observation_vectors(traces[:,[t1,t2,t3,t4,t5]])
            out_sets = sets_from_observation(obs)
            f = sets2fun(out_sets)
            update_traces!(traces,f,t1,t2,t3)
        end

        for child_i in children_triplet_indices_left
            (t3_3, t4, t5) = triplets[child_i,1:3]
            @assert t3 == t3_3
            # println("Child triplet: ($t3, $t4) -> $t5")
            obs = observation_vectors(traces[:,[t1,t2,t3,t4,t5]])
            out_sets = sets_from_observation(obs)
            f = sets2fun(out_sets)
            update_traces!(traces,f,t1,t2,t3)
        end
    end

    after_ent = length(Set(traces[:,t3]))
    println("Before: $before_ent, after: $after_ent")
end

# function sets_from_observation(obs::Dict{Tuple{UInt8, UInt8},Array{UInt16,1}})::Set{Set{Tuple{UInt8,UInt8}}}
#     out_sets::Set{Set{Tuple{UInt8,UInt8}}} = Set()

#     for v1 in 0:255
#         for v2 in 0:255
#             #observation for this pair of inputs
#             ob = obs[(v1,v2)]

#             if !is_obs_empty(ob)
#                 #try to include this pair into one of the existing set
#                 compatible_sets = 0
#                 for out_set in out_sets
#                     compat_set = true
#                     for (v3, v4) in out_set
#                         if !are_obs_compatible(obs[(v3,v4)], ob)
#                             compat_set = false
#                         end
#                     end


#                     if compat_set == true
#                         push!(out_set, (v1, v2))
#                         compatible_sets += 1
#                     end
#                 end

#                 #no possible set
#                 if compatible_sets == 0
#                     s = Set([(v1,v2)])
#                     push!(out_sets, s)
#                 elseif compatible_sets > 1
#                     println("($v1,$v2) are compatible with $compatible_sets sets")
#                 end
#             end

#             print("\e[2K\e[1G $v1/255 - $v2/255 ($compatible_sets) => ($(length(out_sets)) sets)")
#         end
#     end

#     out_sets
# end

#UNSAFE: we do not check for consistency. Ok if more than ~6000000 executions => obs are dense enough (value obtained from trial and errors).
#checking consistency is really costly
function sets_from_observation(obs::Dict{Tuple{UInt8, UInt8},Array{UInt16,1}})::Set{Set{Tuple{UInt8,UInt8}}}
    out_sets::Set{Set{Tuple{UInt8,UInt8}}} = Set()

    c = 0
    tot = length(obs)
    for (pair, ob) in obs
        compatible_sets = 0
        for existing_set in out_sets
            if are_obs_compatible(ob, obs[first(existing_set)])
                push!(existing_set, pair)
                compatible_sets += 1
            end
        end

        if compatible_sets == 0
            s = Set([pair])
            push!(out_sets, s)
        # elseif compatible_sets > 1
        #     println("More than 1 compatible set for $pair.")
        # else
        #     println("$pair is compatible with $compatible_sets sets")
        end

        c += 1
        print("\e[2K\e[1G $c/$tot ($(length(out_sets)))")
    end


    ol = length(out_sets)
    print("\e[2K\e[1G")
    if ol > 256
        throw(ArgumentError("Not consistent ($ol sets) => observations are not dense enough => more executions are needed."))
    end
    

    out_sets
end

function sets2fun(out_sets::Set{Set{Tuple{UInt8,UInt8}}})::Dict{Tuple{UInt8, UInt8},UInt8}
    val = 0
    d::Dict{Tuple{UInt8, UInt8},UInt8} = Dict()

    # println("#sets = $(length(out_sets))")
    for s in out_sets
        for pair in s
            d[pair] = val
        end

        val += 1
    end
    d
end

function update_traces!(traces::Array{UInt8,2}, fun::Dict{Tuple{UInt8, UInt8}, UInt8}, t1::Int64, t2::Int64, t3::Int64)
    (tcount, tlen) = size(traces)

    for exe in 1:tcount
        traces[exe,t3] = fun[(traces[exe,t1], traces[exe,t2])]
    end
end

function update_traces!(traces::Array{UInt8,2}, fun::Array{UInt8,2}, t1::Int64, t2::Int64, t3::Int64)
    (tcount, tlen) = size(traces)

    for exe in 1:tcount
        traces[exe,t3] = fun[traces[exe,t1]+1, traces[exe,t2]+1]
    end
end

function are_obs_compatible(obs1::Array{UInt16,1}, obs2::Array{UInt16,1})::Bool
    obs_size = size(obs1,1)

    @assert size(obs2,1) == obs_size

    for i in 1:obs_size
        if obs1[i] != obs2[i]
            if obs1[i] != dontcare && obs2[i] != dontcare
                return false
            end
        end
    end

    return true
end

function are_obs_compatible_score(obs1::Array{Set{UInt8},1}, obs2::Array{Set{UInt8},1})::Int64
    obs_size = size(obs1,1)

    @assert size(obs2,1) == obs_size

    score = sum(map(i->length(intersect(obs1[i], obs2[i])), 1:obs_size))

    return score
end


function are_obs_compatible(obs1::Array{Set{UInt8},1}, obs2::Array{Set{UInt8},1})::Bool
    obs_size = size(obs1,1)

    @assert size(obs2,1) == obs_size

    for i in 1:obs_size
        if isempty(intersect(obs1[i], obs2[i]))
            return false
        end
    end
    

    return true
end

function is_obs_empty(obs::Array{UInt16,1})::Bool
    for i in 1:size(obs,1)
        if obs[i] != dontcare
            return false
        end
    end

    return true
end