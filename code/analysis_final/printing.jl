using LightGraphs
using EzXML
using GraphIO

function triplets_to_graph(triplets::Array{Int64,2})
    g = SimpleDiGraph(maximum(triplets))

    for i in 1:size(triplets,1)
        add_edge!(g, triplets[i,1], triplets[i,3])
        add_edge!(g, triplets[i,2], triplets[i,3])
    end

    savegraph("triplets.gml", g, GraphMLFormat())
    # graphml2gv triplets.gml > triplets.gv
    # dot -Tpng triplets.gv > triplets.png
    return g
end