using Statistics
using Distances
using LinearAlgebra

# Compute all rows distances pairwise
function compute_rows_distances(traces::Array{UInt8,2})

    @assert(size(traces,1) <= 100000)
    (rows, cols) = size(traces)
    traces = convert.(Int64, traces)

    d = zeros(rows, rows)

    int_traces = convert(Array{Int64,2},traces)

    # @showprogress "Computing rows distances..." 
    for r1 in 1:rows
        for r2 in r1+1:rows
            d[r1,r2] = euclidean(int_traces[r1,:], int_traces[r2,:])
            # d[r1,r2] = chebyshev(int_traces[r1,:], int_traces[r2,:])
            d[r2,r1] = d[r1,r2]
        end
    end

    return d
end

function threshold_cluster_counting(distances::Array{Float64,2}, threshold)
    rows = size(distances, 1)

    assigned::Set{Int64} = Set()
    cluster_assignment::Array{Int64} = zeros(rows)
    cluster_count = 0
    for r1 in 1:rows
        if !(r1 in assigned)
            in_cluster = Set(findall(r2 -> distances[r1, r2] <= threshold, 1:rows))

            

            if length(intersect(assigned, in_cluster)) > 0
                incoh = first(intersect(assigned, in_cluster))
                # println(intersect(assigned, in_cluster))
                # println(r1)
                # println("Incoherent: $r1 <> $(cluster_assignment[incoh])")
                return 0
            end

            union!(assigned, in_cluster)
            cluster_assignment[collect(in_cluster)] .= cluster_count

            cluster_count += 1
            if cluster_count > 256
                return 0
            end
        end
    end
    return cluster_count
end


using Clustering

function kmedoids_centers(traces::Array{UInt8,2})
    rows = min(size(traces,1), 5000)
    d = compute_rows_distances(traces[1:rows,:])

    (cluster_count, threshold) = find_threshold(d)
    cl = kmedoids(d, cluster_count)

    centers = zeros(cluster_count, size(traces, 2))
    a = assignments(cl)

    for ci in 1:cluster_count
        ai = findall(i->a[i] == ci, 1:rows)
        centers[ci,:] = mean(traces[ai,:],dims=1)
    end
    return (centers, threshold)
end

# Knowing the centers, evaluate the best candidate, closest to this vec.
function assign_center(vec::Array{UInt8,1}, centers::Array{Float64,2}, threshold::Float64)
    dists = map(i-> euclidean(vec, centers[i,:]), 1:size(centers,1))
    potential = findall(i -> dists[i] <= threshold, 1:length(dists))
    best = argmin(dists)

    # @assert(length(potential) == 1)

    if length(potential) != 1
        println("Not just 1 inside threshold: $(length(potential))")
        println("Best: $(best-1), $threshold < $(dists[best])")
        scatter(dists)
        throw(ArgumentError("OneToOne error"))
    end
    
    return best-1 #-1 to convert to UInt8
end


function evaluate_threshold(traces::Array{UInt8, 2}, nsteps = 1000)

    rows = min(size(traces,1), 5000)
    d = compute_rows_distances(traces[1:rows,:])

    return evaluate_threshold(d,nsteps)
end


function evaluate_threshold(d::Array{Float64, 2}, nsteps = 1000)

    max_dist = maximum(d)

    th = 0.0
    counts = zeros(Int64, nsteps)

    # @showprogress "Evaluating thresholds..." 
    for i in 1:nsteps
        th += (max_dist/nsteps)
        counts[i] = threshold_cluster_counting(d, th)
    end

    return (counts, max_dist/nsteps)
end

function find_threshold(traces::Array{UInt8,2})


    (counts, step) = evaluate_threshold(traces, 1000)
    m = maximum(counts)

    im = findall(i->counts[i] == m, 1:length(counts))
    mid = (maximum(im) + minimum(im))/2

    return (m, mid*step)
end

function find_threshold(d::Array{Float64,2})


    (counts, step) = evaluate_threshold(d, 1000)
    m = maximum(counts)

    im = findall(i->counts[i] == m, 1:length(counts))
    mid = (maximum(im) + minimum(im))/2

    return (m, mid*step)
end

function distance2stochastic(d::Array{Float64,2})
    (rows, cols) = size(d)
    st = zeros(rows, cols)
    for r in 1:rows
        for c in 1:cols
            if d[r,c] > 0
                st[r,c] = 1 / (d[r,c] * d[r,c])
            # else
            #     st[r,c] = 1.0
            end
        end
    end

    for c in 1:cols
        st[:,c] ./= sum(st[:,c])
    end
    return st
end

function stochastic_center(traces::Array{UInt8,2}, st::Array{Float64,2}, in_vec::Array{Float64,1}, thstd::Float64 = 3.0)

    in_vec ./= sum(in_vec) #normalize
    f = st * in_vec

    # only keep 3stds -> saturate
    th = mean(f) + thstd * std(f)

    cluster::Array{Int64,1} = []
    
    for i in 1:size(f,1)
        if f[i] < th
            f[i] = 0.0
            if in_vec[i] > th
                push!(cluster, i)
            end
        else
            push!(cluster, i)
        end

        
    end

    f ./= sum(f)

    center = zeros(size(traces,2))
    for i in 1:size(f,1)
        center += f[i] .* traces[i,:]
    end

    return (center, cluster)
end

function stochastic_centers(st::Array{Float64,2}, t::Array{UInt8, 2}, thstd::Float64 = 3.0)::Array{Float64,2}
    rows = size(st,1)

    cluster_assignment::Array{Int64,1} = zeros(Int64, rows)
    ass = 1
    counter = 0

    centers = []

    while true
        next_row = findfirst(i-> i == 0, cluster_assignment)
        # print("\e[2K\e[1G $counter/$rows")
        if next_row === nothing
            break
        end

        ei = zeros(rows)
        ei[next_row] = 1.0

        (center, cluster) = stochastic_center(t, st, ei, thstd)
        cluster_assignment[cluster] .= ass
        ass += 1
        counter += length(cluster)
        push!(centers, center)
    end

    # print("\e[2K\e[1G")

    if length(centers) > 256 #we can deal with noise, but we cant fit more than 256 values in a byte
        # println("New try with thstd = $(thstd-0.5)")
        return stochastic_centers(st, t, thstd - 0.5)
    else
        return hcat(centers...)'
    end

    
end

function stochastic_centers(traces::Array{UInt8,2}, thstd::Float64 = 3.0)::Array{Float64,2}
    rows = min(size(traces,1), 5000)
    d = compute_rows_distances(traces[1:rows,:])
    st = distance2stochastic(d)

    return stochastic_centers(st, traces, thstd)
    
end


# function replace_with_cluster(traces::Array{UInt8,2}, in_indices::Array{Int64,1}, cluster::Array{UInt8,1})
#     first_index = minimum(in_indices)
#     new_indices = setdiff(1:size(traces,2), in_indices)
#     # keep chronological order
#     before = filter(i -> i < first_index, new_indices)
#     after = filter(i -> i > first_index, new_indices)

#     #compute index mapping
#     # in_indices are all mapped to cluster index
#     push!(new_indices, first_index)
#     new_indices = sort(new_indices)
#     mapping = zeros(Int64, size(traces,2))
#     mapping[in_indices] .= first_index

#     for i in 1:length(new_indices)
#         mapping[new_indices[i]] = i
#     end

#     return (hcat(traces[:, before], cluster, traces[:,after ]), mapping)
# end


function CovHeuristic2_test(traces::Array{UInt8,2}, i1, i2, i3)
    dists = zeros(256, 256)
    (rows, cols) = size(traces)

    for r in 1:rows
        # dists[traces[r,i1]+1, traces[r,i2]+1, traces[r,i3]+1] += 1
        dists[traces[r,i1]+1, traces[r,i2]+1] += traces[r,i3]
    end

   
    # for a in 1:256
    #     for b in 1:256
    #         dists[a,b] /= rows
    #     end
    # end

    cd = cov(dists)
    cd2 = cov(dists')

    m1 = mean(abs.(cd))/mean(diag(cd))
    m2 = mean(abs.(cd2))/mean(diag(cd2))


    return m1 + m2
end

function CovHeuristic2_raw(traces::Array{UInt8,2}, i1, i2, i3)
    dists = zeros(256, 256)
    (rows, cols) = size(traces)

    for r in 1:rows
        # dists[traces[r,i1]+1, traces[r,i2]+1, traces[r,i3]+1] += 1
        dists[traces[r,i1]+1, traces[r,i2]+1] += traces[r,i3]
    end

   
    # for a in 1:256
    #     for b in 1:256
    #         dists[a,b] /= rows
    #     end
    # end

    return dists
end

function disp_coeff(d::Array{Float64, 1})
    n = length(d)
    m = mean(d)
    s = 0

    for i in 1:n
        if d[i] != 0
            s += abs(m - d[i])
        end
    end

    return s/(n*m)
end

function CovHeuristic1_raw(traces::Array{UInt8,2}, i1, i2)
    dists = zeros(256)
    (rows, cols) = size(traces)

    for r in 1:rows
        # dists[traces[r,i1]+1, traces[r,i2]+1, traces[r,i3]+1] += 1
        dists[traces[r,i1]+1] += traces[r,i2]
    end

   
    for a in 1:256
        dists[a] /= rows
    end

    return dists
end

function CovHeuristic1_test(traces::Array{UInt8,2}, i1, i2)
    dists1 = zeros(256)
    dists2 = zeros(256)
    (rows, cols) = size(traces)

    for r in 1:rows
        dists1[traces[r,i1]+1] += traces[r,i2]
        dists2[traces[r,i2]+1] += traces[r,i1]
    end

   
    for a in 1:256
        dists1[a] /= rows
        dists2[a] /= rows
    end

    return min(var(dists1), var(dists2))
    # return var(dists1)
end

# Quite effictient heuristic to preselect i3 with low fractional_added_information.
# A second pass with fai should be performed.
function CovHeuristic2_tests(traces::Array{UInt8,2}, i1, i2)
    (rows, cols) = size(traces)
    # imax = max(i1, i2)

    variations = zeros(cols)

    # for i in 1:imax
    #     variations[i] = -Inf
    # end

    for i3 in 1:cols
        if i3 != i1 && i3 != i2
            variations[i3] = CovHeuristic2_test(traces, i1, i2, i3)
        end
    end
    
    return variations
end

function CovHeuristic2_tests2(traces::Array{UInt8,2}, i1, i2)
    (rows, cols) = size(traces)
    # imax = max(i1, i2)

    variations = zeros(cols,3)

    # for i in 1:imax
    #     variations[i] = -Inf
    # end

    for i3 in 1:cols
        if i3 != i1 && i3 != i2
            variations[i3,1] = CovHeuristic2_test(traces, i1, i2, i3)
            variations[i3,2] = CovHeuristic2_test(traces, i1, i3, i2)
            variations[i3,3] = CovHeuristic2_test(traces, i2, i3, i1)
        end
    end
    
    return variations
end

function CovHeuristic1_tests(traces::Array{UInt8,2}, i1)
    (rows, cols) = size(traces)

    variations = zeros(cols)

    for i in 1:i1
        variations[i] = -Inf
    end

    for i2 in i1+1:cols
        variations[i2] = CovHeuristic1_test(traces, i1, i2)
    end
    
    return variations
end

struct DetectionConfiguration
    # preselection_2to1_threshold::Float64
    fai_1to1_threshold::Float64
    fai_2to1_threshold::Float64
end

function fai1(traces::Array{UInt8,2}, t::Int64)::Array{Float64,1}
    return map(c -> fractional_added_information(traces[:,c], traces[:,t]), 1:size(traces,2))
end

function fai2(traces::Array{UInt8,2}, t1::Int64, t2::Int64)::Array{Float64,1}
    return map(c -> fractional_added_information2(traces[:,c], traces[:,[t1,t2]]), 1:size(traces,2))
end

function OneToOne_find(traces::Array{UInt8,2}, i1::Int64, dconfig::DetectionConfiguration)::Array{Int64, 1}
    (rows, cols) = size(traces)
    tcount = min(rows, 100000)

    #1-Heuristic

    # ch =  CovHeuristic1_tests(traces, i1)

    # keep indices for a high test result
    # pre_selected = findall(i-> ch[i] >= pre_selection_th, 1:cols)

    # 2 - Definitive selection with fai : slow but specific
    selected = filter(i2-> 
    begin
        fractional_added_information(traces[1:tcount,i2], traces[1:tcount,i1]) < dconfig.fai_1to1_threshold
    end,
    (i1+1):cols)

    return selected
end

function OneToOne_find_total(traces::Array{UInt8,2}, i1::Int64, noise_threshold::Float64)::Array{Int64, 1}
    (rows, cols) = size(traces)
    tcount = min(rows, 100000)

    #1-Heuristic

    # ch =  CovHeuristic1_tests(traces, i1)

    # keep indices for a high test result
    # pre_selected = findall(i-> ch[i] >= pre_selection_th, 1:cols)

    # 2 - Definitive selection with fai : slow but specific
    th = 1.0 - noise_threshold
    selected = filter(i2-> 
    begin
        fractional_added_information(traces[1:tcount,i2], traces[1:tcount,i1]) < th
    end,
    union(1:(i1-1), (i1+1:cols)))

    return selected
end

# function ReduceOneToOne(traces::Array{UInt8, 2}, indices::Array{Int64, 1}, clustering_threshold::Float64 = 5.0)
    
#     # new column to replace traces[:,indices]
#     ci = threshold_clustering(traces[:,indices], 5.0)
#     return replace_with_cluster(traces, indices, ci) #(new_traces, mapping)
# end

# Find the output times for a function with inputs at time i1 and i2.
function TwoToOne_find(traces::Array{UInt8, 2}, i1, i2, dconfig::DetectionConfiguration, window::Int64 = 0)
    (rows, cols) = size(traces)
    tcount = min(rows, 100000)
    # c = setdiff(1:cols, [i1,i2])
    
    # # 1 - Covariance Heuristic: fast, sensitive, not specific
    # pre_selected = TwoToOne_preselect(traces,i1,i2)
    # println("$(length(pre_selected)) preselected.")

    # scores = ones(cols)

    # for i3 in pre_selected
    #     scores[i3] = fractional_added_information2(traces[:,i3], traces[:,[i1,i2]])
    # end

    ts = i2+1:cols

    if window != 0
        ts = i2+1:min(cols, i2+window)
    end
    

    # threshold = mean(scores[pre_selected]) - std(scores[pre_selected])
    # 2 - Definitive selection with fai : slow but specific
    selected = filter(i3-> 
    begin
        fractional_added_information2(traces[1:tcount,i3], traces[1:tcount,[i1,i2]]) < dconfig.fai_2to1_threshold
    end,
    # pre_selected)
    ts)
    # selected = filter(i3 -> scores[i3] < threshold, c)

    return selected
end

function TwoToOne_preselect(traces::Array{UInt8, 2}, i1, i2)
    (rows, cols) = size(traces)
    tcount = min(rows, 1000000)
    c = setdiff(1:cols, [i1,i2])
    th = 1.5

    ch_result = CovHeuristic2_tests2(traces, i1, i2)
    m = vec(mean(ch_result[c,:], dims=1))
    s = vec(std(ch_result[c,:], dims=1))

    scores = zeros(cols, 3)
    selected::Set{Int64} = Set()
    for i3 in i2+1:cols
        scores[i3,:] = (ch_result[i3,:] - m)./s
        for com in 1:size(scores,2)
            if abs(scores[i3,com]) > th
                push!(selected, i3)
            end
        end
    end

    # selected = findall(i3 -> abs(scores[i3]) > 2, 1:cols)

    return selected
    # return scores
end 

function two2one_graph(traces::Array{UInt8, 2}, input_indices_set::Array{Int64, 1}, dconfig::DetectionConfiguration, window::Int64 = 0)

    (trace_count, trace_len) = size(traces)

    if window == 0
        window = trace_len
    end

    triplets::Array{Int64, 2} = zeros(Int64, 0, 3)

    todo_set::Set{Tuple{Int64, Int64}} = Set()
    active_nodes::Set{Int64} = Set()
    done = 0


    function push_new_index(time::Int64)
        if time in active_nodes
            return
        end

        # println("Pushing $time")

        
        for iX in active_nodes
            if abs(iX - time) <= window
                if iX < time
                    push!(todo_set, (iX, time))
                else
                    push!(todo_set, (time, iX))
                end
            end
        end 

        push!(active_nodes, time)

    end

    # init todo_set

    for iX in input_indices_set
        push_new_index(iX)
    end

    prog = ProgressUnknown("2 to 1 identification: ")

    # println(length(todo_set))

    # while there is a pair (i1, i2) in todo_set
    while length(todo_set) > 0
        (i1, i2) = pop!(todo_set)
        tot = done + length(todo_set)
        ProgressMeter.next!(prog; showvalues = [("$done", "$tot")])
        # print("\r$done/$tot ($(round(100*done/tot, digits=2))%)")
        # println("Testing inputs $i1 and $i2..., still $(length(todo_set)) to do.")
        i3s = TwoToOne_find(traces, i1, i2,dconfig, window)

        # i3set = Set(i3s)
        i3set=i3s

        # if length(i3set) > 1#error
        #     println("\nError: $i1 + $i2 -> $i3set\n")
        #     # return triplets
        #     i3set = Set([])
        # end

        # triplets identified
        while length(i3set) > 0
            i3 = pop!(i3set)
            triplets = vcat(triplets, [i1, i2, i3]')
            println("\n$i1, $i2 -> $i3")

            push_new_index(i3)
        end

        
        done += 1
    end
    ProgressMeter.finish!(prog)

    # println()

    return triplets
end

function active_times_from_triplets(triplets::Array{Int64, 2})::Array{Int64, 1}

    s::Set{Int64} = Set()
    for ti in 1:size(triplets,1)
        push!(s, triplets[ti,1])
        push!(s, triplets[ti,2])
        push!(s, triplets[ti,3])
    end
    return sort(collect(s))
end

# function load_and_simplify(path::String, traces_count::Int64 = 4000000)
    
#     traces = load_traces(path, 0, traces_count)
#     (trace_count, trace_len) = size(traces)

#     println("Initial traces length = $trace_len")

#     i = 1
#     while i < trace_len
#         println("\r$i/$trace_len")
        
#         one_to_one_indices = OneToOne_find(traces, i)
#         push!(one_to_one_indices, i)
#         i += 1

#         if length(one_to_one_indices) > 1
#            (traces, mapping) = ReduceOneToOne(traces, one_to_one_indices)
#             println("There is now $(size(traces, 2)) times in traces")
#             GC.gc()
#             trace_len = trace_len - length(one_to_one_indices) + 1
#         end
#     end

#     return traces
# end