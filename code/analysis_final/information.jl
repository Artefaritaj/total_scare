using ProgressMeter


function fractional_added_information(a::Array{UInt8, 1}, b::Array{UInt8, 2})

    if size(b,2) == 2
        return fractional_added_information2(a,b)
    else

        eb = shannon_entropy(a)
        if abs(eb) < 1e-3
            return 1.0
        end

        ce = shannon_entropy(hcat(a, b)) - shannon_entropy(b)
        
        # ce = get_conditional_entropy(a, b, number_of_bins=256)
        # eb = get_entropy(b, number_of_bins=256)
        return ce/eb
    end
end

entropy_estimators = Dict((0,0) => zeros(Float64,1,1)) #(dims, number_of_samples) => estimator_table

function precompute_entropy_estimator(dims::Int64, number_of_samples::Int64)
    t = generate_entropy_estimator_table(number_of_samples, dims)

    entropy_estimators[(dims, number_of_samples)] = t
end

function fractional_added_information2(a::Array{UInt8, 1}, b::Array{UInt8, 2})

    @assert(size(b, 2) == 2)

    eb = shannon_entropy(a)
    if abs(eb) < 1e-3
        return 1.0
    end

    ce = shannon_entropy3(hcat(a, b)) - shannon_entropy2(b)
    
    # ce = get_conditional_entropy(a, b, number_of_bins=256)
    # eb = get_entropy(b, number_of_bins=256)
    return ce/eb
end

function fractional_added_information(a::Array{UInt8, 1}, b::Array{UInt8, 1})


    eb = shannon_entropy(a)
    if abs(eb) < 1e-3
        return 1.0
    end

    ce = shannon_entropy2(hcat(a, b)) - shannon_entropy(b)
    
    # ce = get_conditional_entropy(a, b, number_of_bins=256)
    # eb = get_entropy(b, number_of_bins=256)
    return ce/eb
end

# function fractional_added_information_gpu(a::CuArray{UInt8, 1}, b::CuArray{UInt8, 1})::Float32


#     eb = shannon_entropy_gpu(a)
#     if abs(eb) < 1e-3
#         return 1f0
#     end

#     ce = shannon_entropy2_gpu(hcat(a, b)) - shannon_entropy_gpu(b)
#     return ce/eb
# end


function byte_probability_vector(vector::Array{UInt8,1})::Array{Float64,1}
    rows = size(vector, 1)

    res = zeros(256)

    for i in 1:rows
        res[vector[i]+1] += 1
    end

    for i in 1:256
        res[i] /= rows
    end

    return res
end

# using CuCountMap


function se_from_probability_vector(p::Array{Float64,1})::Float64
    return -sum(i-> p[i] == 0 ? 0 : p[i]*log(2,p[i]), 1:256)
end

function shannon_entropy(vector::Array{UInt8,1}, corrected::Bool = true)::Float64

    # p = byte_probability_vector(vector)
    # rows = length(vector)
    # se = -sum(i-> p[i] == 0 ? 0 : p[i]*log(2,p[i]), 1:256)

    # println("Size is $(size(vector,1)), length is $(length(vector))")
    rows = size(vector,1)
    cm = countmap(vector)
    se = -sum(c ->  begin
        p = c/rows
        return p*log(2,p)
    end, values(cm))

    if corrected == true
        estimator_table = get_estimator_table(1, rows)
        se = entropy_estimate(estimator_table, se)
    end

    return se

end

# function shannon_entropy_gpu(vector::CuArray{UInt8,1})::Float32
#     rows = size(vector,1)
#     cm = countmap(vector)
#     se = -sum(c ->  begin
#         p = c/rows
#         return p*log(2,p)
#     end, values(cm))
    

#     return se

# end

function shannon_entropy(vector::Array{UInt8,2}, corrected::Bool=true)::Float64
    d = Dict()
    (nrows, ncols) = size(vector)

    if ncols == 2
        return shannon_entropy2(vector, corrected)
    elseif ncols == 3
        return shannon_entropy3(vector, corrected)
    end

    for i in 1:nrows
        current_count = get(d, vector[i,:], 0)
        d[vector[i,:]] = current_count + 1
    end

    p::Float64 = 0.0

    for (k, v) in d
        thisp = v/nrows
        p += thisp * log(2, thisp)
    end

    se = -p

    if corrected == true
        estimator_table = get_estimator_table(ncols, nrows)
        se = entropy_estimate(estimator_table, se)
    end

    return se
end

# when vector has only 2 columns
function shannon_entropy2(vector::Array{UInt8,2}, corrected::Bool=true)#::Float64

    d = zeros(Int32, 2^16)
    (nrows, ncols) = size(vector)

    @assert (ncols == 2)

    for i in 1:nrows
        v = (vector[i,1] | convert(Int32, vector[i,2])<< 8) + 1
        d[v] += 1
    end


    p::Float64 = 0.0

    for i in 1:size(d,1)
        v = d[i]
        if v != 0
            
            thisp = v/nrows
            p += thisp * log(2, thisp)
        end
    end

    se = -p

    # v = convert(Array{UInt16,1}, vector[:,2])
    # v .<<= 8 
    # v .|= vector[:,1]

    # cm = countmap(v)
    # se = -sum(c ->  begin
    #         p = c/nrows
    #         return p*log(2,p)
    # end, values(cm))

    if corrected == true
        estimator_table = get_estimator_table(ncols, nrows)
        se = entropy_estimate(estimator_table, se)
    end

    return se
end

# function shannon_entropy2_gpu(vector::CuArray{UInt8,2})::Float32
#     (nrows, ncols) = size(vector)
#     @assert (ncols == 2)

#     v = convert(CuArray{UInt16,1}, vector[:,2])
#     v .<<= 8
#     v .|= vector[:,1]

#     cm = countmap(v)
#     se = -sum(c ->  begin
#         if c == 0
#             return 0f0
#         else
#             p = c/nrows
#             return p*log(2,p)
#         end
#     end, values(cm))
    

#     return se
# end

function get_estimator_table(dims::Int64, number_of_samples::Int64)::Array{Float64,2}
    if haskey(entropy_estimators, (dims, number_of_samples))
        return entropy_estimators[(dims, number_of_samples)]
    else
        estimator_table = generate_entropy_estimator_table(number_of_samples, dims)
        entropy_estimators[(dims, number_of_samples)] = estimator_table # update the static dict of tables
        return estimator_table
    end
end

function shannon_entropy3(vector::Array{UInt8,2}, corrected::Bool=true)#::Float64

    d = zeros(Int32, 2^24)
    (nrows, ncols) = size(vector)

    @assert (ncols == 3)

    for i in 1:nrows
        v = (vector[i,1] | convert(Int32, vector[i,2])<< 8 | convert(Int32, vector[i,3]) << 16) + 1
        d[v] += 1
    end


    p::Float64 = 0.0

    for i in 1:size(d,1)
        v = d[i]
        if v != 0
            
            thisp = v/nrows
            p += thisp * log(2, thisp)
        end
    end

    se = -p

    if corrected == true
        estimator_table = get_estimator_table(ncols, nrows)
        se = entropy_estimate(estimator_table, se)
    end

    return se

end

#build a function from examples
function extract_fun3(triplets::Array{UInt8,2})::Dict{Tuple{UInt8, UInt8},UInt8}
    @assert size(triplets, 2) == 3
    d = Dict{Tuple{UInt8, UInt8},UInt8}()
    tcount = size(triplets,1)

    for i in 1:tcount
        k = (triplets[i,1], triplets[i,2])
        if haskey(d, k)
            last_val = d[k]
            if last_val != triplets[i,3]
                throw(ArgumentError("Conflicting outputs $last_val and $(triplets[i,3]) for ($k)"))
            end
        else#first apparition
            # print("\e[2K\e[1G$i: $(triplets[i,1:2]) => $(triplets[i,3])")
            push!(d, k => triplets[i,3])
        end
    end
    d
end

function extract_fun3_noisy(triplets::Array{UInt8,2})::Dict{Tuple{UInt8, UInt8},UInt8}
    @assert size(triplets, 2) == 3
    d = Dict{Tuple{UInt8, UInt8},UInt8}()
    tcount = size(triplets,1)
    valcount = zeros(Int64, 256, 256, 256)

    for i in 1:tcount
        valcount[triplets[i,1] + 1, triplets[i,2] + 1, triplets[i,3] + 1] += 1
    end

    for i1 in 1:256
        for i2 in 1:256
            (maxval, maxind) = findmax(valcount[i1,i2,:])
            push!(d, (i1-1, i2-1) => maxind - 1)
        end
    end
    d
end

#build a function from examples
function extract_fun2(pairs::Array{UInt8, 2})::Dict{UInt8, UInt8}
    @assert size(pairs, 2) == 2
    d = Dict{UInt8,UInt8}()
    tcount = size(pairs,1)

    for i in 1:tcount
        if haskey(d, pairs[i,1])
            last_val = d[pairs[i,1]]
            if last_val != pairs[i,2]
                throw(ArgumentError("Conflicting outputs $last_val and $(pairs[i,2]) for ($(pairs[i,1]))"))
            end
        else#first apparition
            # print("\e[2K\e[1G$i: $(triplets[i,1:2]) => $(triplets[i,3])")
            push!(d, pairs[i,1] => pairs[i,2])
        end
    end
    d
end

function extract_fun2_noisy(pairs::Array{UInt8,2})::Dict{UInt8,UInt8}
    @assert size(pairs, 2) == 2
    d = Dict{UInt8,UInt8}()
    tcount = size(pairs,1)
    valcount = zeros(Int64, 256, 256)

    for i in 1:tcount
        valcount[pairs[i,1] + 1, pairs[i,2] + 1] += 1
    end

    for i1 in 1:256
        (maxval, maxind) = findmax(valcount[i1,:])
        push!(d, (i1-1) => maxind - 1)
    end
    d
end

function fai_matrix(traces::Array{UInt8,2})
    (rows, cols) = size(traces)
    nrows = min(rows, 100000)

    fai_mat = zeros(cols, cols)

    @showprogress for c1 in 1:cols
        for c2 in c1+1:cols
            
            fai_mat[c1,c2] = fractional_added_information(traces[1:nrows,c1], traces[1:nrows,c2])
            fai_mat[c2,c1] = fractional_added_information(traces[1:nrows,c2], traces[1:nrows,c1])
        end
    end

    return fai_mat
    
end

function joint_probability_matrix3(triplets::Array{UInt8,2})
    @assert size(triplets, 2) == 3
    tot::Float64 = size(triplets,1)

    dic = Dict{Array{UInt8,1},Float64}()
    mat = zeros(Float64, 256*256,256)
    tcount = size(triplets,1)

    #for each triplet, add occurence in mat
    for i in 1:tcount
        a = (convert(Int64, triplets[i,1]) << 8) + convert(Int64, triplets[i,2])
        b = convert(Int64,triplets[i, 3])
        mat[a+1, b+1] += 1.0
        # mat[triplets[i,1]+1, triplets[i,2]+1, triplets[i,3]+1] += 1.0
    end

    

    # for c in 1:size(mat,2)
    #     s = sum(mat[:,c])
    #     if s != 0
    #         mat[:,c] = mat[:,c] / s
    #     end
    # end

    for r in 1:size(mat,1)
        s = sum(mat[r,:])
        if s != 0
            mat[r,:] = mat[r,:] / s
        end
    end

    return mat
end

function entropy_estimate(estimator_table::Array{Float32,2}, measured_entropy::Float32)::Float64
    ma = maximum(estimator_table[:,1])
    mi = minimum(estimator_table[:,1])

    # if measured_entropy > ma || measured_entropy < mi
    #     throw(ArgumentError("Measured entropy does not corresponds to the given estimator table.  Should have $mi < $measured_entropy < $ma"))
    # end

    if measured_entropy >= ma
        return estimator_table[end,2]
    end

    if measured_entropy <= mi
        return estimator_table[1,2]
    end

    sup = 0
    bel = 0

    for i in 1:size(estimator_table,1)
        if measured_entropy <= estimator_table[i,1]
            if sup == 0
                sup = i
            end
        else
            bel = i
        end
    end

    if bel > sup
        s = sup
        sup = bel
        bel = s
    end


    # println("$bel $sup, $measured_entropy")

    a = (measured_entropy - estimator_table[bel,1])/(estimator_table[sup,1] - estimator_table[bel,1])


    return a*(estimator_table[sup,2] - estimator_table[bel,2]) + estimator_table[bel,2]
end

function entropy_estimate(estimator_table::Array{Float64,2}, measured_entropy::Float64)::Float64
    ma = maximum(estimator_table[:,1])
    mi = minimum(estimator_table[:,1])

    # if measured_entropy > ma || measured_entropy < mi
    #     throw(ArgumentError("Measured entropy does not corresponds to the given estimator table.  Should have $mi < $measured_entropy < $ma"))
    # end

    if measured_entropy >= ma
        return estimator_table[end,2]
    end

    if measured_entropy <= mi
        return estimator_table[1,2]
    end

    sup = 0
    bel = 0

    for i in 1:size(estimator_table,1)
        if measured_entropy <= estimator_table[i,1]
            if sup == 0
                sup = i
            end
        else
            bel = i
        end
    end

    if bel > sup
        s = sup
        sup = bel
        bel = s
    end


    # println("$bel $sup, $measured_entropy")

    a = (measured_entropy - estimator_table[bel,1])/(estimator_table[sup,1] - estimator_table[bel,1])


    return a*(estimator_table[sup,2] - estimator_table[bel,2]) + estimator_table[bel,2]
end