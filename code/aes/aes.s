/*
 * File: aes.s
 * Project: aes
 * Created Date: Thursday June 10th 2021
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Friday, 11th June 2021 3:37:57 pm
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2021 INRIA
 */

import "ark.s";
import "sb.s";
import "sr.s";
import "mc.s";
import "key_sch.s";

//memory layout
// @0x1000: plaintext up to 0x100F
// @0x1010: key up to 0x101F
// @0x1020: ciphertext up to 0x102F



// State is stored on s0, s1, s2, s3
// #[address(0xA0000)]
// @cipher_data:

//assume row encoded

// def pt1: u32 = 0x00112233;
// def pt2: u32 = 0x44556677;
// def pt3: u32 = 0x8899AABB;
// def pt4: u32 = 0xCCDDEEFF;


#[address(0xB0000)]
@key_data:

def k1: u32 = 0x00010203;
def k2: u32 = 0x04050607;
def k3: u32 = 0x08090A0B;
def k4: u32 = 0x0C0D0E0F;


//init
#[address(0x8000)]
@entry:

//init stack
sp <- 0x3000;

// set fixed plaintext in memory
// t0 <-t1 [&pt1]; //load pt1 in t0
// [0x1000] <-t1 t0;//store t0 at 0x1000
// t0 <-t1 [&pt2];
// [0x1004] <-t1 t0;
// t0 <-t1 [&pt3];
// [0x1008] <-t1 t0;
// t0 <-t1 [&pt4];
// [0x100C] <-t1 t0;

// set key in memory
t0 <-t1 [&k1];
[0x1010] <-t1 t0;
t0 <-t1 [&k2];
[0x1014] <-t1 t0;
t0 <-t1 [&k3];
[0x1018] <-t1 t0;
t0 <-t1 [&k4];
[0x101C] <-t1 t0;

// init state with plaintext in memory
// s0 <-t0 [0x1000];
// s1 <-t0 [0x1004];
// s2 <-t0 [0x1008];
// s3 <-t0 [0x100C];

//init plaintext with prng
s0 <- CSR[0x123];
s1 <- CSR[0x123];
s2 <- CSR[0x123];
s3 <- CSR[0x123];

call &ark;

//round counter
a0 <- 0;
@loop_start:
push a0;

call &subbytes;
call &shiftrows;
call &mixcolumns;

pop a0;
push a0;// recall but keep in stack
call &next_round_key;
call &ark;

pop a0;
a0 <- a0 + 1;
t0 <- 9;
if (a0 <u t0) goto &loop_start;
push a0;

call &subbytes;
call &shiftrows;
pop a0;
call &next_round_key;
call &ark;


//save output
output s0;
output s1;
output s2;
output s3;
CSR[0] <- x0;//halt with error