/*
 * File: sr.s
 * Project: aes
 * Created Date: Monday February 3rd 2020
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Wednesday, 19th February 2020 11:10:58 am
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2020 INRIA
 */

#[global]
@shiftrows:
    push ra;
    call &rotate;
    
    t0 <- s1 << 8;
    t1 <- s1 >> 24;
    s1 <- t0 xor t1;

    t0 <- s2 << 16;
    t1 <- s2 >> 16;
    s2 <- t0 xor t1;

    t0 <- s3 << 24;
    t1 <- s3 >> 8;
    s3 <- t0 xor t1;

    call &rotate;
    pop ra;
    return;

@rotate:

    //row0
    t1 <- 0xFF000000;
    a0 <- s0 & t1;

    t0 <- s1 & t1;
    t0 <- t0 >> 8;
    a0 <- a0 xor t0;

    t0 <- s2 & t1;
    t0 <- t0 >> 16;
    a0 <- a0 xor t0;

    t0 <- s3 & t1;
    t0 <- t0 >> 24;
    a0 <- a0 xor t0;

    //row1
    t1 <- 0xFF0000;
    t0 <- s0 & t1;
    t0 <- t0 << 8;
    a1 <- t0; 

    t0 <- s1 & t1;
    a1 <- a1 xor t0;

    t0 <- s2 & t1;
    t0 <- t0 >> 8;
    a1 <- a1 xor t0;

    t0 <- s3 & t1;
    t0 <- t0 >> 16;
    a1 <- a1 xor t0;

    //row2
    t1 <- 0xFF00;
    t0 <- s0 & t1;
    t0 <- t0 << 16;
    a2 <- t0; 

    t0 <- s1 & t1;
    t0 <- t0 << 8;
    a2 <- a2 xor t0;

    t0 <- s2 & t1;
    a2 <- a2 xor t0;

    t0 <- s3 & t1;
    t0 <- t0 >> 8;
    a2 <- a2 xor t0;

    //row3
    t1 <- 0xFF;
    t0 <- s0 & t1;
    t0 <- t0 << 24;
    a3 <- t0; 

    t0 <- s1 & t1;
    t0 <- t0 << 16;
    a3 <- a3 xor t0;

    t0 <- s2 & t1;
    t0 <- t0 << 8;
    a3 <- a3 xor t0;

    t0 <- s3 & t1;
    a3 <- a3 xor t0;

    s0 <- a0;
    s1 <- a1;
    s2 <- a2;
    s3 <- a3;

    return;