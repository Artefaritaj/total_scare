using LightGraphs
using ProgressMeter

diff_threshold = 0.0001

function create_belief_graph_order1(traces::Array{UInt8, 2})::SimpleDiGraph{Int64}
    se_mat = se_matrix(traces)
    se_vec = se_vector(traces)
    create_belief_graph_order1(traces, se_mat, se_vec)
end

epsilon = 1.0e-10

function create_belief_graph_order1(traces::Array{UInt8, 2}, se_mat::Array{Float64, 2}, se_vec::Array{Float64, 1})::SimpleDiGraph{Int64}
    (trace_count, trace_len) = size(traces)

    g = SimpleDiGraph(trace_len)

    println("Creating all 1 to 1 edges...")
    #create all edges t1 -> t2 (t1, t2) when there is a dependency be the two
    for t1 in 1:trace_len
        for t2 in t1+1:trace_len
            if se_mat[t1, t2] <= (max(se_vec[t1], se_vec[t2]) + epsilon)
                add_edge!(g, t1, t2)
            end

            print("\e[2K\e[1G t1 = $t1 - t2 = $t2")
        end
    end
    println("\nGraph has $(nv(g)) vertices and $(ne(g)) edges.")
    g
end


function create_belief_graph_order1_v2(traces::Array{UInt8, 2}, mi_mat::Array{Float64, 2}, mi_noise::Float64)::SimpleDiGraph{Int64}
    (trace_count, trace_len) = size(traces)

    g = SimpleDiGraph(trace_len)

    # println("Creating all 1 to 1 edges...")
    #create all edges t1 -> t2 (t1, t2) when there is a dependency be the two
    @showprogress 1 "Creating 1-to-1 edges..." for t1 in 1:trace_len
        for t2 in t1+1:trace_len
            if mi_mat[t1, t2] > mi_noise
                add_edge!(g, t1, t2)
            end

            # print("\e[2K\e[1G t1 = $t1 - t2 = $t2")
        end
    end
    # println("\nGraph has $(nv(g)) vertices and $(ne(g)) edges.")
    g
end


function create_dual_traces(traces::Array{UInt8, 2}, order1::SimpleDiGraph{Int64})
    cg = connected_components(order1)

    (traces_count, real_traces_len) = size(traces)
    dual_traces_len = length(cg)

    dual_traces = zeros(UInt8, traces_count, dual_traces_len)
    real2dv_dict::Dict{Int64, Dict{Array{UInt8, 1}, UInt8}} = Dict()

    #create a new mapping
    @showprogress 1 "Creating dual traces..." for dt in 1:dual_traces_len
        real2dv = create_dv_dict(traces[:, cg[dt]])
        real2dv_dict[dt] = real2dv
        dual_traces[:,dt] = map(i-> real2dv[traces[i,cg[dt]]], 1:traces_count)
        # print("\e[2K\e[1G $dt/$dual_traces_len")
    end
    # println()
    (dual_traces, real2dv_dict)
end

function create_dv_dict(traces::Array{UInt8,2})::Dict{Array{UInt8, 1}, UInt8}
    s = Set([traces[i,:] for i in 1:size(traces,1)])
    if length(s) > 256
        throw(DomainError("Connected component cannot be made into a dual value"))
    end

    Dict([el => convert(UInt8, i-1) for (i, el) in enumerate(s)])
end

