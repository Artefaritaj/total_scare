using ProgressMeter

@inline function allequal(x)
    length(x) < 2 && return true
    e1 = x[1]
    @inbounds for i=2:length(x)
        x[i] == e1 || return false
    end
    return true
end

function remove_constants(m::Array{UInt8,2})::Array{UInt8,2}
	# convert to vector of vectors (on columns)	
	vv = [m[:,i] for i in 1:size(m, 2)]
	f = filter!(v -> allequal(v) == false,vv)
	hcat(f...)
end

function remove_same(m::Array{UInt8,2})::Array{UInt8, 2}
    # convert to vector of vectors (on columns)	
    vv = [m[:,i] for i in 1:size(m, 2)]
    vv = unique(vv)
    hcat(vv...)
end

function loading_by_parts_no_constants_no_same(path::String, max::Int64, batch_size::Int64)
    return loading_simplify_by_parts(path, max, batch_size) do x::Array{UInt8, 2}
        remove_same(remove_constants(x))
    end
end

using InformationMeasures
function mi_matrix(data::Array{UInt8,2})

    (rows, cols) = size(data)

    mimat = zeros(Float64, cols, cols)

    @showprogress 1 "Computing MIs..." for c1 = 1:cols
        for c2 = c1:cols
            # print("\rc1=$c1, c2=$c2")
            thismi = get_mutual_information(data[:,c1], data[:,c2])
            mimat[c1, c2] = thismi
            mimat[c2, c1] = thismi
        end
    end
    
    mimat
end

function se_vector(traces::Array{UInt8,2})
    (trace_count, trace_len) = size(traces)

    se_vec = zeros(Float64, trace_len)
    
    for i in 1:trace_len
        se_vec[i] = shannon_entropy(traces[:,i])
    end

    se_vec
end

using Base.Threads
using SharedArrays
#compute Shannon entropy of tuples
function se_matrix(data::Array{UInt8, 2})
    (rows, cols) = size(data)

    semat::SharedArray{Float64, 2} = zeros(Float64, cols, cols)
    #@showprogress 1 "Computing SEs..."
    i = 0
    tot = cols*cols/2

    @threads for c1 = 1:cols
        for c2 = c1+1:cols
            # print("\e[2K\e[1G c1=$c1, c2=$c2")
            i += 1
            percent::Int64 = round(Int64, i/tot * 100)
            print("\e[2K\e[1G $percent%")
            # thisse = get_entropy(data[:,c1], data[:,c2])
            thisse = shannon_entropy(data[:,[c1, c2]])
            semat[c1, c2] = thisse
            semat[c2, c1] = thisse
        end
        # print("\e[2K\e[1G $c1/$cols")
    end
    
    semat
end

# can be displayed with
# using Plots
# Plots.heatmap(cvmat)


function mi_cluster_at(traces::Array{<: Real, 2}, mi_mat::Array{Float64,2}, mi_noise_threshold::Float64, at::Int64)
    (trace_count, trace_len) = size(traces)

    # get all indices of time values linked with index [at]
    significative_indices = findall(x -> x > mi_noise_threshold, mi_mat[at, :])

    counter = Dict()

    for i in 1:trace_count
        current_count = get(counter, traces[i,significative_indices], 0)
        counter[traces[i,significative_indices]] = current_count + 1
    end

    counter
end

function se_find_cluster(traces::Array{UInt8, 2}, at::Int64)
    (trace_count, trace_len) = size(traces)

    significative_indices::Array{Int64, 1} = [at]

    #first identify dependencies
    for t2 in at+1:trace_len
        if get_entropy(traces[:,at], traces[:,t2], number_of_bins=256) < 8.0
            push!(significative_indices,t2)
        end
    end

    significative_indices
end


function se_find_cluster(traces::Array{UInt8, 2}, se_matrix::Array{Float64, 2}, at::Int64)
    (trace_count, trace_len) = size(traces)

    significative_indices::Array{Int64, 1} = [at]

    #first identify dependencies
    for t2 in at+1:trace_len
        if se_matrix[at, t2] < 8.0
            push!(significative_indices,t2)
        end
    end

    significative_indices
end

function count_dual_values_from_cluster(traces::Array{UInt8, 2}, cluster::Array{Int64, 1})::Dict{Array{UInt8, 1}, Int64}

    (trace_count, trace_len) = size(traces)
    dual_values = Dict()

    for i in 1:trace_count
        current_count = get(dual_values, traces[i,cluster], 0)
        dual_values[traces[i,cluster]] = current_count + 1
    end

    dual_values
end

function dual_values_from_cluster(traces::Array{UInt8, 2}, cluster::Array{Int64, 1})::Array{UInt8, 2}

    (trace_count, trace_len) = size(traces)
    dual_values::Array{UInt, 2} = zeros(UInt8, 256, length(cluster))
    next_key::Int64 = 1
    recorded_vectors::Dict{Array{UInt8, 1}, Bool} = Dict()

    for i in 1:trace_count
        if !haskey(recorded_vectors, traces[i,cluster])
            #new correspondance
            recorded_vectors[traces[i,cluster]] = true
            dual_values[next_key,:] = traces[i,cluster]
            if next_key < 256
                next_key += 1
            else
                break
            end
        end
    end

    dual_values
end

