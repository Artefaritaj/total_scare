

#Random Gaussian noise
using SpecialFunctions
sqrt2 = sqrt(2)
RandGaussianNoise(sigma) = sigma * sqrt2 * erfinv(2.0*rand()-1.0)

HWNoise(x, sigma) = RandGaussianNoise(sigma) + count_ones(x)
Noise(x, sigma) = RandGaussianNoise(sigma) + x

using ProgressMeter
using LightGraphs
using Distances

function sample(val::AbstractFloat, min_max::Tuple{AbstractFloat, AbstractFloat})::UInt8
    (min_val, max_val) = min_max
    

    if val <= min_val
        return 0x0
    elseif val >= max_val
        return 0xFF
    else
        span = max_val-min_val

        return round(UInt8, (val - min_val)*255.0 / span)
    end
end

function sample_traces(traces::Array{Float16, 2})::Array{UInt8,2}
    tend = min(size(traces,2), 50000)

    min_val = minimum(traces[1:tend,:])
    max_val = maximum(traces[1:tend,:])
    println("Min = $min_val, max = $max_val")

    (tcount, tlen) = size(traces)

    
    @showprogress 1 "Sampling... " for exe in 1:tcount
        for t in 1:tlen
            traces[exe,t] = sample(traces[exe,t], (min_val, max_val))
        end
        # print("\e[2K\e[1G$exe/$tcount")
    end
    # print("\e[2K\e[1G")

    return convert.(UInt8, traces)
end

# using Base.Threads
function apply_noise(traces::Array{UInt8, 2}, sigma::Float64)
    (tcount, tlen ) = size(traces)
    noisy_traces::Array{Float16, 2} = zeros(Float16, tcount, tlen)

    # c = 0
    @showprogress 1 "Applying noise... " for i in 1:tcount
        noisy_traces[i,:] = convert.(Float16, Noise.(traces[i,:], sigma))
        # c += 1
        # print("\e[2K\e[1G$c/$tcount")
    end
    # print("\e[2K\e[1G")
    noisy_traces = sample_traces(noisy_traces)

    return noisy_traces
end

function apply_noise!(traces::Array{UInt8, 2}, sigma::Float64)
    (tcount, tlen ) = size(traces)
    batch_size = 100000
    batch_count = round(Int64, tcount / batch_size)

    for b in 1:batch_count
        bstart::Int64 = (b-1)*batch_size + 1
        bend::Int64 = b*batch_size
        # println("$bstart:$bend")

        traces[bstart:bend,:] = apply_noise(traces[bstart:bend,:], sigma)
    end

end


struct NoisyValue
    means::Array{Float64, 1}
    stds::Array{Float64, 1}
end

struct NoisyVariable
    mapping::Dict{UInt8, NoisyValue}
end

function fractional_added_information(a::Array{UInt8, 1}, b::Array{UInt8, 2})

    if size(b,2) == 2
        return fractional_added_information2(a,b)
    else

        eb = shannon_entropy(a)
        if abs(eb) < 1e-3
            return 1.0
        end

        ce = shannon_entropy(hcat(a, b)) - shannon_entropy(b)
        
        # ce = get_conditional_entropy(a, b, number_of_bins=256)
        # eb = get_entropy(b, number_of_bins=256)
        return ce/eb
    end
end

function fractional_added_information2(a::Array{UInt8, 1}, b::Array{UInt8, 2})

    @assert(size(b, 2) == 2)

    eb = shannon_entropy(a)
    if abs(eb) < 1e-3
        return 1.0
    end

    ce = shannon_entropy3(hcat(a, b)) - shannon_entropy2(b)
    
    # ce = get_conditional_entropy(a, b, number_of_bins=256)
    # eb = get_entropy(b, number_of_bins=256)
    return ce/eb
end

function fractional_added_information(a::Array{UInt8, 1}, b::Array{UInt8, 1})


    eb = shannon_entropy(a)
    if abs(eb) < 1e-3
        return 1.0
    end

    ce = shannon_entropy2(hcat(a, b)) - shannon_entropy(b)
    
    # ce = get_conditional_entropy(a, b, number_of_bins=256)
    # eb = get_entropy(b, number_of_bins=256)
    return ce/eb
end

# a very naive method for clustering given a known noise threshold
function threshold_clustering(in::Array{UInt8, 2}, threshold)
    (nrows, ncols) = size(in)
    clusters = zeros(UInt8, nrows)

    work_row = 1

    @showprogress 1 "Clustering... " for cluster_index in 255:-1:0 # max 256 clusters
        while clusters[work_row] != 0
            work_row += 1

            if work_row == nrows
                return clusters
            end
        end

        ref = in[work_row, :]
        dists = map(r -> euclidean(ref, in[r,:]), work_row+1:nrows)
        in_clusters = findall(r -> dists[r] < threshold, 1:size(dists,1))
        in_clusters .+= work_row
        clusters[in_clusters] .= cluster_index
        clusters[work_row] = cluster_index
    end

    return clusters
end

function replace_with_cluster(traces::Array{UInt8,2}, in_indices::Array{Int64,1}, cluster::Array{UInt8,1})
    first_index = minimum(in_indices)
    new_indices = setdiff(1:size(traces,2), in_indices)
    # keep chronological order
    before = filter(i -> i < first_index, new_indices)
    after = filter(i -> i > first_index, new_indices)

    #compute index mapping
    # in_indices are all mapped to cluster index
    push!(new_indices, first_index)
    new_indices = sort(new_indices)
    mapping = zeros(Int64, size(traces,2))
    mapping[in_indices] .= first_index

    for i in 1:length(new_indices)
        mapping[new_indices[i]] = i
    end

    return (hcat(traces[:, before], cluster, traces[:,after ]), mapping)
end

function heuristic_fractional_added_information(in::Array{UInt8, 2}, out::Array{UInt8,2})
    d = Dict()

    (_, out_count) = size(out)
    (nrows, in_count) = size(in)

    @showprogress 1 "Computing heuristic..." for i in 1:nrows
        current_array = get(d, in[i,:], Set([])) # get array of distributions (not normalized) for this in value ([] if no values)


        push!(current_array, out[i,:])
        d[in[i,:]] = current_array
    end


    return d
end

function fai1(traces::Array{UInt8,2}, t::Int64)::Array{Float64,1}
    return map(c -> fractional_added_information(traces[:,t], traces[:,c]))
end

function OneToOne_find(traces::Array{UInt8,2}, i1::Int64)::Array{Int64, 1}
    (rows, cols) = size(traces)

    # magic thresholds
    pre_selection_th = 0.02  # heuristic bigger than that
    selection_th = 0.1      # fai lower than that

    

    #1-Heuristic

    ch =  CovHeuristic1_tests(traces, i1)

    # keep indices for a high test result
    pre_selected = findall(i-> ch[i] >= pre_selection_th, 1:cols)

    if length(pre_selected) > 0
        println("$(pre_selected) preselected.")
    end

    # 2 - Definitive selection with fai : slow but specific
    selected = filter(i2-> 
    begin
        print(".")
        fractional_added_information(traces[:,i2], traces[:,i1]) < selection_th
    end,
    pre_selected)
    if length(selected) > 0
        println("\r\n$(selected) selected.")
    else
        println()#after the dots .......
    end

    return selected
end

function ReduceOneToOne(traces::Array{UInt8, 2}, indices::Array{Int64, 1}, clustering_threshold::Float64 = 5.0)
    
    # new column to replace traces[:,indices]
    ci = threshold_clustering(traces[:,indices], 5.0)
    return replace_with_cluster(traces, indices, ci) #(new_traces, mapping)
end

# function find_1to1(traces::Array{UInt8, 2}, source::Int64, fai_mat::Array{Float64, 2}, threshold::Float64 = 0.1)
#     return findall(i-> fai_mat[source,i] < threshold, 1:size(fai_mat, 2))
# end

# function simplify_1to1!(traces::Array{UInt8, 2}, indices::Array{Int64, 1}, fai_mat)
#     fai_mat_threshold = 0.25
#     clustering_threshold = 5.0

#     @showprogress 1 "Simplifying 1 to 1..." for i in indices
#         ti = findall(j-> fai_mat[6,j] < fai_mat_threshold, 1:size(fai_mat, 2))
#         ci = threshold_clustering(traces[:,ti], 5.0)
#         replace_with_cluster!(traces, ti, ci)
#         GC.gc()
#     end
# end

function noisy_analyze_test(pts::Array{UInt8,2}, traces::Array{UInt8,2}, cts::Array{UInt8,2})
    
    # traces = hcat(pts, noisy, cts)
    input_len = size(pts, 2)
    output_len = size(cts,2)
    (trace_count, trace_len) = size(traces)
    tot_len = trace_len
    trace_len = trace_len - input_len - output_len

    fract_threshold = 0.5

    g = SimpleDiGraph(trace_len + input_len + output_len)
    
    for in_byte in 1:input_len
        cond_ent = ones(Float64, tot_len)
        @showprogress 1 "Initial conditionnal entropies for byte $in_byte " for t in 1+input_len:trace_len
            cond_ent[t] = fractional_added_information(traces[:,in_byte], traces[:,t])
        end
        significative_indices = findall(i -> cond_ent[i] < fract_threshold, 1:tot_len)
        
        for sigi in significative_indices
            add_edge!(g, in_byte, sigi)
        end
    end

    return g
end

function noisy_analyze(traces::Array{UInt8,2})
    
    (trace_count, trace_len) = size(traces)

    fract_threshold = 0.2

    # g = SimpleDiGraph(trace_len)
    r = zeros(trace_len, trace_len)
    
    for t1 in 1:trace_len
        @showprogress 1 "Initial conditionnal entropies for time $t1" for t2 in t1+1:trace_len
            fai = fractional_added_information(traces[:,t2], traces[:,t1])
            r[t1, t2] = fai
            r[t2, t1] = fai
            # if fai < fract_threshold
            #     add_edge!(g, t1, t2)
            # end
        end
    end

    return r
end

using LinearAlgebra
using Statistics

function get_normal_params(traces::Array{UInt8,2})
    m = vec(mean(traces, dims=1))
    c = cov(traces) + Diagonal(0.1*ones(size(traces,2)))
    # c = Diagonal(vec(std(traces, dims=1)))
    return (m, c)
end

function kldiv(a::Tuple{Array{Float64, 1}, Array{Float64, 2}}, b::Tuple{Array{Float64, 1}, Array{Float64, 2}})
    (ma, ca) = a
    k = size(ma, 1)
    (mb, cb) = b
    cbinv = pinv(cb)
    kld = 0.5*(tr(cbinv*ca) + (mb-ma)' * cbinv * (mb-ma) -k + (logdet(cb) - logdet(ca))/log(2.0))
    # ln(det(cb)/det(ca)))
    return kld
end

function maha_dist(v::Array{UInt8, 1}, normal_dist::Tuple{Array{Float64, 1}, Array{Float64, 2}})
    (m, c) = normal_dist
    cinv = pinv(c)
    return (v - m)' * cinv * (v - m)
end

#Bhattacharyya
function bha_dist(a_dist::Tuple{Array{Float64, 1}, Array{Float64, 2}}, b_dist::Tuple{Array{Float64, 1}, Array{Float64, 2}})
    (ma, ca) = a_dist
    (mb, cb) = b_dist
    s = 0.5*(ca + cb)
    invs = inv(s)
    db = 0.125 * (ma-mb)' * invs * (ma-mb) + 0.5 * logdet(s) - 0.25 * logdet(ca) - 0.25 * logdet(cb)
    return db
end



function ce3_test(traces::Array{UInt8,2}, i1, i2)
    (tc, tlen) = size(traces)
    cmi = ones(tlen)

    @showprogress 1 "ce3_test " for i3 in i2+1:tlen
        # print("\e[2K\e[1G$i3")
        cmi[i3] = fractional_added_information(traces[:,i3], traces[:,[i1, i2]])
    end

    return cmi

end

function get_rows16(v1, v2, traces)
    (tc, tlen) = size(traces)
    findall(i -> traces[i,1] == v1 && traces[i,6] == v2, 1:tc)
end

function establish_clustering_threshold(traces::Array{UInt8, 2}, t1::Int64, t2::Int, t3::Array{Int64, 1})
    # t3 = f(t1,t2)

    #rows for (0,0)
    r00 = findall(i -> traces[i,t1] == 0 && traces[i,t2] == 0, 1:size(traces, 1))
    ref_dist = get_normal_params(traces[r00, t3])

    bha_mat = bha2ref(ref_dist, traces, t1, t2, t3)

    t = 0
    step = 0.02

    last_count = -1

    while true
        ct = count(x -> x < t, bha_mat)

        if ct == last_count
            return (t, ct)
        else
            last_count = ct
            t += step
        end
    end
end

function bha2ref(ref_dist::Tuple{Array{Float64, 1}, Array{Float64, 2}}, traces::Array{UInt8, 2},  t1::Int64, t2::Int, t3::Array{Int64, 1})
   
    (tc, _) = size(traces)

    dists = zeros(256, 256)

    @showprogress 1 "bha test" for v1 in 0:255
        for v2 in 0:255
            r = findall(i -> traces[i,t1] == v1 && traces[i,t2] == v2, 1:tc)

            dists[v1+1, v2+1] = bha_dist(get_normal_params(traces[r, t3]), ref_dist)
        end
    end

    return dists
end




function CovHeuristic2_test(traces::Array{UInt8,2}, i1, i2, i3)
    dists = zeros(256, 256)
    (rows, cols) = size(traces)

    for r in 1:rows
        # dists[traces[r,i1]+1, traces[r,i2]+1, traces[r,i3]+1] += 1
        dists[traces[r,i1]+1, traces[r,i2]+1] += traces[r,i3]
    end

   
    # for a in 1:256
    #     for b in 1:256
    #         dists[a,b] /= rows
    #     end
    # end

    cd = cov(dists)
    cd2 = cov(dists')

    m1 = mean(abs.(cd))/mean(diag(cd))
    m2 = mean(abs.(cd2))/mean(diag(cd2))


    return m1 + m2
end

function CovHeuristic2_raw(traces::Array{UInt8,2}, i1, i2, i3)
    dists = zeros(256, 256)
    (rows, cols) = size(traces)

    for r in 1:rows
        # dists[traces[r,i1]+1, traces[r,i2]+1, traces[r,i3]+1] += 1
        dists[traces[r,i1]+1, traces[r,i2]+1] += traces[r,i3]
    end

   
    # for a in 1:256
    #     for b in 1:256
    #         dists[a,b] /= rows
    #     end
    # end

    return dists
end

function disp_coeff(d::Array{Float64, 1})
    n = length(d)
    m = mean(d)
    s = 0

    for i in 1:n
        if d[i] != 0
            s += abs(m - d[i])
        end
    end

    return s/(n*m)
end

function CovHeuristic1_raw(traces::Array{UInt8,2}, i1, i2)
    dists = zeros(256)
    (rows, cols) = size(traces)

    for r in 1:rows
        # dists[traces[r,i1]+1, traces[r,i2]+1, traces[r,i3]+1] += 1
        dists[traces[r,i1]+1] += traces[r,i2]
    end

   
    for a in 1:256
        dists[a] /= rows
    end

    return dists
end

function CovHeuristic1_test(traces::Array{UInt8,2}, i1, i2)
    dists1 = zeros(256)
    dists2 = zeros(256)
    (rows, cols) = size(traces)

    for r in 1:rows
        dists1[traces[r,i1]+1] += traces[r,i2]
        dists2[traces[r,i2]+1] += traces[r,i1]
    end

   
    for a in 1:256
        dists1[a] /= rows
        dists2[a] /= rows
    end

    return min(var(dists1), var(dists2))
    # return var(dists1)
end

# Quite effictient heuristic to preselect i3 with low fractional_added_information.
# A second pass with fai should be performed.
function CovHeuristic2_tests(traces::Array{UInt8,2}, i1, i2)
    (rows, cols) = size(traces)
    imax = max(i1, i2)

    variations = zeros(cols)

    for i in 1:imax
        variations[i] = -Inf
    end

    @showprogress 1 "2 to 1 heuristic" for i3 in imax+1:cols
        variations[i3] = CovHeuristic2_test(traces, i1, i2, i3)
    end
    
    return variations
end

function CovHeuristic1_tests(traces::Array{UInt8,2}, i1)
    (rows, cols) = size(traces)

    variations = zeros(cols)

    for i in 1:i1
        variations[i] = -Inf
    end

    @showprogress 1 "1 to 1 heuristic" for i2 in i1+1:cols
        variations[i2] = CovHeuristic1_test(traces, i1, i2)
    end
    
    return variations
end

function fai_tests(traces::Array{UInt8,2}, i1, i2)
    (rows, cols) = size(traces)
    imax = max(i1, i2)

    variations = ones(cols)

    @showprogress 1 "2 to 1 fai" for i3 in imax+1:cols
        variations[i3] = fractional_added_information2(traces[:,i1],traces[:,[i2, i3]])
    end
    
    return variations
end


function CovHeuristic2_input_tests(traces::Array{UInt8,2}, iX, i3)
    (rows, cols) = size(traces)
    

    variations = zeros(cols)

    for i in 1:cols
        variations[i] = -Inf
    end

    @showprogress 1 "2 to 1 heuristic before" for i1 in 1:iX-1
        variations[i1] = CovHeuristic2_test(traces, i1, iX, i3)
    end

    @showprogress 1 "2 to 1 heuristic after" for i2 in iX+1:i3
        variations[i2] = CovHeuristic2_test(traces, iX, i2, i3)
    end
    
    return variations
end


function fai1to1(traces::Array{UInt8, 2}, iX)
    (rows, cols) = size(traces)

    result = zeros(cols)

    @showprogress 1 "fai 1 to 1 before" for i1 in 1:(iX-1)
        result[i1] = fractional_added_information(traces[:,i1], traces[:,iX])
    end

    @showprogress 1 "fai 1 to 1 after" for i2 in (iX+1):cols
        result[i2] = fractional_added_information(traces[:,iX], traces[:,i2])
    end

    return result
end

# Find the output times for a function with inputs at time i1 and i2.
function TwoToOne_find(traces::Array{UInt8, 2}, i1, i2, selection_th = 0.1)
    (rows, cols) = size(traces)
    

    # magic thresholds
    pre_selection_th = 0.5  # heuristic bigger than that
    # selection_th = 0.1      # fai lower than that
    input_bias_th = 1e-3    # log mean variance difference lower than that

    # 1 - Covariance Heuristic: fast, sensitive, not specific
    ch_result = CovHeuristic2_tests(traces, i1, i2)
    # keep indices for a high test result
    pre_selected = findall(i-> ch_result[i] >= pre_selection_th, 1:cols)

    if length(pre_selected) > 0
        println("$(pre_selected) preselected.")
    end

    # 2 - Definitive selection with fai : slow but specific
    selected = filter(i3-> 
    begin
        print(".")
        # print("\r$i3/$(size(pre_selected,1))")
        fractional_added_information2(traces[:,i3], traces[:,[i1,i2]]) < selection_th
    end,
    pre_selected)
    if length(selected) > 0
        println("\r\n$(selected) selected.")
    else
        println()#after the dots .......
    end

    return (selected, pre_selected)
end

function TwoToOne_find_inputs(traces::Array{UInt8, 2}, i3::Int64)
    (rows, cols) = size(traces)

    # magic thresholds
    pre_selection_th = 0.5  # heuristic bigger than that
    selection_th = 0.2      # fai lower than that
    input_bias_th = 1e-3    # log mean variance difference lower than that

    variations = zeros(i3, i3)
    fai         = zeros(i3, i3)

    @showprogress 1 "finding inputs" for i1 in 1:i3-2
        for i2 in i1+1:i3-1
            variations[i1, i2] = CovHeuristic2_test(traces, i1, i2, i3)

            if variations[i1, i2] > pre_selection_th
                fai[i1, i2] = fractional_added_information(traces[:,i3], traces[:,[i1, i2]])
            end
        end
    end

    return fai
end



function remap_tuples_set(set::Set{Tuple{Int64, Int64}}, mapping::Array{Int64, 1})
    new_set::Set{Tuple{Int64, Int64}} = Set()

    for (a, b) in set
        push!(new_set, (mapping[a], mapping[b]))
    end

    new_set
end

function remap_set(set::Set{Int64}, mapping::Array{Int64, 1})
    new_set::Set{Int64} = Set()

    for a in set
        push!(new_set, mapping[a])
    end

    new_set
end
 
function load_and_simplify(path::String, traces_count::Int64 = 4000000)
    
    traces = load_traces(path, 0, traces_count)
    (trace_count, trace_len) = size(traces)

    println("Initial traces length = $trace_len")

    i = 1
    while i < trace_len
        println("\r$i/$trace_len")
        
        one_to_one_indices = OneToOne_find(traces, i)
        push!(one_to_one_indices, i)
        i += 1

        if length(one_to_one_indices) > 1
            (traces, mapping) = ReduceOneToOne(traces, one_to_one_indices)
            println("There is now $(size(traces, 2)) times in traces")
            GC.gc()
            trace_len = trace_len - length(one_to_one_indices) + 1
        end
    end

    return traces
end

function build_graph(traces::Array{UInt8, 2}, input_indices_set::Array{Int64, 1}, traces_count::Int64 = 4000000)

    (trace_count, trace_len) = size(traces)

    triplets::Array{Int64, 2} = zeros(Int64, 0, 3)

    todo_set::Set{Tuple{Int64, Int64}} = Set()
    active_nodes::Set{Int64} = Set()

    function push_new_index(time::Int64)
        if time in active_nodes
            return
        end

        println("Pushing $time")

        
        for iX in active_nodes
            if iX < time
                push!(todo_set, (iX, time))
            else
                push!(todo_set, (time, iX))
            end
        end 

        push!(active_nodes, time)

    end

    # init todo_set

    for iX in input_indices_set
        push_new_index(iX)
    end


    # while there is a pair (i1, i2) in todo_set
    while length(todo_set) > 0
        (i1, i2) = pop!(todo_set)
        println("Testing inputs $i1 and $i2..., still $(length(todo_set)) to do.")
        (i3s, preselected) = TwoToOne_find(traces, i1, i2)

        i3set = Set(i3s)
        # triplets identified
        while length(i3set) > 0
            i3 = pop!(i3set)
            triplets = vcat(triplets, [i1, i2, i3]')

            push_new_index(i3)
        end

        

    end

    return triplets
end