include("./loaders.jl")
include("./analysis.jl")
include("./inf.jl")
include("./AES.jl")
include("./graphs.jl")
include("./predictor.jl")

#give a plaintext, return a dict association temporal position with plaintext bytes
function input2dict(plaintext::Array{UInt8,1})::Dict{Int64, UInt8}
    inpositions = [4, 3, 2, 1, 8, 7, 6, 5, 12, 11, 10, 9, 16, 15, 14, 13]

    d = Dict()
    for i in 1:16
        d[inpositions[i]] =  plaintext[i]
    end
    d
end

function get_input(trace::Array{UInt8,1})::Array{UInt8,1}
    inpositions = [4, 3, 2, 1, 8, 7, 6, 5, 12, 11, 10, 9, 16, 15, 14, 13]
    pt = zeros(UInt8, 16)

    for i in 1:16
        pt[i] = trace[inpositions[i]]
    end
    pt
end

function get_output(trace::Array{UInt8,1})::Array{UInt8,1}
    outpositions = [109, 102, 95, 111, 141, 134, 127, 143, 173, 166, 159, 175, 205, 198, 191, 207]
    ct = zeros(UInt8, 16)

    for i in 1:16
        ct[i] = trace[outpositions[i]]
    end
    ct
end

function get_inputs(traces::Array{UInt8,2})::Array{UInt8,2}
    hcat([get_input(traces[i,:]) for i in 1:size(traces,1)]...)'
end

function get_outputs(traces::Array{UInt8,2})::Array{UInt8,2}
    hcat([get_output(traces[i,:]) for i in 1:size(traces,1)]...)'
end

function dual2rv(t::Int64, val::UInt8)
    for (k, v) in rv2dual_dict[t]
        if v == val
            return k
        end
    end
    println("Cannot find real value")
end

# for i in 1:64 
#     trace[cg1[i]] = dual2rv(i, dt[i])
# end

# pt = [0x0, 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88, 0x99, 0xAA, 0xBB, 0xCC, 0xDD, 0xEE, 0xFF]
# key = [0x0, 0x1, 0x2, 0x3, 0x4, 0x5, 0x6, 0x7, 0x8, 0x9, 0xA, 0xB, 0xC, 0xD, 0xE, 0xF]


# using Plots

# traces = remove_same(remove_constants(load_traces("traces.binu8")))
# traces = load_traces("small_traces.binu8")


using CSV

#se_mat = se_matrix(traces[1:80000,:])
#CSV.write("se_matrix.csv", DataFrame(se_mat))
# se_mat = convert(Array{Float64, 2}, CSV.read("se_matrix.csv"))
# se_vec = se_vector(traces)
# order1 = create_belief_graph_order1(traces, se_mat, se_vec)
# (dual_traces, rv2dual_dict) = create_dual_traces(traces, order1)
#dual_se_mat = se_matrix(dual_traces[1:80000,:])
#CSV.write("dual_se_matrix.csv", DataFrame(dual_se_mat))
# dual_se_mat = convert(Array{Float64, 2}, CSV.read("dual_se_matrix.csv"))
#triplets = detect2to1(dual_traces[1:80000,:])
#CSV.write("triplets.csv", DataFrame(triplets))
# triplets = convert(Array{Int64,2}, CSV.read("triplets.csv"))
# pts = get_inputs(traces)
# cts = get_outputs(traces)
# pr = Predictor(dual_traces, triplets, pts, cts)

# hw_traces = convert.(UInt8, count_ones.(traces))
# hw_traces = remove_same(remove_constants(hw_traces))
# #hw_se_mat = se_matrix(hw_traces[1:80000,:])
# #CSV.write("hw_se_matrix.csv", DataFrame(hw_se_mat))
# hw_se_mat = convert(Array{Float64, 2}, CSV.read("hw_se_matrix.csv"))
# hw_mi_mat = convert(Array{Float64, 2}, CSV.read("hw_mi_matrix.csv"))
# hw_se_vec = se_vector(hw_traces)

# hw_order1 = create_belief_graph_order1_v2(hw_traces, hw_mi_mat, 0.001)
# (hw_dual_traces, hw_rv2dual_dict) = create_dual_traces(hw_traces, hw_order1)
# # hw_dual_se_mat = se_matrix(hw_dual_traces[1:80000,:])
# # CSV.write("hw_dual_se_matrix.csv", DataFrame(hw_dual_se_mat))
# hw_dual_se_mat = convert(Array{Float64, 2}, CSV.read("hw_dual_se_matrix.csv"))
# hw_dual_se_vec = se_vector(hw_dual_traces)

# # hw_triplets = detect2to1(hw_dual_traces[1:80000,:])
# #CSV.write("hw_triplets.csv", DataFrame(hw_triplets))
# hw_triplets = convert(Array{Int64,2}, CSV.read("hw_triplets.csv"))

# hw_pdual_traces = convert(Array{UInt8, 2}, CSV.read("hw_poor_dual_traces2.csv"))
# hw_pdual_se_mat = convert(Array{Float64, 2}, CSV.read("hw_dual_se_matrix.csv"))
# hw_pdual_se_vec = se_vector(hw_pdual_traces)
# hw_triplets = convert(Array{Int64,2}, CSV.read("hw_poor_triplets.csv"))

# traces = loading_by_parts("traces.binu8", 10000000, 100000)
# pts = get_inputs(traces)
# cts = get_outputs(traces)
# hw_traces = convert.(UInt8, count_ones.(traces))
#traces = remove_constants(hcat(pts, hw_traces, cts))
# traces = load_traces("traces.binu8")
# mi_mat = mi_matrix(traces[1:80000,:])
# hw_order1 = create_belief_graph_order1_v2(hw_traces, mi_mat, 0.28)
# poor_dual_traces = load_traces("poor_dual_traces.binu8")
# poor_dual_se_mat =  convert(Array{Float64, 2}, CSV.read("poor_dual_se_mat.csv"))
# poor_dual_se_vec = se_vector(poor_dual_traces[1:100000,:])
# triplets = convert(Array{Int64,2}, CSV.read("hw_poor_triplets.csv"))
# pts = load_traces("pts.binu8")
# cts = load_traces("cts.binu8")

# xor trace generation
#traces = rand(UInt8, 30000000, 4)
# traces = hcat(traces, map(i-> xor(traces[i,1], traces[i,2]), 1:30000000))
# traces = hcat(traces, map(i-> xor(traces[i,3], traces[i,4]), 1:30000000))
# traces = hcat(traces, map(i-> xor(traces[i,5], traces[i,6]), 1:30000000))
# traces[:,5:6] = count_ones.(traces[:,5:6])
# triplets = [[1, 3, 5] [2, 4, 6] [5, 6, 7]]


#@assert predict(pr, get_input(traces[1,:])) == get_output(traces[1,:])

#noisy

traces = load_traces("noisy.binu8")
mi_mat = convert(Array{Float64, 2}, CSV.read("noisy_mi_mat.csv"))
order1 = create_belief_graph_order1_v2(traces, mi_mat, 2.0)
cg = connected_components(order1)
c1 = cg[1]
# i0 = findall(i->traces[i,1] == 0, 1:size(traces,1))
rang = 1:10000
# ftraces = convert.(Float16, traces[rang,:])
# ftraces ./= Float16(255.0)
# d = pairwise(SqEuclidean(), ftraces[:,c1]', ftraces[:,c1]')
ftraces = convert(Array{Float16, 2}, CSV.read("noisy_ftraces.csv"))
ftraces_dist = convert(Array{Float16, 2}, CSV.read("noisy_ftrace_distances.csv"))

function coverage2to1(l::Int64)::Float64
    r = rand(UInt8, l, 2)
    l = length(Set([r[i,:] for i in 1:size(r, 1)]))
    l/65536.0
end

#predict(pr, pt)
#ffmpeg -framerate 10 -i _f%00d.png -c:v libx264 -profile:v high -crf 18 -pix_fmt yuv420p propagation1.mp4