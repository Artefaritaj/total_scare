using SparseArrays

function byte_probability_vector(vector::Array{UInt8,1})::Array{Float64,1}
    rows = size(vector, 1)

    res = zeros(256)

    for i in 1:rows
        res[vector[i]+1] += 1
    end

    for i in 1:256
        res[i] /= rows
    end

    return res
end

function byte_probability_vector32(vector::Array{UInt8,1})::Array{Float32,1}
    rows = size(vector, 1)

    res = zeros(Float32, 256)

    for i in 1:rows
        res[vector[i]+1] += 1
    end

    for i in 1:256
        res[i] /= rows
    end

    return res
end


function shannon_entropy(vector::Array{UInt8,1})::Float64

    p = byte_probability_vector(vector)

    -sum(i-> p[i] == 0 ? 0 : p[i]*log(2,p[i]), 1:256)

end

function shannon_entropy(vector::Array{UInt8,2})::Float64
    d = Dict()
    (nrows, ncols) = size(vector)

    for i in 1:nrows
        current_count = get(d, vector[i,:], 0)
        d[vector[i,:]] = current_count + 1
    end

    p::Float64 = 0.0

    for (k, v) in d
        thisp = v/nrows
        p += thisp * log(2, thisp)
    end

    return -p
end

# when vector has only 2 columns
function shannon_entropy2(vector::Array{UInt8,2})#::Float64

    d = zeros(Int32, 2^16)
    (nrows, ncols) = size(vector)

    @assert (ncols == 2)

    for i in 1:nrows
        v = (vector[i,1] | convert(Int32, vector[i,2])<< 8) + 1
        d[v] += 1
    end


    p::Float64 = 0.0

    for i in 1:size(d,1)
        v = d[i]
        if v != 0
            
            thisp = v/nrows
            p += thisp * log(2, thisp)
        end
    end

    return -p

end

function shannon_entropy3(vector::Array{UInt8,2})#::Float64

    d = zeros(Int32, 2^24)
    (nrows, ncols) = size(vector)

    @assert (ncols == 3)

    for i in 1:nrows
        v = (vector[i,1] | convert(Int32, vector[i,2])<< 8 | convert(Int32, vector[i,3]) << 16) + 1
        d[v] += 1
    end


    p::Float64 = 0.0

    for i in 1:size(d,1)
        v = d[i]
        if v != 0
            
            thisp = v/nrows
            p += thisp * log(2, thisp)
        end
    end

    return -p

end

function joint_probability_matrix2(pairs::Array{UInt8,2})::Array{Float64,2}
    @assert size(pairs, 2) == 2

    mat = zeros(Float64, 256,256)

    #for each pair, add occurence in mat
    for i in 1:size(pairs,1)
        mat[pairs[i,1]+1, pairs[i,2]+1] += 1.0
    end

    mat ./= sum(mat)
    return mat
end

#build a function from examples
function extract_fun3(triplets::Array{UInt8,2})::Dict{Tuple{UInt8, UInt8},UInt8}
    @assert size(triplets, 2) == 3
    d = Dict{Tuple{UInt8, UInt8},UInt8}()
    tcount = size(triplets,1)

    for i in 1:tcount
        k = (triplets[i,1], triplets[i,2])
        if haskey(d, k)
            last_val = d[k]
            if last_val != triplets[i,3]
                throw(ArgumentError("Conflicting outputs $last_val and $(triplets[i,3]) for ($k)"))
            end
        else#first apparition
            # print("\e[2K\e[1G$i: $(triplets[i,1:2]) => $(triplets[i,3])")
            push!(d, k => triplets[i,3])
        end
    end
    d
end

function extract_fun3_noisy(triplets::Array{UInt8,2})::Dict{Tuple{UInt8, UInt8},UInt8}
    @assert size(triplets, 2) == 3
    d = Dict{Tuple{UInt8, UInt8},UInt8}()
    tcount = size(triplets,1)
    valcount = zeros(Int64, 256, 256, 256)

    for i in 1:tcount
        valcount[triplets[i,1] + 1, triplets[i,2] + 1, triplets[i,3] + 1] += 1
    end

    for i1 in 1:256
        for i2 in 1:256
            maxind = findmax(i->valcount[i1,i2,i], 1:256)
            push!(d, (i1-1, i2-1) => maxind - 1)
        end
    end
    d
end

#build a function from examples
function extract_fun2(pairs::Array{UInt8, 2})::Dict{UInt8, UInt8}
    @assert size(pairs, 2) == 2
    d = Dict{UInt8,UInt8}()
    tcount = size(pairs,1)

    for i in 1:tcount
        if haskey(d, pairs[i,1])
            last_val = d[pairs[i,1]]
            if last_val != pairs[i,2]
                throw(ArgumentError("Conflicting outputs $last_val and $(pairs[i,2]) for ($(pairs[i,1]))"))
            end
        else#first apparition
            # print("\e[2K\e[1G$i: $(triplets[i,1:2]) => $(triplets[i,3])")
            push!(d, pairs[i,1] => pairs[i,2])
        end
    end
    d
end

function joint_probability_matrix3(triplets::Array{UInt8,2})::Dict{Array{UInt8,1},Float64}
    @assert size(triplets, 2) == 3
    tot::Float64 = size(triplets,1)

    dic = Dict{Array{UInt8,1},Float64}()
    # mat = zeros(Float64, 256,256,256)
    tcount = size(triplets,1)

    #for each triplet, add occurence in mat
    for i in 1:tcount
        prev = get(dic, triplets[i,:], 0)
        push!(dic, triplets[i,:] => prev + (1.0/tot))
        # mat[triplets[i,1]+1, triplets[i,2]+1, triplets[i,3]+1] += 1.0
    end

    # mat ./= sum(mat)
    return dic
end

# normalized by columns (sum(col) = 1.0)
function p1given2(joint_probability_matrix::Array{Float64,2})::Array{Float64,2}
    mat = copy(joint_probability_matrix)

    #normalize by column

    for c in 1:256
        s = sum(mat[:,c])
        if s != 0.0
            mat[:,c] ./= s
        end 
    end

    return mat
end

# normalized by columns (sum(col) = 1.0)
function p1given2(joint_probability_matrix::SparseMatrixCSC{Float64,Int32})::Array{Float64,2}
    mat::Array{Float64,2} = Array(joint_probability_matrix)

    #normalize by column

    for c in 1:256
        s = sum(mat[:,c])
        if s != 0.0
            mat[:,c] ./= s
        end
    end

    return mat
end

function p2given1(joint_probability_matrix::Array{Float64,2})::Array{Float64,2}
    mat = copy(joint_probability_matrix)'

    #normalize by column

    for c in 1:256
        s = sum(mat[:,c])
        if s != 0.0
            mat[:,c] ./= s
        end
    end

    return mat
end

# normalized by columns (sum(col) = 1.0)
function p2given1(joint_probability_matrix::SparseMatrixCSC{Float64,Int32})::Array{Float64,2}
    mat::Array{Float64,2} = Array(joint_probability_matrix)'

    #normalize by column

    for c in 1:256
        s = sum(mat[:,c])
        if s != 0.0
            mat[:,c] ./= s
        end
    end

    return mat
end

function p1given2(pairs::Array{UInt8,2})::Array{Float64,2}
    mat = joint_probability_matrix2(pairs)

    #normalize by column
    for c in 1:256
        s = sum(mat[:,c])
        if s != 0.0
            mat[:,c] ./= s
        end
    end

    return mat
end

function p2given1(pairs::Array{UInt8,2})::Array{Float64,2}
    mat = joint_probability_matrix2(pairs)' #beware of the transposed matrix

    #normalize by column
    for c in 1:256
        s = sum(mat[:,c])
        if s != 0.0
            mat[:,c] ./= s
        end
    end

    return mat
end

function conditionnal_probabilities(pairs::Array{UInt8,2})
    jpm = joint_probability_matrix2(pairs)

    p2given1 = copy(jpm)'
    p1given2 = copy(jpm)

    for c in 1:256
        s = sum(p2given1[:,c])
        if s != 0.0
            p2given1[:,c] ./= s
        end
    end

    for c in 1:256
        s = sum(p1given2[:,c])
        if s != 0.0
            p1given2[:,c] ./= s
        end
    end

    (p1given2, p2given1)
end

function conditionnal_probabilities(joint_probability_matrix::Array{Float64,2})
    p1given2 = copy(joint_probability_matrix)
    p2given1 = copy(joint_probability_matrix)'

    l = size(joint_probability_matrix, 1)

    for c in 1:l
        s = sum(p2given1[:,c])
        if s != 0.0
            p2given1[:,c] ./= s
        end
    end

    for c in 1:l
        s = sum(p1given2[:,c])
        if s != 0.0
            p1given2[:,c] ./= s
        end
    end

    (p1given2, p2given1)
end


function propagate_probabilites(in_probabilites::Array{Float64,1}, joint_probability_matrix::Array{Float64,2})::Array{Float64,1}
    joint_probability_matrix * in_probabilites
end

function mutual_information(pairs::Array{UInt8,2})::Float64
    @assert size(pairs, 2) == 2

    # px = byte_probability_vector(pairs[:,1])
    # py = byte_probability_vector(pairs[:,2])

    pxy = joint_probability_matrix2(pairs)

    px = map(x -> sum(pxy[x,:]), 1:256)
    py = map(y -> sum(pxy[:,y]), 1:256)

    #println("sum(pxy) = $(sum(pxy)), sum(px) = $(sum(px)), sum(py) = $(sum(py))")

    mi::Float64 = 0.0

    for x in 1:256
        for y in 1:256
            if pxy[x,y] != 0.0 && px[x] != 0.0 && py[y] != 0.0
                mi += pxy[x,y] * (log(2.0, pxy[x,y]/(px[x]*py[y])))
            end
        end
    end
    
    return mi
end

function detect2to1(traces::Array{UInt8, 2}, window::Int64 = 0)
    tcount = min(size(traces, 1), 80000)
    se_mat::Array{Float64, 2} = convert(Array{Float64, 2}, se_matrix(traces[1:tcount,:]))
    se_vec = se_vector(traces[1:tcount,:])
    detect2to1(traces, se_mat, se_vec, window)
end

function detect2to1(traces::Array{UInt8, 2}, se_mat::Array{Float64, 2}, se_vec::Array{Float64, 1}, window::Int64 = 0)

    triplets::Array{Int64,2} = zeros(Int64, 0, 3)
    (traces_count, traces_len) = size(traces)
    if window == 0
        window = traces_len
    end

    tcount = min(traces_count, 200000)

    count = 0

    for t1 in 1:traces_len
        for t2 in t1+1:traces_len
            if t2 - t1 > window
                continue
            end

            for t3 in t2+1:traces_len
                if t3 - t1 > window
                    continue
                end

                print("\e[2K\e[1G t1 = $t1 - t2 = $t2 - t3 = $t3: $count")
                #suppose that se_mat has been computed with tcount traces
                tri_se = shannon_entropy(traces[1:tcount,[t1, t2, t3]])
                if tri_se < 16.0 && tri_se < (se_mat[t1,t2] + se_vec[t3] - 0.5)
                    count += 1
                    triplets = vcat(triplets, [t1 t2 t3])
                end
            end
        end
    end

    triplets
end

