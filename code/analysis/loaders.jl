

using ProgressMeter

function trace_count(path::String)
  f = open(path, "r")

  if(eof(f) == true)# if file empty, quit
    close(f)
    return 0
  end

  trace_len::Int32 = read(f, Int32)# read trace length
  trace_count::Int32 = read(f, Int32)# read trace count

  return trace_count
end


function loading_simplify_by_parts(simplify, path::String, max::Int64, batch_size::Int64)

  if max == 0
    max = trace_count(path)
  end

  @assert max % batch_size == 0

  traces::Array{UInt8,2} = simplify(load_traces_no_progress(path, 0, batch_size))
  # index::Int64 = 1
  loop_count::Int64 = (max / batch_size) - 1

  @showprogress for index in 1:loop_count
  # while index*batch_size < max
      traces = vcat(traces, simplify(load_traces_no_progress(path, index*batch_size, batch_size)))
      index += 1
  end

  traces
end

function load_traces_no_progress(filepath::String, min::Int64 = 0, read_count::Int64 = 0)::Array{UInt8, 2}
  f = open(filepath, "r")

  if(eof(f) == true)# if file empty, quit
    close(f)
    return 0
  end

  trace_len::Int32 = read(f, Int32)# read trace length
  trace_count::Int32 = read(f, Int32)# read trace count

  if read_count == 0
    read_count = trace_count
  end

  # max = min + read_count

  #println("Trace len $(trace_len), trace count $(trace_count )")

  M=zeros(UInt8, read_count, trace_len)#init the result matrix

  # i::Int32 = 1
  temp = Array{UInt8}(undef,trace_len)

  seek(f, 8 + min*trace_len)

  # while eof(f) == false && i <= read_count # read each trace and put in matrix
  for i in 1:read_count
    if eof(f) == true
      break
    end

    # M[i, :] = read(f, Int8, trace_len)
    read!(f, temp)
    M[i,:] = temp
    i+=1
  end

  close(f)
  return M

end


function load_traces(filepath::String, min::Int64 = 0, read_count::Int64 = 0)::Array{UInt8, 2}
  f = open(filepath, "r")

  if(eof(f) == true)# if file empty, quit
    close(f)
    return 0
  end

  trace_len::Int32 = read(f, Int32)# read trace length
  trace_count::Int32 = read(f, Int32)# read trace count

  if read_count == 0
    read_count = trace_count
  end

  # max = min + read_count

  #println("Trace len $(trace_len), trace count $(trace_count )")

  M=zeros(UInt8, read_count, trace_len)#init the result matrix

  # i::Int32 = 1
  temp = Array{UInt8}(undef,trace_len)

  seek(f, 8 + min*trace_len)

  # while eof(f) == false && i <= read_count # read each trace and put in matrix
  @showprogress 1 "Loading traces ($filepath)... " for i in 1:read_count
    if eof(f) == true
      break
    end

    # M[i, :] = read(f, Int8, trace_len)
    read!(f, temp)
    M[i,:] = temp
    i+=1
  end

  close(f)
  return M

end

function write_traces(filepath::String, traces::Array{UInt8, 2})
  f = open(filepath, "w")

  (trace_count::Int32, trace_len::Int32) = size(traces)

  write(f, trace_len)
  write(f, trace_count)

  #println("Trace len $(trace_len), trace count $(trace_count )")

  for i in 1:trace_count
    write(f, traces[i,:])
  end

  close(f)
end
